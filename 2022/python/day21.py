from common import is_int, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=21)


def parse_input(puzzle_input: list) -> dict:
    monkeys = {}
    for line in puzzle_input:
        monkey, operation = line.split(": ")
        monkeys[monkey] = operation
    return monkeys


def resolve_monkey_business(monkeys):
    has_changed = True
    while has_changed:
        has_changed = False
        for monkey, operation in monkeys.items():
            if isinstance(operation, int):
                continue
            match operation.split():
                case [value]:
                    monkeys[monkey] = int(value)
                case [monkey1, command, monkey2]:

                    if not monkey1 in monkeys or not monkey2 in monkeys:
                        continue
                    if not is_int(monkeys[monkey1]) or not is_int(
                        monkeys[monkey2]
                    ):
                        continue

                    has_changed = True
                    match command:
                        case "*":
                            monkeys[monkey] = int(monkeys[monkey1]) * int(
                                monkeys[monkey2]
                            )
                        case "/":
                            monkeys[monkey] = int(monkeys[monkey1]) // int(
                                monkeys[monkey2]
                            )
                        case "+":
                            monkeys[monkey] = int(monkeys[monkey1]) + int(
                                monkeys[monkey2]
                            )
                        case "-":
                            monkeys[monkey] = int(monkeys[monkey1]) - int(
                                monkeys[monkey2]
                            )


def find_missing_number(monkey: str, number: int, monkeys: dict) -> int:
    value1, value2, new_value, new_monkey = None, None, 0, "humn"
    monkey1, operation, monkey2 = monkey.split()
    if is_int(monkey1):
        value1 = int(monkey1)
    elif monkey1 in monkeys and is_int(monkeys[monkey1]):
        value1 = int(monkeys[monkey1])

    if is_int(monkey2):
        value2 = int(monkey2)
    elif monkey2 in monkeys and is_int(monkeys[monkey2]):
        value2 = int(monkeys[monkey2])

    if value1:
        match operation:
            case "*":
                new_value = number // value1
            case "/":
                new_value = value1 // number
            case "+":
                new_value = number - value1
            case "-":
                new_value = value1 - number

        new_monkey = monkeys[monkey2] if monkey2 in monkeys else "humn"

    elif value2:
        match operation:
            case "*":
                new_value = number // value2
            case "/":
                new_value = number * value2
            case "+":
                new_value = number - value2
            case "-":
                new_value = number + value2

        new_monkey = monkeys[monkey1] if monkey1 in monkeys else "humn"

    if new_monkey != "humn":
        return find_missing_number(new_monkey, new_value, monkeys)
    return new_value


def part1(puzzle_input):
    monkeys = parse_input(puzzle_input)
    resolve_monkey_business(monkeys)
    return monkeys["root"]


def part2(puzzle_input):
    monkeys = parse_input(puzzle_input)
    del monkeys["humn"]
    resolve_monkey_business(monkeys)
    root = monkeys["root"]
    monkey1, _, monkey2 = root.split()
    if is_int(monkeys[monkey1]):
        missing_value = find_missing_number(
            monkeys[monkey2], monkeys[monkey1], monkeys
        )
    else:
        missing_value = find_missing_number(
            monkeys[monkey1], monkeys[monkey2], monkeys
        )

    return missing_value


def main():
    result_part1 = part1(puzzle_input)
    result_part2 = part2(puzzle_input)

    print("Part1:", result_part1)
    print("Part2:", result_part2)


if __name__ == "__main__":
    main()
