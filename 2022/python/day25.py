from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=25)


def snafu2dec(number):
    length = len(number) - 1
    dec_number = 0
    for char in number:
        if char == "2":
            dec_number += 2 * 5**length
        elif char == "1":
            dec_number += 1 * 5**length
        elif char == "-":
            dec_number -= 1 * 5**length
        elif char == "=":
            dec_number -= 2 * 5**length
        length -= 1

    return dec_number


def dec2snafu(number):
    snafu = []
    while number:
        diff = number % 5

        if diff == 2:
            snafu.append("2")
        elif diff == 1:
            snafu.append("1")
        elif diff == 0:
            snafu.append("0")
        elif diff == 3:
            snafu.append("=")
            number += 2
        elif diff == 4:
            snafu.append("-")
            number += 1
        number //= 5

    return "".join(reversed(snafu))


def main():
    total_sum = 0
    for line in puzzle_input:
        decimal_number = snafu2dec(line)
        total_sum += decimal_number

    result = dec2snafu(total_sum)
    assert result == "2-=2-0=-0-=0200=--21"

    print("Result:", result)


if __name__ == "__main__":
    main()
