from collections import defaultdict
from copy import copy

from common import manhattan_distance, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=15)


def parse_sensor_beacons(puzzle_input):
    sensors = {}
    beacons_row = defaultdict(set)

    for line in puzzle_input:
        line = line[10:]

        # Sensor
        sensor_raw, beacon_raw = line.split(": closest beacon is at ")
        sensor_x, sensor_y = sensor_raw.split(", ")
        sensor_x = int(sensor_x[2:])
        sensor_y = int(sensor_y[2:])

        # Beacon
        beacon_x, beacon_y = beacon_raw.split(", ")
        beacon_x = int(beacon_x[2:])
        beacon_y = int(beacon_y[2:])

        beacons_row[beacon_y].add(beacon_x)

        m_dist = manhattan_distance([sensor_x, sensor_y], [beacon_x, beacon_y])

        sensors[(sensor_x, sensor_y)] = (beacon_x, beacon_y, m_dist)

    return sensors, beacons_row


def range_for_row(sensor, m_dist, row):
    sensor_col, sensor_row = sensor
    if sensor_row - m_dist <= row <= sensor_row + m_dist:
        x_values = m_dist - abs(sensor_row - row)
        return (sensor_col - x_values, sensor_col + x_values)
    return (0, 0)


def sensor_signal_positions_at_row(sensors, row_to_check):
    x_ranges = []
    for sensor, beacon in sensors.items():
        m_dist = beacon[2]
        new_range = range_for_row(sensor, m_dist, row_to_check)
        if new_range != (0, 0):
            x_ranges.append(new_range)
    x_ranges_sorted = sorted(x_ranges)

    prev_min = x_ranges_sorted[0][0]
    prev_max = x_ranges_sorted[0][1]
    new_list = []
    for i in range(len(x_ranges_sorted)):
        if i >= len(x_ranges_sorted):
            break

        x1, x2 = x_ranges_sorted[i]
        if x1 >= prev_min and x1 <= prev_max:
            prev_max = max(prev_max, x2)
        elif x1 > prev_max:
            new_list.append((prev_min, prev_max))
            prev_min = x1
            prev_max = x2

    new_list.append((prev_min, prev_max))
    return new_list


def occupied_positions(sensors, beacons_row, row_to_check):
    new_list = sensor_signal_positions_at_row(sensors, row_to_check)

    beacons_in_range = 0
    total_number = 0
    for l in new_list:
        total_number += abs(l[1] - l[0] + 1)
        for beacon in beacons_row[row_to_check]:
            if l[0] <= beacon <= l[1]:
                beacons_in_range += 1

    return total_number - beacons_in_range, beacons_in_range


def rows_adjacent_sensor_reach(sensor1, sensor2):
    rows = set()

    if sensor1[0] > sensor2[0]:
        sensor1, sensor2 = sensor2, sensor1

    x1, y1, d1 = sensor1
    x2, y2, d2 = sensor2

    s1_cols_at_s2_row = d1 - abs(y2 - y1)

    overlapping_points = s1_cols_at_s2_row + x1 - (x2 - d2) + 1

    if overlapping_points <= 0:
        return set()

    rows_delta = overlapping_points // 2 + 1

    if y2 >= y1:
        high = y2
        low = y1
    else:
        high = y1
        low = y2

    new_row1 = high + rows_delta
    rows.add(new_row1)

    new_row2 = low - rows_delta
    rows.add(new_row2)

    return rows


def find_rows_to_check(sensors, max_row):

    sensors_to_test = copy(sensors)

    rows_to_test = set()
    for sensor1, beacon1 in sensors.items():
        del sensors_to_test[sensor1]

        for sensor2, beacon2 in sensors_to_test.items():
            s1 = (sensor1[0], sensor1[1], beacon1[2])
            s2 = (sensor2[0], sensor2[1], beacon2[2])
            new_rows = rows_adjacent_sensor_reach(s1, s2)
            for row in new_rows:
                if 0 < row < max_row:
                    rows_to_test.add(row)

    return rows_to_test


def main():
    sensors, beacons_row = parse_sensor_beacons(puzzle_input)

    # Part1
    result_part1, _ = occupied_positions(
        sensors, beacons_row, row_to_check=2000000
    )
    print("Part1", result_part1)

    # Part2
    rows_to_test = find_rows_to_check(sensors, 4000000)
    for row in rows_to_test:
        occupied = sensor_signal_positions_at_row(sensors, row_to_check=row)
        if len(occupied) != 1:
            result_part2 = (occupied[0][1] + 1) * 4000000 + row
            break
    print("Part2", result_part2)


if __name__ == "__main__":
    main()
