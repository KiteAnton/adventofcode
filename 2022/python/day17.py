from collections import defaultdict

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=17)

CAVE_WIDTH = 7


def try_move(rock: list, offset: tuple, occupied_space: defaultdict) -> bool:
    can_move = True
    row_offset, col_offset = offset
    for i, pos in enumerate(rock):
        row, col = pos
        if col + col_offset in occupied_space[row + row_offset]:
            can_move = False

    if can_move:
        for i, pos in enumerate(rock):
            rock[i] = (pos[0] + row_offset, pos[1] + col_offset)

    return can_move


def maybe_left(rock: list, occupied_space: defaultdict) -> None:
    left_pos = min([col for _, col in rock])

    if left_pos - 1 < 0:
        return

    try_move(rock, (0, -1), occupied_space)


def maybe_right(rock: list, occupied_space: defaultdict) -> None:
    right_pos = max([col for _, col in rock])

    if right_pos + 1 >= CAVE_WIDTH:
        return

    try_move(rock, (0, 1), occupied_space)


def maybe_down(rock: list, occupied_space: defaultdict) -> bool:
    return try_move(rock, (-1, 0), occupied_space)


def get_rock(rock_index: int) -> list:
    rocks = [
        [(0, 0), (0, 1), (0, 2), (0, 3)],
        [(0, 1), (1, 0), (1, 1), (1, 2), (2, 1)],
        [(0, 0), (0, 1), (0, 2), (1, 2), (2, 2)],
        [(0, 0), (1, 0), (2, 0), (3, 0)],
        [(0, 0), (0, 1), (1, 0), (1, 1)],
    ]
    return rocks[rock_index % len(rocks)]


def add_rock(
    rock: list,
    top_row: int,
    jet_movement: str,
    jet_iteration: int,
    occupied_space: defaultdict,
) -> tuple:

    rock_left = min([col for _, col in rock])
    rock_bottom = min([row for row, _ in rock])
    start_left = rock_left + 2
    start_top = top_row + 1 + 3 + rock_bottom

    for i, pos in enumerate(rock):
        rock[i] = (pos[0] + start_top, pos[1] + start_left)

    while True:
        jet_move = jet_movement[jet_iteration]
        jet_iteration = (jet_iteration + 1) % len(jet_movement)

        if jet_move == ">":
            maybe_right(rock, occupied_space)
        else:
            maybe_left(rock, occupied_space)

        rock_not_settled = maybe_down(rock, occupied_space)
        if not rock_not_settled:
            for pos in rock:
                row, col = pos
                occupied_space[row].add(col)
                top_row = max(top_row, row)
            break

    return top_row, jet_iteration


def solve(nr_rocks: int, jet_movement: str) -> int:

    occupied_space = defaultdict(set)
    occupied_space[0] = {i for i in range(CAVE_WIDTH)}  # Add Floor
    top_row = 0

    visited = {}
    added = 0

    iteration = 0
    jet_iteration = 0
    while iteration < nr_rocks:
        rock = get_rock(iteration)
        top_row, jet_iteration = add_rock(
            rock, top_row, jet_movement, jet_iteration, occupied_space
        )

        state = (iteration % 5, jet_iteration)  # 5 rocks

        if iteration >= 2022 and state in visited:
            last_top_row, last_iteration = visited[state]
            delta_rows = top_row - last_top_row
            delta_iterations = iteration - last_iteration
            add = (nr_rocks - iteration) // delta_iterations
            added += add * delta_rows
            iteration += add * delta_iterations

        visited[state] = (top_row, iteration)
        iteration += 1

    return top_row + added


def main():
    jet_movement = puzzle_input[0]

    nr_rocks = 2022
    part1 = solve(nr_rocks, jet_movement)

    nr_rocks = 1_000_000_000_000
    part2 = solve(nr_rocks, jet_movement)

    print("Part1:", part1)
    print("Part2:", part2)


if __name__ == "__main__":
    main()
