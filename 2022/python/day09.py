from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=9)


def needs_to_move(lead, follower):
    return abs(lead[0] - follower[0]) > 1 or abs(lead[1] - follower[1]) > 1


def follow_lead(lead, follower, vertical=False):
    pos = 0 if vertical else 1
    if lead[pos] > follower[pos]:
        follower[pos] += 1
    else:
        follower[pos] -= 1


def move_parts(lead, follower):
    if needs_to_move(lead, follower):
        if lead[0] == follower[0]:
            follow_lead(lead, follower)
        elif lead[1] == follower[1]:
            follow_lead(lead, follower, vertical=True)
        else:
            follow_lead(lead, follower, vertical=True)
            follow_lead(lead, follower)


def move_head(head, move):
    if move == "R":
        head[1] += 1
    elif move == "U":
        head[0] += 1
    elif move == "L":
        head[1] -= 1
    elif move == "D":
        head[0] -= 1


def play_game(input_lines, nr_knots=2):
    knots = [[0, 0] for _ in range(nr_knots)]
    visited = set()
    for line in input_lines:
        move, steps = line.split()
        steps = int(steps)
        for _ in range(steps):
            move_head(knots[0], move)
            for knot in range(1, len(knots)):
                move_parts(knots[knot - 1], knots[knot])
            visited.add(tuple(knots[-1]))
    return len(visited)


def part1(input_lines):
    return play_game(input_lines)


def part2(input_lines):
    return play_game(input_lines, nr_knots=10)


def main():
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))


if __name__ == "__main__":
    main()
