import heapq
from copy import copy

from common import (
    find_adjacent_positions_2d,
    manhattan_distance,
    puzzle_input_as_str,
)

puzzle_input = puzzle_input_as_str(day=12)


def parse_map(puzzle_input):
    heightmap = {}
    nr_rows = len(puzzle_input)
    nr_cols = len(puzzle_input[0])
    start = (0, 0)
    end = (0, 0)
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            height = ord(char) - 97
            heightmap[(row, col)] = height
            if char == "S":
                start = (row, col)
                heightmap[(row, col)] = 0
            elif char == "E":
                end = (row, col)

    return heightmap, start, end, nr_rows, nr_cols


def find_path(heightmap, start, end, nr_rows, nr_cols):
    to_visit = [(0, start)]
    nodes = {start: (0, start)}

    boundaries = ((0, nr_rows - 1), (0, nr_cols - 1))
    while to_visit:

        current_node_g, current_node = heapq.heappop(to_visit)

        if current_node == end:
            return current_node_g

        for new_node_position in find_adjacent_positions_2d(
            current_node, boundaries=boundaries
        ):

            # Check if move allowed
            current_node_value = heightmap[current_node]
            new_node_value = heightmap[new_node_position]
            if new_node_value > current_node_value + 1:
                continue

            # Cost of one move/step
            new_node_g = current_node_g + 1

            # Heuristics
            new_node_h = manhattan_distance(new_node_position, end)

            new_node_f = new_node_g + new_node_h

            # new position already visited with 'cheaper' route
            if (
                new_node_position in nodes
                and nodes[new_node_position][0] <= new_node_f
            ):
                continue

            nodes[new_node_position] = (new_node_f, current_node)
            heapq.heappush(to_visit, (new_node_g, new_node_position))

    return 0


def part2(heightmap, end, nr_rows, nr_cols):

    starting_positions = [
        position for position, height in heightmap.items() if height == 0
    ]

    return min(
        filter(
            lambda x: x,  # I.e. non-zero values
            map(
                lambda start_pos: find_path(
                    heightmap, start_pos, end, nr_rows, nr_cols
                ),
                starting_positions,
            ),
        )
    )


def main():
    heightmap, start, end, nr_rows, nr_cols = parse_map(puzzle_input)
    print("Part1", find_path(heightmap, start, end, nr_rows, nr_cols))
    print("Part2", part2(heightmap, end, nr_rows, nr_cols))


if __name__ == "__main__":
    main()
