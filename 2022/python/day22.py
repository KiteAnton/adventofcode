from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=22)


class MonkeyMap2D:
    def __init__(self):
        self.map = set()
        self.blocked = set()
        self.start_pos = (0, 0)
        self.current_position = (0, 0)
        self.direction = 0
        self.size = (0, 0)

    def find_start(self):
        for i in range(self.size[1]):
            if (0, i) in self.map and (0, i) not in self.blocked:
                self.current_position = (0, i)
                break

    def add_free(self, position):
        self.map.add(position)
        self.size = (
            max(self.size[0], position[0] + 1),
            max(self.size[1], position[1] + 1),
        )

    def add_blocked(self, position):
        self.add_free(position)
        self.blocked.add(position)

    def change_direction(self, turn_right):
        change = 1 if turn_right else -1
        self.direction = (self.direction + change) % 4

    def move(self, steps):
        movement = [[0, 1], [1, 0], [0, -1], [-1, 0]][self.direction]
        for _ in range(steps):
            new_pos = (
                self.current_position[0] + movement[0],
                self.current_position[1] + movement[1],
            )
            while new_pos not in self.map:
                new_pos = (
                    (new_pos[0] + movement[0]) % self.size[0],
                    (new_pos[1] + movement[1]) % self.size[1],
                )

            if new_pos in self.blocked:
                break

            self.current_position = new_pos

    def get_map_score(self):
        return (
            (self.current_position[0] + 1) * 1000
            + (self.current_position[1] + 1) * 4
            + self.direction
        )


class MonkeyMap3D(MonkeyMap2D):
    def __init__(self, face_size):
        super().__init__()
        self.face_size = face_size
        self.size = (149, 149)  # TODO delete

    def move(self, steps):
        for _ in range(steps):
            movement = [[0, 1], [1, 0], [0, -1], [-1, 0]][self.direction]
            new_pos = (
                self.current_position[0] + movement[0],
                self.current_position[1] + movement[1],
            )

            new_direction = -1
            if new_pos not in self.map:
                new_pos, new_direction = self.find_wrap_around(new_pos)

            if new_pos in self.blocked:
                break

            self.current_position = new_pos
            if new_direction != -1:
                self.direction = new_direction

    def in_face(self) -> int:
        row, col = self.current_position

        if 0 <= row < self.face_size:
            if col < self.face_size * 2:
                return 1
            return 2
        elif self.face_size <= row < self.face_size * 2:
            return 3
        elif row < self.face_size * 3:
            if col < self.face_size:
                return 4
            return 5
        return 6

    def find_wrap_around(self, new_pos: tuple):
        new_direction = 0
        row, col = self.current_position

        match [self.direction, self.in_face()]:

            ## Right
            # Face 2, right -> Face 5 going left
            case [0, 2]:
                new_pos = (
                    self.face_size * 3 - row - 1,
                    self.face_size * 2 - 1,
                )
                new_direction = 2  # Left

            # Face 3, right -> Face 2 going up
            case [0, 3]:
                new_pos = (self.face_size - 1, row + self.face_size)
                new_direction = 3  # Up

            # Face 5, right -> Face 2 going left
            case [0, 5]:
                new_pos = (
                    self.face_size * 3 - row - 1,
                    self.face_size * 3 - 1,
                )
                new_direction = 2  # Left

            # Face 6, right -> Face 5 going up
            case [0, 6]:
                new_pos = (self.face_size * 3 - 1, row - self.face_size * 2)
                new_direction = 3  # Up

            ## Down
            # Face 2, down -> Face 3 going left
            case [1, 2]:
                new_pos = (col - self.face_size, self.face_size * 2 - 1)
                new_direction = 2  # Left

            # Face 5, down -> Face 6 going left
            case [1, 5]:
                new_pos = (self.face_size * 2 + col, self.face_size - 1)
                new_direction = 2  # Left

            # Face 6, down -> Face 2 going down
            case [1, 6]:
                new_pos = (0, col + self.face_size * 2)
                new_direction = 1  # Down

            ## Left
            # Face 1, left -> Face 4 going right
            case [2, 1]:
                new_pos = (self.face_size * 3 - row - 1, 0)
                new_direction = 0  # Right

            # Face 3, left -> Face 4 going down
            case [2, 3]:
                new_pos = (self.face_size * 2, row - self.face_size)
                new_direction = 1  # Down

            # Face 4, left -> Face 1 going right
            case [2, 4]:
                new_pos = (self.face_size * 3 - row - 1, self.face_size)
                new_direction = 0  # Right

            # Face 6, left -> Face 1 going down
            case [2, 6]:
                new_pos = (0, row - self.face_size * 2)
                new_direction = 1  # Down

            ## Up
            # Face 1, up -> Face 6 going right
            case [3, 1]:
                new_pos = (col + self.face_size * 2, 0)
                new_direction = 0  # Right

            # Face 2, up -> Face 6 going up
            case [3, 2]:
                new_pos = (self.face_size * 4 - 1, col - self.face_size * 2)
                new_direction = 3  # Up

            # Face 4, up -> Face 3 going right
            case [3, 4]:
                new_pos = (col + self.face_size, self.face_size)
                new_direction = 0  # Right

        return new_pos, new_direction

    @staticmethod
    def create_from_2dmap(monkey_map_2d: MonkeyMap2D, face_size: int):
        new_monkey_map = MonkeyMap3D(face_size=face_size)
        new_monkey_map.map = monkey_map_2d.map
        new_monkey_map.blocked = monkey_map_2d.blocked
        new_monkey_map.find_start()

        return new_monkey_map


def parse_map(map_input: list) -> MonkeyMap2D:
    new_monkey_map = MonkeyMap2D()
    for row, line in enumerate(map_input):
        for col, char in enumerate(line):
            if char == ".":
                new_monkey_map.add_free((row, col))
            elif char == "#":
                new_monkey_map.add_blocked((row, col))

    new_monkey_map.find_start()
    return new_monkey_map


def parse_instructions(instructions_input: str) -> list:
    last_pos = 0
    instructions = []

    for i, char in enumerate(instructions_input):
        if char == "R" or char == "L":
            instructions.append(int(instructions_input[last_pos:i]))
            instructions.append(instructions_input[i])
            last_pos = i + 1

    instructions.append(int(instructions_input[last_pos:]))
    return instructions


def run(monkey_map: MonkeyMap2D, instructions: list) -> int:

    for instruction in instructions:
        match instruction:
            case str() as direction:
                monkey_map.change_direction(direction == "R")
            case int() as steps:
                monkey_map.move(steps)

    return monkey_map.get_map_score()


def main():
    monkey_map_2d = parse_map(puzzle_input[:-2])
    instructions = parse_instructions(puzzle_input[-1])
    result_part1 = run(monkey_map_2d, instructions)
    monkey_map_3d = MonkeyMap3D.create_from_2dmap(monkey_map_2d, face_size=50)
    result_part2 = run(monkey_map_3d, instructions)

    assert result_part1 == 88268
    assert result_part2 == 124302

    print("Part1", result_part1)
    print("Part2", result_part2)


if __name__ == "__main__":
    main()
