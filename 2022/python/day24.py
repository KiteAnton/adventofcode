from collections import deque

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=24)


def parse_input(puzzle_input):
    blizzards_w = set()
    blizzards_e = set()
    blizzards_s = set()
    blizzards_n = set()
    end_row = len(puzzle_input) - 1
    start_pos = (0, 0)
    end_pos = (0, 0)
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if row == 0 and char == ".":
                start_pos = (row, col)
                continue
            elif row == end_row and char == ".":
                end_pos = (row, col)
                continue
            if char == "<":
                blizzards_w.add((row, col))
            elif char == ">":
                blizzards_e.add((row, col))
            elif char == "v":
                blizzards_s.add((row, col))
            elif char == "^":
                blizzards_n.add((row, col))

    blizzards = (blizzards_n, blizzards_e, blizzards_s, blizzards_w)
    return blizzards, start_pos, end_pos


def print_map(blizzards, start, end, current_position):

    print()
    blizzards_n, blizzards_e, blizzards_s, blizzards_w = blizzards
    left_wall = start[1] - 1
    right_wall = end[1] + 1
    bottom_wall = end[0]
    for row in range(start[0], end[0] + 1):
        line = ""
        for col in range(start[1] - 1, end[1] + 2):
            cur_pos_blizzards = []
            if (row, col) in blizzards_w:
                cur_pos_blizzards.append("<")
            if (row, col) in blizzards_e:
                cur_pos_blizzards.append(">")
            if (row, col) in blizzards_n:
                cur_pos_blizzards.append("^")
            if (row, col) in blizzards_s:
                cur_pos_blizzards.append("v")

            if len(cur_pos_blizzards) > 1:
                line += str(len(cur_pos_blizzards))
            elif len(cur_pos_blizzards) == 1:
                line += cur_pos_blizzards[0]
            elif current_position == (row, col):
                line += "E"
            elif (row, col) == start:
                line += "."
            elif (
                row == 0
                or col == left_wall
                or col == right_wall
                or row == bottom_wall
            ):
                line += "#"
            else:
                line += "."
        print(line)


blizzards_visited = {}


def move_blizzards(blizzards, minute, boundaries):
    if minute in blizzards_visited:
        return blizzards_visited[minute]

    (blizzards_n, blizzards_e, blizzards_s, blizzards_w) = blizzards
    new_blizzards_n = set()
    new_blizzards_s = set()
    new_blizzards_e = set()
    new_blizzards_w = set()

    for blizzard in blizzards_e:
        new_row = blizzard[0]
        new_col = (blizzard[1] - 1 + minute) % (boundaries[3] - 1) + 1
        new_blizzards_e.add((new_row, new_col))

    for blizzard in blizzards_w:
        new_row = blizzard[0]
        new_col = (blizzard[1] - 1 - minute) % (boundaries[3] - 1) + 1
        new_blizzards_w.add((new_row, new_col))

    for blizzard in blizzards_n:
        new_row = (blizzard[0] - 1 - minute) % (boundaries[1] - 1) + 1
        new_col = blizzard[1]
        new_blizzards_n.add((new_row, new_col))

    for blizzard in blizzards_s:
        new_row = (blizzard[0] - 1 + minute) % (boundaries[1] - 1) + 1
        new_col = blizzard[1]
        new_blizzards_s.add((new_row, new_col))

    result = (
        new_blizzards_n | new_blizzards_e | new_blizzards_s | new_blizzards_w
    )
    blizzards_visited[minute] = result
    return result


def next_pos_possible(position, blizzards, minute, boundaries, start, end):
    max_move_permutations = (boundaries[1] - 1) * (boundaries[3] - 1)
    minute = minute % max_move_permutations

    blizzards = move_blizzards(blizzards, minute, boundaries)

    if position in [start, end]:  # No blizzards in these positions
        return True

    row, col = position
    row_min, row_max, col_min, col_max = boundaries
    if (
        row_min < row < row_max
        and col_min < col < col_max
        and position not in blizzards
    ):
        return True

    return False


def solve(puzzle_input):
    blizzards_start, start, end = parse_input(puzzle_input)
    boundaries = (start[0], end[0], start[1] - 1, end[1] + 1)
    current_position = start
    minute = 0

    visited = set()

    current_state = (minute, current_position)
    move_queue = deque([current_state])

    end_pos = [end, start, end]
    end = end_pos.pop()
    part1_result = 0
    while move_queue:
        current_state = move_queue.popleft()
        minute, current_position = current_state

        if current_position == end:
            if part1_result == 0:
                part1_result = minute
                continue
            if len(end_pos) > 0:
                visited = set()
                start = end
                end = end_pos.pop()
                move_queue = deque([current_state])
                continue
            else:
                return part1_result, minute

        if current_state in visited:
            continue
        visited.add(current_state)

        new_positions = [
            current_position,  # Stay
            (current_position[0] + 1, current_position[1]),  # South
            (current_position[0] - 1, current_position[1]),  # North
            (current_position[0], current_position[1] + 1),  # East
            (current_position[0], current_position[1] - 1),  # West
        ]
        for new_pos in new_positions:
            if next_pos_possible(
                new_pos, blizzards_start, minute + 1, boundaries, start, end
            ):
                move_queue.append((minute + 1, new_pos))

    return part1_result, minute


def main():

    part1_result, part2_result = solve(puzzle_input)

    assert part1_result == 283
    assert part2_result == 883

    print("Part1:", part1_result)
    print("Part2:", part2_result)


if __name__ == "__main__":
    main()
