from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=6)


def find_unique_sliding_window(sequence, window_size):
    for index in range(window_size - 1, len(sequence)):
        if len(set(sequence[index - window_size : index])) == window_size:
            return index
    return 0


def main():
    part1 = find_unique_sliding_window(puzzle_input[0], 4)
    part2 = find_unique_sliding_window(puzzle_input[0], 14)
    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main()
