import itertools
from copy import copy

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=19)


def parse_inputs(puzzle_input):
    blueprints = {}
    for line in puzzle_input:
        words = line.split()
        blueprint_id = int(words[1][:-1])
        ore_cost = int(words[6])
        clay_cost = int(words[12])
        obsidian_cost = [int(words[18]), int(words[21])]
        geode_cost = [int(words[27]), int(words[30])]

        new_blueprint = {
            "id": blueprint_id,
            "ore_cost": ore_cost,
            "clay_cost": clay_cost,
            "obsidian_cost": obsidian_cost,
            "geode_cost": geode_cost,
        }
        blueprints[blueprint_id] = new_blueprint

    return blueprints


def build_robot(
    inventory: list, robot_collection: list, option: int, blueprint: dict
) -> list:
    new_robots = copy(robot_collection)
    new_robots[option] += 1

    if option == 0:
        inventory[0] -= blueprint["ore_cost"]

    elif option == 1:
        inventory[0] -= blueprint["clay_cost"]

    elif option == 2:
        inventory[0] -= blueprint["obsidian_cost"][0]
        inventory[1] -= blueprint["obsidian_cost"][1]

    elif option == 3:
        inventory[0] -= blueprint["geode_cost"][0]
        inventory[2] -= blueprint["geode_cost"][1]

    return new_robots


def can_build(inventory: list, option: int, blueprint: dict) -> bool:

    if option == 0:  # Ore
        if inventory[0] >= blueprint["ore_cost"]:
            return True

    elif option == 1:  # Clay
        if inventory[0] >= blueprint["clay_cost"]:
            return True

    elif option == 2:  # Obsidian
        if (
            inventory[0] >= blueprint["obsidian_cost"][0]
            and inventory[1] >= blueprint["obsidian_cost"][1]
        ):
            return True

    elif option == 3:  # Geode
        if (
            inventory[0] >= blueprint["geode_cost"][0]
            and inventory[2] >= blueprint["geode_cost"][1]
        ):
            return True

    return False


def harvest(inventory: list, robot_collection: list) -> None:
    # 0 ore, 1, clay, 2, obsidian, 3 geode

    inventory[0] += robot_collection[0]
    inventory[1] += robot_collection[1]
    inventory[2] += robot_collection[2]
    inventory[3] += robot_collection[3]


def most_expensive_robot_in_ore(blueprint: dict):
    return max(
        [
            blueprint["ore_cost"],
            blueprint["clay_cost"],
            blueprint["obsidian_cost"][0],
            blueprint["geode_cost"][0],
        ]
    )


visited = {}


def solve(
    time_left: int, robots: list, inventory: list, blueprint: dict
) -> int:

    if time_left <= 0:
        return inventory[3]

    # Cap states,
    # No need for more robots
    robots[0] = min(robots[0], most_expensive_robot_in_ore(blueprint))
    robots[1] = min(robots[1], blueprint["obsidian_cost"][1])
    robots[2] = min(robots[2], blueprint["geode_cost"][1])

    # Cap states
    # No need for more inventory then possible to spend
    inventory[0] = min(
        inventory[0],
        time_left * most_expensive_robot_in_ore(blueprint)
        - robots[0] * (time_left - 1),
    )
    inventory[1] = min(
        inventory[1],
        time_left * blueprint["obsidian_cost"][1]
        - robots[1] * (time_left - 1),
    )
    inventory[2] = min(
        inventory[2],
        time_left * blueprint["geode_cost"][1] - robots[2] * (time_left - 1),
    )

    state = tuple([time_left, *robots, *inventory])

    # Memoization
    if state in visited:
        return visited[state]

    # if visited and len(visited) % 10_000 == 0:
    #     number_formated = "{:,.0f}".format(len(visited))
    #     print(number_formated, state)

    assert (
        inventory[0] >= 0
        and inventory[1] >= 0
        and inventory[2] >= 0
        and inventory[3] >= 0
    )

    # If possible to buy geode, then do this and no other options this round
    if can_build(inventory=inventory, option=3, blueprint=blueprint):
        harvest(inventory=inventory, robot_collection=robots)
        new_robot_collection = build_robot(
            inventory=inventory,
            robot_collection=robots,
            option=3,
            blueprint=blueprint,
        )
        return solve(time_left - 1, new_robot_collection, inventory, blueprint)

    # Do nothing
    new_inventory = copy(inventory)
    robot_collection = copy(robots)
    harvest(new_inventory, robot_collection)
    result = solve(time_left - 1, robot_collection, new_inventory, blueprint)

    for option in [3, 2, 1, 0]:

        # Speed improvements
        # 1, Dont build more ore robots if already can produce the cost of the most
        # expensive robot in ore per round
        if option == 0 and robots[0] >= most_expensive_robot_in_ore(blueprint):
            continue
        # 2. Don't build more clay robots
        if option == 1 and robots[1] >= blueprint["obsidian_cost"][1]:
            continue

        if can_build(inventory=inventory, option=option, blueprint=blueprint):
            new_inventory = copy(inventory)
            robot_collection = copy(robots)

            harvest(inventory=new_inventory, robot_collection=robot_collection)
            new_robot_collection = build_robot(
                inventory=new_inventory,
                robot_collection=robot_collection,
                option=option,
                blueprint=blueprint,
            )
            new_result = solve(
                time_left - 1, new_robot_collection, new_inventory, blueprint
            )
            result = max(new_result, result)

            if option == 2:  # If obisidian built, no other options this round
                break

    visited[state] = result
    return result


def part1(blueprints):
    run_time = 24  # Minutes

    total_score = 0
    for blueprint_id, blueprint in blueprints.items():
        visited.clear()
        robots = [1, 0, 0, 0]
        inventory = [0, 0, 0, 0]
        score = solve(run_time, robots, inventory, blueprint)
        total_score += score * blueprint_id

    return total_score


def part2(blueprints):

    run_time = 32  # Minutes
    blueprints = dict(itertools.islice(blueprints.items(), 3))

    total_score = 1
    for blueprint in blueprints.values():
        visited.clear()
        robots = [1, 0, 0, 0]
        inventory = [0, 0, 0, 0]
        score = solve(run_time, robots, inventory, blueprint)
        total_score *= score

    return total_score


def main():
    blueprints = parse_inputs(puzzle_input)
    result_part1 = part1(blueprints)
    result_part2 = part2(blueprints)

    print("Part1:", result_part1)
    print("Part2:", result_part2)


if __name__ == "__main__":
    main()
