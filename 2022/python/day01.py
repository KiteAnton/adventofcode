from common import puzzle_input_as_int

puzzle_input = puzzle_input_as_int(day=1, separator="\n\n")


def main():
    inventories = sorted([sum(group) for group in puzzle_input], reverse=True)
    print("Part1:", inventories[0])
    print("Part2:", sum(inventories[:3]))


if __name__ == "__main__":
    main()
