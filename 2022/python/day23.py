from common import (
    find_adjacent_positions_2d,
    find_boundaries,
    puzzle_input_as_str,
)

puzzle_input = puzzle_input_as_str(day=23)


def has_neighbors(elf, elves):
    for pos in find_adjacent_positions_2d(elf, moves=8):
        if pos in elves:
            return True
    return False


def get_elves(puzzle_input):
    elves = set()
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if char == "#":
                elves.add((row, col))

    return elves


def can_move(elf, movement, elves) -> bool:
    new_positions = [
        [  # North
            (-1, -1),
            (-1, 0),
            (-1, 1),
        ],
        [  # South
            (1, -1),
            (1, 0),
            (1, 1),
        ],
        [  # West
            (-1, -1),
            (0, -1),
            (+1, -1),
        ],
        [  # East
            (-1, 1),
            (0, 1),
            (+1, 1),
        ],
    ]
    move_possible = True
    for pos in new_positions[movement]:
        try_pos = (elf[0] + pos[0], elf[1] + pos[1])
        if try_pos in elves:
            move_possible = False

    return move_possible


def move_around(elves: set, move: int) -> bool:
    new_positions = {}

    movements = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    no_move = True
    for elf in elves:
        if has_neighbors(elf, elves):
            no_move = False
            for i in range(4):
                movement = (move + i) % 4
                if can_move(elf, movement, elves):
                    new_pos = (
                        elf[0] + movements[movement][0],
                        elf[1] + movements[movement][1],
                    )
                    if new_pos not in new_positions:
                        new_positions[new_pos] = []
                    new_positions[new_pos].append(elf)
                    break

    for new_pos, old_pos in new_positions.items():
        if len(old_pos) == 1:
            elves.add(new_pos)
            elves.discard(old_pos[0])

    return no_move


def run(puzzle_input):
    elves = get_elves(puzzle_input)

    move = 0
    iteration = 0

    part1 = 0
    while True:
        all_done = move_around(elves, move)
        iteration += 1
        move = (move + 1) % 4
        if iteration == 10:
            ((row_min, row_max), (col_min, col_max)) = find_boundaries(elves)
            part1 = (row_max + 1 - row_min) * (col_max + 1 - col_min) - len(
                elves
            )
        if all_done:
            break

    part2 = iteration

    return part1, part2


def main():
    part1, part2 = run(puzzle_input)

    assert part1 == 4005
    assert part2 == 1008

    print("Part1:", part1)
    print("Part2:", part2)


if __name__ == "__main__":
    main()
