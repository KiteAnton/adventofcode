from collections import deque

from common import find_adjacent_positions_3d, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=18)


def parse_input(puzzle_input):
    cube_positions = set()

    for line in puzzle_input:
        x, y, z = list(map(int, line.split(",")))
        cube_positions.add((x, y, z))
    x_pos = [x for x, _, _ in cube_positions]
    y_pos = [y for _, y, _ in cube_positions]
    z_pos = [z for _, _, z in cube_positions]
    x_min = min(x_pos)
    x_max = max(x_pos)

    y_min = min(y_pos)
    y_max = max(y_pos)

    z_min = min(z_pos)
    z_max = max(z_pos)

    return cube_positions, (
        (x_min - 1, x_max + 1),
        (y_min - 1, y_max + 1),
        (z_min - 1, z_max + 1),
    )


def part1(cube_positions: set, boundaries: tuple) -> int:

    total_sides_exposed = 0

    for cube_pos in cube_positions:
        for option in find_adjacent_positions_3d(
            cube_pos, boundaries, moves=6
        ):
            if option not in cube_positions:
                total_sides_exposed += 1

    return total_sides_exposed


def part2(cube_positions: set, boundaries: tuple) -> int:

    total_sides_exposed = 0
    empty_space_outside = set()
    empty_space_inside = set()
    for cube_pos in cube_positions:
        for option in find_adjacent_positions_3d(
            cube_pos, boundaries, moves=6
        ):
            if option in empty_space_inside or option in cube_positions:
                continue

            if option in empty_space_outside or find_inside(
                option,
                cube_positions,
                empty_space_inside,
                empty_space_outside,
                boundaries,
            ):
                total_sides_exposed += 1

    return total_sides_exposed


def find_inside(
    position: tuple,
    cube_positions: set,
    inside: set,
    outside: set,
    boundaries: tuple,
) -> bool:

    ((x_min, x_max), (y_min, y_max), (z_min, z_max)) = boundaries
    queue = deque([position])

    visited = set()
    while queue:
        current_position = queue.popleft()
        x, y, z = current_position

        if (
            x <= x_min
            or x >= x_max
            or y <= y_min
            or y >= y_max
            or z <= z_min
            or z >= z_max
        ):
            outside.update(visited)
            return True

        if current_position in visited:
            continue
        visited.add(current_position)

        for option in find_adjacent_positions_3d(
            current_position, boundaries, moves=6
        ):
            if option not in cube_positions:
                queue.append(option)

    inside.update(visited)
    return False


def main():
    cube_positions, boundaries = parse_input(puzzle_input)
    result_part1 = part1(cube_positions, boundaries)
    result_part2 = part2(cube_positions, boundaries)
    print("Part1", result_part1)
    print("Part2", result_part2)


if __name__ == "__main__":
    main()
