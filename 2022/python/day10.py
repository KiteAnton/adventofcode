from common import fill_char, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=10)


class Device:
    def __init__(self, instructions):
        self.cycle = 1
        self.reg_x = 1
        self.signal_strength = 0
        self.instructions = instructions
        self.crt_output = ""

    def run(self):
        for line in self.instructions:
            match line.split():
                case ["noop"]:
                    self.step()
                case ["addx", value]:
                    value = int(value)
                    self.step()
                    self.step()
                    self.reg_x += value

    def step(self):
        if self.cycle in [20, 60, 100, 140, 180, 220]:
            self.signal_strength += self.cycle * self.reg_x

        crt_pos = (self.cycle - 1) % 40
        if crt_pos in [self.reg_x - 1, self.reg_x, self.reg_x + 1]:
            self.crt_output += fill_char
        else:
            self.crt_output += " "
        if crt_pos == 39:
            self.crt_output += "\n"
        self.cycle += 1


def main():
    device = Device(puzzle_input)
    device.run()
    print("Part 1:", device.signal_strength)
    print(device.crt_output)


if __name__ == "__main__":
    main()
