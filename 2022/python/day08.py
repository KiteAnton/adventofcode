from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=8)


def map_inputs_as_int(str_input_map):
    output = []
    for line in str_input_map:
        output.append(list(map(int, line)))
    return output


def part1(trees):
    trees_visible = 2 * len(trees) + 2 * len(trees[0]) - 4

    for row in range(1, len(trees) - 1):
        for col in range(1, len(trees[0]) - 1):
            tree_height = trees[row][col]

            if (
                # left
                tree_height > max(trees[row][col - 1 :: -1])
                # right
                or tree_height > max(trees[row][col + 1 :])
                # up
                or tree_height
                > max(
                    [tree_line[col] for tree_line in trees][row - 1 :: -1],
                )
                # down
                or tree_height
                > max([tree_line[col] for tree_line in trees][row + 1 :])
            ):
                trees_visible += 1

    return trees_visible


def view_distance(tree_height, tree_range):
    try:
        return (
            list(map(lambda x: x >= tree_height, tree_range)).index(True) + 1
        )
    except ValueError:
        return len(tree_range)


def part2(trees):
    max_scenic_score = 0

    for row in range(1, len(trees) - 1):
        for col in range(1, len(trees[0]) - 1):
            scenic_score = 1
            tree_height = trees[row][col]
            scenic_score *= view_distance(
                tree_height, trees[row][col - 1 :: -1]
            )
            scenic_score *= view_distance(tree_height, trees[row][col + 1 :])
            scenic_score *= view_distance(
                tree_height,
                [tree_line[col] for tree_line in trees][row - 1 :: -1],
            )
            scenic_score *= view_distance(
                tree_height, [tree_line[col] for tree_line in trees][row + 1 :]
            )
            max_scenic_score = max(max_scenic_score, scenic_score)

    return max_scenic_score


def main():
    tree_map = map_inputs_as_int(puzzle_input)
    print("Part 1:", part1(tree_map))
    print("Part 2:", part2(tree_map))


if __name__ == "__main__":
    main()
