from common import puzzle_input_as_int

puzzle_input = puzzle_input_as_int(day=20)


class Link:
    def __init__(self, number):
        self.number = number
        self.back = self
        self.forward = self


def linked_list_from_values(values: list) -> tuple:
    first_link = Link(values[0])

    prev_link = first_link
    zero_link = None
    for value in values[1:]:
        new_link = Link(value)
        if value == 0:
            zero_link = new_link
        new_link.back = prev_link
        prev_link.forward = new_link
        prev_link = new_link

    first_link.back = prev_link
    prev_link.forward = first_link

    return first_link, zero_link


def move_link(link, steps, first_link):
    if steps > 0:
        for _ in range(steps):
            a = link.back
            b = link
            c = link.forward
            d = c.forward

            a.forward = c
            b.back = c
            b.forward = d
            c.back = a
            c.forward = b
            d.back = b

            if link == first_link:
                first_link = c
    return first_link


def mix_sequence(puzzle_input, decryption_key_final=1, iterations=1):
    decryption_key = decryption_key_final

    initial_list = [number * decryption_key for number in puzzle_input]
    initial_list_len = len(initial_list)

    first_link, zero_link = linked_list_from_values(initial_list)

    working_list = [first_link]
    next_link = first_link.forward
    while next_link != first_link:
        working_list.append(next_link)
        next_link = next_link.forward

    for i in range(iterations):

        for link in working_list:
            steps = link.number % (initial_list_len - 1)
            first_link = move_link(link, steps, first_link)

    first_pos = 1000 % initial_list_len
    second_pos = 2000 % initial_list_len
    third_pos = 3000 % initial_list_len

    next_link = zero_link
    sum_of_values = 0
    for i in range(max(first_pos, second_pos, third_pos) + 1):
        if i == first_pos:
            sum_of_values += next_link.number
        if i == second_pos:
            sum_of_values += next_link.number
        if i == third_pos:
            sum_of_values += next_link.number

        next_link = next_link.forward

    return sum_of_values


def main():
    part1 = mix_sequence(puzzle_input)

    decryption_key_final = 811589153
    part2 = mix_sequence(
        puzzle_input, decryption_key_final=decryption_key_final, iterations=10
    )

    print("Part1:", part1)
    print("Part2:", part2)


if __name__ == "__main__":
    main()
