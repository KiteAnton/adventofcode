import ast
import functools

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=13, separator="\n\n")


def compare(left, right):
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        elif left > right:
            return -1
        else:
            return 0

    elif isinstance(left, list) and isinstance(right, int):
        return compare(left, [right])

    elif isinstance(left, int) and isinstance(right, list):
        return compare([left], right)

    for item1, item2 in zip(left, right):
        match = compare(item1, item2)
        if match != 0:
            return match

    if len(left) < len(right):
        return 1

    elif len(left) > len(right):
        return -1

    return 0


def part1(input_pairs):
    match_indices = []
    for pair_index in range(0, len(input_pairs), 2):
        left = input_pairs[pair_index]
        right = input_pairs[pair_index + 1]
        if compare(left, right) > 0:
            match_indices.append(pair_index // 2 + 1)

    return sum(match_indices)


def setup_list_pairs(puzzle_input):
    pairs = []
    for pair in puzzle_input:
        left = ast.literal_eval(pair[0])
        right = ast.literal_eval(pair[1])
        pairs.append(left)
        pairs.append(right)
    return pairs


def part2(input_pairs):
    input_pairs.append([[2]])
    input_pairs.append([[6]])

    input_pairs.sort(key=functools.cmp_to_key(compare), reverse=True)

    index_2 = input_pairs.index([[2]])
    index_6 = input_pairs.index([[6]])
    return (index_2 + 1) * (index_6 + 1)


def main():
    input_pairs = setup_list_pairs(puzzle_input)
    print("Part1", part1(input_pairs))
    print("Part2", part2(input_pairs))


if __name__ == "__main__":
    main()
