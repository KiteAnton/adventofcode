from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=7)


class Dir:
    def __init__(self, name, parent=None):
        self.internal_size = 0
        self.parent = parent
        self.childs = []
        self.name = name

    def add_child(self, child):
        child.parent = self
        self.childs.append(child)

    @property
    def size(self):
        return self.internal_size + sum([child.size for child in self.childs])

    def path(self):
        if self.parent:
            return self.parent.path() + "/" + self.name + "/"
        return self.name + "/"


def nodes_from_input(sequence):
    top_node = Dir("/")
    all_nodes = {"/": top_node}
    current_node = None
    for line in sequence:
        match line.split():
            case ["$", "ls"]:
                continue
            case ["dir", _]:
                continue
            case ["$", "cd", "/"]:
                current_node = top_node
            case ["$", "cd", ".."]:
                current_node = current_node.parent
            case ["$", "cd", directory]:
                new_name = current_node.path() + directory
                if new_name not in all_nodes:
                    new_node = Dir(directory, current_node)
                    all_nodes[new_name] = new_node
                    current_node.add_child(new_node)
                    current_node = new_node
            case [size, _]:
                current_node.internal_size += int(size)

    return all_nodes


def main():
    all_nodes = nodes_from_input(puzzle_input)
    part1 = sum(
        [node.size for node in all_nodes.values() if node.size <= 100000]
    )

    space_total = 70_000_000
    space_needed = 30_000_000

    space_missing = space_needed - (space_total - all_nodes["/"].size)

    part2 = min(
        [node.size for node in all_nodes.values() if node.size > space_missing]
    )

    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main()
