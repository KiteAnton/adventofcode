from copy import copy

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=16)


def parse_input(lines):
    valves = {}
    for line in lines:
        words = line.split()
        valve = words[1]
        flow_rate = int(words[4][5:-1])
        tunnels = [word[:2] for word in words[9:]]

        valves[valve] = {"flow_rate": flow_rate, "tunnels": tunnels}
    return valves


visited = {}


def solve(
    minute,
    closed_valves,
    current_node,
    valves,
    shortest_distances,
    elephant=False,
):

    if minute <= 0:
        if elephant:
            return solve(
                26,
                closed_valves,
                "AA",
                valves,
                shortest_distances,
                elephant=False,
            )
        return 0

    # Memoization
    current_state = tuple(
        [minute, tuple(closed_valves), current_node, elephant]
    )
    if current_state in visited:
        return visited[current_state]

    result = 0
    if visited and len(visited) % 10_000 == 0:
        print(len(visited) // 1_000, "x 1_000", current_state)

    # Traverse
    for option in closed_valves:
        if option == current_node:
            continue
        new_closed_valves = copy(closed_valves)
        new_result = solve(
            minute - shortest_distances[current_node][option],
            new_closed_valves,
            option,
            valves,
            shortest_distances,
            elephant,
        )
        result = max(new_result, result)

    # If current valve possible to open
    if current_node in closed_valves:
        new_closed_valves = copy(closed_valves)
        new_closed_valves.remove(current_node)
        new_result = solve(
            minute - 1,
            new_closed_valves,
            current_node,
            valves,
            shortest_distances,
            elephant,
        )
        new_result += valves[current_node]["flow_rate"] * (minute - 1)
        result = max(new_result, result)

    visited[current_state] = result
    return result


def floyd_warshall(valves):
    default_row = {}
    shortest_distances = {}

    # Initial state
    for key in valves.keys():
        default_row[key] = int(1e3)

    for key, valve in valves.items():
        new_row = copy(default_row)
        new_row[key] = 0
        for tunnel in valve["tunnels"]:
            new_row[tunnel] = 1

        shortest_distances[key] = new_row

    # Update
    for r in valves.keys():
        for p in valves.keys():
            for q in valves.keys():
                shortest_distances[p][q] = min(
                    shortest_distances[p][q],
                    shortest_distances[p][r] + shortest_distances[r][q],
                )

    return shortest_distances


def main(lines):
    valves = parse_input(lines)
    shortest_distances = floyd_warshall(valves)

    closed_valves = [
        key for key, value in valves.items() if value["flow_rate"] > 0
    ]
    minute = 30
    part1 = solve(
        minute, copy(closed_valves), "AA", valves, shortest_distances
    )

    minute = 26
    part2 = solve(
        minute,
        copy(closed_valves),
        "AA",
        valves,
        shortest_distances,
        elephant=True,
    )

    print("----------------------")
    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main(puzzle_input)
