from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=4)


def sections_from_str(input_string):
    part1, part2 = [int(value) for value in input_string.split("-")]
    return set(section for section in range(part1, part2 + 1))


def assignment_pair_sets(line):
    return [sections_from_str(assignment) for assignment in line.split(",")]


def main():
    elf_pairs1, elf_pairs2 = zip(*map(assignment_pair_sets, puzzle_input))

    part1 = sum(
        map(
            lambda x, y: x.issubset(y) or y.issubset(x), elf_pairs1, elf_pairs2
        )
    )
    part2 = sum(map(lambda x, y: len(x & y) > 0, elf_pairs1, elf_pairs2))
    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main()
