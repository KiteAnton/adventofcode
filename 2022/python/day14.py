from copy import copy

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=14)


def create_map(rock_walls):
    rock_map = set()
    x_min, x_max, y_min, y_max = 500, 500, 0, 0

    for line in rock_walls:
        coordinates = line.split(" -> ")

        for i in range(1, len(coordinates)):
            x1, y1 = coordinates[i - 1].split(",")
            x2, y2 = coordinates[i].split(",")
            x1, x2 = min(int(x1), int(x2)), max(int(x1), int(x2))
            y1, y2 = min(int(y1), int(y2)), max(int(y1), int(y2))
            for k in range(x1, x2 + 1):
                rock_map.add((k, y1))
                x_min = min(x_min, k)
                x_max = max(x_max, k)

            for k in range(y1, y2 + 1):
                rock_map.add((x1, k))
                y_min = min(y_min, k)
                y_max = max(y_max, k)

    return rock_map, tuple([x_min, x_max, y_min, y_max])


def add_floor(rock_map, map_range):
    rock_map = copy(rock_map)
    x_min, x_max, y_min, y_max = map_range
    x_min -= y_max
    x_max += y_max

    for x in range(x_min, x_max + 1):
        rock_map.add((x, y_max + 2))

    return rock_map, tuple([x_min, x_max, y_min, y_max + 2])


def add_sand(occupied, map_range):

    if (499, 1) in occupied and (501, 1) in occupied:
        occupied.add((500, 0))
        return False

    sand_pos = (500, 0)

    while sand_pos[1] <= map_range[3]:
        x, y = sand_pos
        if (x, y + 1) not in occupied:
            sand_pos = (x, y + 1)
        elif (x - 1, y + 1) not in occupied:
            sand_pos = (x - 1, y + 1)
        elif (x + 1, y + 1) not in occupied:
            sand_pos = (x + 1, y + 1)
        else:
            break

    if sand_pos[1] < map_range[3]:
        occupied.add(sand_pos)
        return True

    return False


def fill_sand(rock_map, map_range):
    occupied = copy(rock_map)
    while add_sand(occupied, map_range):
        pass

    return len(occupied) - len(rock_map)


def main():
    rock_map, map_range = create_map(puzzle_input)

    print("Part1", fill_sand(rock_map, map_range))

    rock_map, map_range = add_floor(rock_map, map_range)
    print("Part2", fill_sand(rock_map, map_range))


if __name__ == "__main__":
    main()
