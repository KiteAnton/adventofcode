import string

from common import find_common_char_in_strings, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=3)

priorities = string.ascii_lowercase + string.ascii_uppercase


def find_priority(rucksack_contents: str) -> int:
    common_char = find_common_char_in_strings(
        rucksack_contents[: len(rucksack_contents) // 2],
        rucksack_contents[len(rucksack_contents) // 2 :],
    )
    return priorities.index(common_char) + 1


def find_common_in_group(group_rucksacks):
    common_char = find_common_char_in_strings(
        group_rucksacks[0], group_rucksacks[1], group_rucksacks[2]
    )
    return priorities.index(common_char) + 1


def main():
    part1 = sum(map(find_priority, puzzle_input))

    elf_groups = [
        [x, y, z]
        for x, y, z in zip(
            puzzle_input[0::3], puzzle_input[1::3], puzzle_input[2::3]
        )
    ]
    part2 = sum(map(find_common_in_group, elf_groups))
    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main()
