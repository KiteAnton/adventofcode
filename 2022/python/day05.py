import collections
import string
from copy import deepcopy

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=5, separator="\n\n")


def setup_crates(crate_input):
    stacks = [collections.deque() for _ in range(9)]
    for line in crate_input:
        crates = line[1::4]
        for stack, crate in enumerate(crates):
            if crate in string.ascii_letters:
                stacks[stack].append(crate)
    return stacks


def move_crates(stacks, move_inputs, CrateMover=9000):
    for move in move_inputs:
        move_count, from_stack, to_stack = [
            int(x) for x in move.split() if x.isnumeric()
        ]
        temp = collections.deque()
        for _ in range(move_count):
            temp.append(stacks[from_stack - 1].popleft())

        for _ in range(move_count):
            if CrateMover == 9001:
                stacks[to_stack - 1].appendleft(temp.pop())
            else:
                stacks[to_stack - 1].appendleft(temp.popleft())


def main():
    crate_config, move_inputs = puzzle_input
    stacks = setup_crates(crate_config[:-1])
    stacks_copy = deepcopy(stacks)
    move_crates(stacks, move_inputs)
    part1 = "".join([stack[0] for stack in stacks if stack])

    stacks = stacks_copy
    move_crates(stacks, move_inputs, CrateMover=9001)
    part2 = "".join([stack[0] for stack in stacks if stack])

    print("Part 1:", part1)
    print("Part 2:", part2)


if __name__ == "__main__":
    main()
