import string
from collections import defaultdict, deque
from copy import copy, deepcopy
from pprint import pprint

from common import (
    find_common_char_in_strings,
    manhattan_distance,
    puzzle_input_as_int,
    puzzle_input_as_str,
)

puzzle_input = puzzle_input_as_str(day=5)
puzzle_input_test = puzzle_input_as_str(day=5, test=1)

puzzle_input = puzzle_input_as_str(day=5, separator="\n\n")
puzzle_input_test = puzzle_input_as_str(day=5, separator="\n\n", test=1)

puzzle_input = puzzle_input_as_int(day=5)
puzzle_input_test = puzzle_input_as_int(day=5, test=1)

puzzle_input = puzzle_input_as_int(day=5, separator="\n\n")
puzzle_input_test = puzzle_input_as_int(day=5, separator="\n\n", test=1)


def part1(puzzle_input):
    return


def part2(puzzle_input):
    return


def main():
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))


if __name__ == "__main__":
    main()

print()
print("Test 1:", part1(puzzle_input_test))
print("Part 1:", part1(puzzle_input))
print("-------------------")
print("Test 2:", part2(puzzle_input_test))
print("Part 2:", part2(puzzle_input))
