import types
from copy import deepcopy
from functools import reduce

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=11, separator="\n\n")


def add_value(self, item):
    return item + self.value


def mul_value(self, item):
    return item * self.value


def x2_value(_, item):
    return item * item


class Monkey:
    def __init__(self, number=None):
        self.number = number
        self.items = []
        self.value = -1
        self.test = -1
        self.iterations = 0

        if number:
            self.monkey_if_true = Monkey()
            self.monkey_if_false = Monkey()

    def throw_items(self, part2=False, modulo_value=1):
        for item in self.items:
            self.iterations += 1
            new_value = self.new_item_value(item)
            if part2:
                new_value %= modulo_value
            else:
                new_value = new_value // 3

            if new_value % self.test:
                self.monkey_if_false.items.append(new_value)
            else:
                self.monkey_if_true.items.append(new_value)

        self.items.clear()

    def new_item_value(self, item):
        return item


def game_setup(monkey_setup):
    monkeys = {}

    # Preallocate the monkeys
    for monkey_number in range(len(monkey_setup)):
        monkeys[monkey_number] = Monkey(monkey_number)

    for monkey in monkey_setup:
        monkey_number = int(monkey[0][7:-1])
        new_monkey = monkeys[monkey_number]

        for item in [int(item) for item in monkey[1].split(":")[1].split(",")]:
            new_monkey.items.append(item)

        match monkey[2].split(":")[1].split(" = ")[1].split()[1:]:
            case ["+", value]:
                new_monkey.new_item_value = types.MethodType(
                    add_value, new_monkey
                )
                new_monkey.value = int(value)
            case ["*", "old"]:
                new_monkey.new_item_value = types.MethodType(
                    x2_value, new_monkey
                )
            case ["*", value]:
                new_monkey.new_item_value = types.MethodType(
                    mul_value, new_monkey
                )
                new_monkey.value = int(value)

        # Test
        new_monkey.test = int(monkey[3].split()[-1])
        monkey_if_true = int(monkey[4].split()[-1])
        monkey_if_false = int(monkey[5].split()[-1])
        new_monkey.monkey_if_true = monkeys[monkey_if_true]
        new_monkey.monkey_if_false = monkeys[monkey_if_false]

        monkeys[monkey_number] = new_monkey

    return monkeys


def run_game(monkeys, part2=False, modulo_value=1):
    iterations = 10000 if part2 else 20
    for _ in range(iterations):
        for monkey in monkeys.values():
            monkey.throw_items(part2, modulo_value)

    monkey_business = sorted(
        [monkey.iterations for monkey in monkeys.values()], reverse=True
    )
    return monkey_business[0] * monkey_business[1]


def main():
    monkeys = game_setup(puzzle_input)
    print("Part1:", run_game(deepcopy(monkeys)))

    monkey_test_values = [monkey.test for monkey in monkeys.values()]
    modulo_value = reduce(lambda x, y: x * y, monkey_test_values)

    print("Part2", run_game(monkeys, part2=True, modulo_value=modulo_value))


if __name__ == "__main__":
    main()
