from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=2)


def part1(game_rounds):
    score_map = {
        "A Y": 2 + 6,
        "A X": 1 + 3,
        "A Z": 3 + 0,
        "B Y": 2 + 3,
        "B X": 1 + 0,
        "B Z": 3 + 6,
        "C Y": 2 + 0,
        "C X": 1 + 6,
        "C Z": 3 + 3,
    }
    return sum(map(lambda round: score_map[round], game_rounds))


def part2(game_rounds):
    score_map = {
        "A Y": 1 + 3,
        "A X": 3 + 0,
        "A Z": 2 + 6,
        "B Y": 2 + 3,
        "B X": 1 + 0,
        "B Z": 3 + 6,
        "C Y": 3 + 3,
        "C X": 2 + 0,
        "C Z": 1 + 6,
    }
    return sum(map(lambda round: score_map[round], game_rounds))


def main():
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))


if __name__ == "__main__":
    main()
