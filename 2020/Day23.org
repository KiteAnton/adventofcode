#+TITLE: Day 23: Crab Cups
#+STARTUP: overview

- [[https://adventofcode.com/2020/day/23][https://adventofcode.com/2020/day/23]]

* Problem part 1
Play the game crab cups with the small crab.
For the game there is a set of cups arranged in a circle clockwise. Each cup has an integer label.
For each move in the game:
- The crab pix up the next 3 cups clockwise from the current cup. These cups are removed from the circle.
- Then a destination cup is selected, the destination will be the current cups label - 1.
  - If this value is within the three picked up cups then subtract 1 until it is not.
    - If the destination cup is lower than the lowest value in the game then the highest cup will be selected.
- The picked up cups is placed immidiately clockwise of the destination cup. They keep the same order as when they got picked up.
- The next current cup is the one immidiately to the right of the current cup.

After 100 moves, in what order is the cups starting from the first one immidiately clockwise of the cup labeled 1.
** Test input
Test example, the starting cups is ~389125467~. Then with 10 moves.
#+begin_src
-- move 1 --
cups: (3) 8  9  1  2  5  4  6  7
pick up: 8, 9, 1
destination: 2

-- move 2 --
cups:  3 (2) 8  9  1  5  4  6  7
pick up: 8, 9, 1
destination: 7

-- move 3 --
cups:  3  2 (5) 4  6  7  8  9  1
pick up: 4, 6, 7
destination: 3

-- move 4 --
cups:  7  2  5 (8) 9  1  3  4  6
pick up: 9, 1, 3
destination: 7

-- move 5 --
cups:  3  2  5  8 (4) 6  7  9  1
pick up: 6, 7, 9
destination: 3

-- move 6 --
cups:  9  2  5  8  4 (1) 3  6  7
pick up: 3, 6, 7
destination: 9

-- move 7 --
cups:  7  2  5  8  4  1 (9) 3  6
pick up: 3, 6, 7
destination: 8

-- move 8 --
cups:  8  3  6  7  4  1  9 (2) 5
pick up: 5, 8, 3
destination: 1

-- move 9 --
cups:  7  4  1  5  8  3  9  2 (6)
pick up: 7, 4, 1
destination: 5

-- move 10 --
cups: (5) 7  4  1  8  3  9  2  6
pick up: 7, 4, 1
destination: 3

-- final --
cups:  5 (8) 3  7  4  1  9  2  6
#+end_src
I.e. after 10 moves the order of the cups immidiately clockwise of 1 would be ~92658374~. (excluding 1)
After 100 moves the order would be ~67384529~.

** Solution part 1
- Read input file (i.e. /./input/23/)
- Create a dictionary/lookup table with the cup label as key and the next/clockwise cup as value.
  - Not using a more complex structure (e.g. linked list) will work for part 1 but re-using solutions for part 2 where the amount of both cups and rounds are significantly larger then that solution will not be fast enough.
- For the lines of the input, creata key/value pair in the dictionary.
  - With the last entry, create a key/value pair linking back to first cup.
  - Return the key for first cup.
- Then make 100 rounds of the game.
  - For each round, using the lookup table. Get the three next values and store these. Change the value for the current key to the one 'linked' from the third picked up value.
  - Find the destination value.
    - Subtract 1 from the current cup until this value is not any of the three picked up values and not smaller than 1. If it is smaller than 1 retry with the highest number in the list (and iterater until this value is not in any of the picked up values).
    - With the destination value.
      - Get the value from the destination key. Re-assing this value to the key for the third picked up cup.
      - Set the key/value for the destination key to the value of the first picked up cup/value.
  - Re-start round with taking the value from the current cup as key and set that as current cup for next round.

- After all rounds, find the value for key 1 in the dictionary and iterate through each key/value until reaching value 1 again. Concatenate all the values together and this is the answer to part 1.

Answer to part 1: 28946753

* Problem part 2
Game setup is almost the same as for part 1 but with a few modifications.
- After setting up all the cups in the input/starting sequence all the remaining values up until one million is added. Values are added started with the one next after the highest one in the starting cups and adding one for each iteration up until all value 1 - 1 000 000 is used.
- 10 000 000 rounds of the game shall be played.

Answer to part 2 is to multiply the next two values after value 1 together.

** Test input
Same example input as for part 1.

After 10 million rounds the two values next after value 1 in this example would then be 934001 and 159792. Multiplying these toghether, 149245887792.

** Solution part 2
- Read input file (i.e. /./input/23/)
- The large set of cups and the large amount of rounds for part 2 leaves little room for slow/complex data structures. I.e. a lookup table/dictionary needs to used.
  - The dictionary should have the cup label as key and the next cup as the value.
- After reading the input, add the remaining values up until 1 000 000 to the dictionary/lookup table.

- Run game 10 000 000 rounds,
- After all rounds completed. Lookup value for key 1 and then the value for the next after that.
  - Multiply these numbers together.

Answer to part 2: 519044017360
* Solutions
- [[file:python/day23.py][Python 2020 Day 23]]
