#+TITLE: Day02 - Password Philosophy
#+STARTUP: overview


[[https://adventofcode.com/2020/day/2][https://adventofcode.com/2020/day/2]]

* Problem part 1
*Find the number of valid passwords*, given a set of passwords together with their defined policy ([[file:input/02][i.e. game input]]).

** Password policy, part 1
Given for each line, *a-b c: password*. Where
- Character *c* must be *between* *a* and *b* times in *password*

** Test input part 1
The following test input:
#+begin_src
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
#+end_src
Has *2* valid passports. Middle passport *cdefg* is not valid since it *needs at least 1 b*.

** Solution part 1
Input file specified in *input/02*

- Loop through lines/text and separate into:
  - Rules (i.e. min/max occurrences)
  - Letter to check
  - String to test
- Check rule against string.
  - Find the number of occurences of specified letter in the given string.
  - If the number is within specified range then increase *counter for valid passwords*.

Answer to part1: *600*

* Problem part 2
Problem is similar as for part 1 but password policy changed.

** Password policy, part 2
Given for each line, *a-b c: password*. Where
- Character *c* must be present *exactly one* times on either position *a* or *b* in *password*. Note first characters start at index 1

** Test input
Test input same as for part1,
- 1-3 a : abcde. *Valid*,
  - Position 1 contains an a, position 3 does not. => One occurence according to rule.
- 1-3 a : abcde. *Invalid*
  - Neither position 1 nor position 3 in *abcde* contains an *a*.
- 2-9 c: ccccccccc. *Invalid*
  - Both position 2 and 9 contains c.

** Solution part 2
Input file specified in *input/02*

- Loop through lines/text and separate into:
  - Rules (i.e. position a/position b)
  - Letter to check
  - String to test

- Check rule against string. Make sure letter exists only in either position a or b. If correct then increase counter.

Answer to part2: *245*
* Finished solutions
- [[file:python/day02.py][Python 2020 Day 2]]
