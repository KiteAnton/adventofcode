from common import puzzle_input_as_str


starting_cups = puzzle_input_as_str(23)

cups = {}
def play_round_large(current_cup, max_value):
    cup = current_cup
    next_cup = cups[cup]
    second_cup = cups[next_cup]
    third_cup = cups[second_cup]

    cups[current_cup] = cups[third_cup]
    destination_value = current_cup - 1

    while destination_value == next_cup or \
          destination_value == second_cup or \
          destination_value == third_cup or \
          destination_value <= 0:
        destination_value -= 1
        if destination_value < 1:
            destination_value = max_value

    next_after = cups[destination_value]
    cups[destination_value] = next_cup
    cups[third_cup] = next_after
    return cups[current_cup]


def cups_from_list(cup_list):
    first_cup = None
    prev_cup = None
    new_cup = None
    for value in cup_list:
        if not first_cup:
            first_cup = value
        else:
            new_cup = value
            if prev_cup:
                cups[prev_cup] = new_cup
            else:
                cups[first_cup] = new_cup
            prev_cup = new_cup

    cups[new_cup] = first_cup

    return first_cup


def cups_from_input(puzzle_input):
    cup_list = [int(value) for value in puzzle_input]
    return cups_from_list(cup_list)


def part1(puzzle_input):
    cups.clear()
    first_cup = cups_from_input(puzzle_input)

    cup_total = len(puzzle_input)

    current_cup = first_cup
    for _ in range(100):
        current_cup = play_round_large(current_cup, cup_total)

    result = ''

    cup = cups[1]
    while cup != 1:
        result += str(cup)
        cup = cups[cup]
    return result

def part2(puzzle_input):
    cups.clear()
    cup_total = 1_000_000
    number_cups = len(puzzle_input)
    cup_list = [int(value) for value in puzzle_input]
    cup_values = set(cup_list)

    value_to_add = max(cup_values)
    for _ in range(number_cups, cup_total):
        while value_to_add in cup_values:
            value_to_add += 1
        cup_list.append(value_to_add)
        value_to_add += 1
    first_cup = cups_from_list(cup_list)

    total_games = 10_000_000
    current_cup = first_cup
    for _ in range(total_games):
        next_cup = cups[current_cup]
        second_cup = cups[next_cup]
        third_cup = cups[second_cup]

        cups[current_cup] = cups[third_cup]
        destination_value = current_cup - 1

        while destination_value == next_cup or \
            destination_value == second_cup or \
            destination_value == third_cup or \
            destination_value <= 0:
            destination_value -= 1
            if destination_value < 1:
                destination_value = cup_total

        next_after = cups[destination_value]
        cups[destination_value] = next_cup
        cups[third_cup] = next_after
        current_cup = cups[current_cup]

    next_cup = cups[1]
    next_next_cup = cups[next_cup]

    result = next_cup * next_next_cup

    return result

part1_answer = part1(starting_cups[0])
part2_answer = part2(starting_cups[0])
print("Part 1:", part1_answer)
print("Part 2:", part2_answer)


# import cProfile
# cProfile.run('print("Part 2:", part2(starting_cups[0]))')
