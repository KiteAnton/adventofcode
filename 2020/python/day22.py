from common import puzzle_input_by_newlines

starting_cards = puzzle_input_by_newlines(22)


def play_round(deck1, deck2):
    card_deck1 = deck1.pop(0)
    card_deck2 = deck2.pop(0)

    if card_deck1 > card_deck2:
        deck1.append(card_deck1)
        deck1.append(card_deck2)
    else:
        deck2.append(card_deck2)
        deck2.append(card_deck1)


def play_game_recursive_combat(deck1, deck2):
    player1_hands = set()
    player2_hands = set()

    while True:
        if tuple(deck1) in player1_hands or tuple(deck2) in player2_hands:
            return 1  # Player 1 wins game

        player1_hands.add(tuple(deck1))
        player2_hands.add(tuple(deck2))

        if len(deck1) - 1 >= deck1[0] and len(deck2) - 1 >= deck2[0]:
            card_deck1 = deck1.pop(0)
            card_deck2 = deck2.pop(0)

            sub_deck1 = deck1[:card_deck1]
            sub_deck2 = deck2[:card_deck2]

            winner_subgame = play_game_recursive_combat(sub_deck1, sub_deck2)
            if winner_subgame == 1:
                deck1.append(card_deck1)
                deck1.append(card_deck2)
            else:
                deck2.append(card_deck2)
                deck2.append(card_deck1)

        else:
            play_round(deck1, deck2)

        if not deck2:
            return 1
        elif not deck1:
            return 2


def part1(puzzle_input):
    deck1 = [int(card) for card in puzzle_input[0][1:]]
    deck2 = [int(card) for card in puzzle_input[1][1:]]

    while deck1 and deck2:
        play_round(deck1, deck2)

    if deck1:
        winning_deck = deck1
    else:
        winning_deck = deck2

    result = 0
    score_multiplier = 1
    while winning_deck:
        result += winning_deck.pop() * score_multiplier
        score_multiplier += 1
    return result

def part2(puzzle_input):
    deck1 = [int(card) for card in puzzle_input[0][1:]]
    deck2 = [int(card) for card in puzzle_input[1][1:]]

    winner = play_game_recursive_combat(deck1, deck2)

    if winner == 1:
        winning_deck = deck1
    else:
        winning_deck = deck2

    result = 0
    score_multiplier = 1
    while winning_deck:
        result += winning_deck.pop() * score_multiplier
        score_multiplier += 1
    return result


print("Part 1:", part1(starting_cards))
print("Part 2:", part2(starting_cards))
