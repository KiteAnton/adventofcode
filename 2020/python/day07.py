from common import puzzle_input_as_str

lines = puzzle_input_as_str(7)

def bag_contained_in(bag_rules_raw):
    contained_in = {}
    for line in bag_rules_raw:
        words = line.split()
        color_id = f"{words[0]} {words[1]}"
        word_index = 5

        while word_index < len(words):
            contain_bags = f"{words[word_index]} {words[word_index+1]}"
            if contain_bags not in contained_in:
                contained_in[contain_bags] = set()
            contained_in[contain_bags].add(color_id)
            word_index += 4
    return contained_in

def can_contain(contained_in, bag_type):
    bag_alternatives = set()
    alternatives_to_check = set(contained_in[bag_type])

    while alternatives_to_check:
        alternative = alternatives_to_check.pop()
        if alternative not in bag_alternatives:
            bag_alternatives.add(alternative)
            if alternative in contained_in:
                alternatives_to_check.update(contained_in[alternative])
    return bag_alternatives


def bag_rules(bag_rules_raw):
    bag_rules = {}
    for line in bag_rules_raw:
        words = line.split()
        color_id = f"{words[0]} {words[1]}"
        bag_rules[color_id] = {}

        word_index = 4
        while word_index < len(words):
            if words[word_index] == 'no':
                amount = 0
                word_index += 4
                continue
            amount = int(words[word_index])
            bag_color_id = f"{words[word_index+1]} {words[word_index+2]}"
            bag_rules[color_id][bag_color_id] = amount
            word_index += 4
    return bag_rules


cached_result = {}
def find_including_bags(br, bag_type):
    nr_of_bags = 1  # Start with 1
    for bag, amount in br[bag_type].items():
        if bag in cached_result:
            nr_of_bags += amount * cached_result[bag]
            continue
        amount_for_bag = find_including_bags(br, bag)
        nr_of_bags += amount * amount_for_bag
        cached_result[bag] = amount_for_bag

    return nr_of_bags

def part1(bag_rules_raw):
    contained_in = bag_contained_in(bag_rules_raw)
    return len(can_contain(contained_in, "shiny gold"))

def part2(bag_rules_raw):
    br = bag_rules(bag_rules_raw)
    included_bags = find_including_bags(br, "shiny gold") - 1
    return included_bags

print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
