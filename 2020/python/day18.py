from common import puzzle_input_as_str

math_expressions = puzzle_input_as_str(18)

def evaluate_complex_expression(string, advanced=False):
    parens_stack = []
    string_list = list(string)
    while string_list:
        for i, char in enumerate(string_list):
            if char == '(':
                parens_stack.append(i)
            elif char == ')':
                str_start = parens_stack.pop()
                expr_result = evaluate_simple_expression(string[str_start+1:i], advanced)
                string = string[:str_start] + str(expr_result) + string[i+1:]
                string_list = list(string)
                break
        else:
            return evaluate_simple_expression(string, advanced)
    return 0

def evaluate_simple_expression_advanced(expression):
    expressions = expression.split(' ')

    while True:
        for i, expr in enumerate(expressions):
            if expr == '+':
                a = int(expressions.pop(i-1))
                b = int(expressions.pop(i))
                expressions[i-1] = str(a + b)
                break
        else:
            return evaluate_simple_expression(' '.join(expressions))


def evaluate_simple_expression(expression, advanced=False):
    if advanced:
        return evaluate_simple_expression_advanced(expression)

    expressions = expression.split(' ')
    result = int(expressions.pop(0))

    while len(expressions):
        operator = expressions.pop(0)
        if operator == '+':
            result += int(expressions.pop(0))
        elif operator == '*':
            result *= int(expressions.pop(0))
        elif operator == '-':
            result -= int(expressions.pop(0))
    return result

def part1(expressions):
    result = 0
    for line in expressions:
        result += evaluate_complex_expression(line)
    return result

def part2(expressions):
    result = 0
    for line in expressions:
        result += evaluate_complex_expression(line, advanced=True)
    return result


print("Part 1:", part1(math_expressions))
print("Part 2:", part2(math_expressions))
