from common import puzzle_input_as_str


door_card_public_keys = puzzle_input_as_str(25)

def calculate_loop_size(key):
    key = int(key)
    subject = 7
    loop_counter = 0

    value = 1

    while value != key:
        value *= subject
        value %= 20201227
        loop_counter += 1

    return loop_counter


def calculate_key(subject, loop_size):
    subject = int(subject)
    value = 1
    for _ in range(loop_size):
        value *= subject
        value %= 20201227
    return value



def part1(puzzle_input):
    card_public_key = puzzle_input[0]
    door_public_key = puzzle_input[1]
    door_loop_size = calculate_loop_size(door_public_key)

    encryption_key = calculate_key(card_public_key, door_loop_size)
    return encryption_key


print("Part 1:", part1(door_card_public_keys))
