from common import puzzle_input_as_str

tiles_to_flip = puzzle_input_as_str(24)

def coordinates_from_string(coordinate_string):
    col = 0
    row = 0

    while coordinate_string:
        if coordinate_string[0] in {'n', 's'}:
            step_size = 2
        else:
            step_size = 1
        step, coordinate_string = coordinate_string[:step_size], coordinate_string[step_size:]

        if step == 'e':
            col += 2
        elif step == 'w':
            col -= 2
        elif step == 'ne':
            col += 1
            row += 1
        elif step == 'se':
            col += 1
            row -= 1
        elif step == 'nw':
            col -= 1
            row += 1
        elif step == 'sw':
            col -= 1
            row -= 1
        else:
            print("Error:", step, coordinate_string)
    return (row, col)


def flip_tiles(tiles, coordinate_string):
    coordinates = coordinates_from_string(coordinate_string)
    if coordinates not in tiles:
        tiles[coordinates] = True
    else:
        tiles[coordinates] = not tiles[coordinates]


def calculate_adjacent_black(tiles, pos):
    result = 0

    adj_pos = [(0, 2), (-1, 1), (-1, -1), (0, -2), (1, -1), (1, 1)]

    for test_pos in adj_pos:
        check_pos = (pos[0] + test_pos[0], pos[1] + test_pos[1])
        if check_pos in tiles and tiles[check_pos]:
            result += 1

    return result


def add_empty_adjacent_tiles(tiles):
    adj_pos = [(0, 2), (-1, 1), (-1, -1), (0, -2), (1, -1), (1, 1)]
    tiles_copy = tiles.copy()
    for pos in tiles_copy.keys():
        for test_pos in adj_pos:
            check_pos = (pos[0] + test_pos[0], pos[1] + test_pos[1])
            if check_pos not in tiles:
                tiles[check_pos] = False


def flip_by_day(tiles):
    add_empty_adjacent_tiles(tiles)
    tiles_copy = tiles.copy()
    for pos, value in tiles.items():
        adjacent_black = calculate_adjacent_black(tiles_copy, pos)
        if value and (adjacent_black == 0 or adjacent_black > 2):
            tiles[pos] = False
        elif not value and adjacent_black == 2:
            tiles[pos] = True


def part1(puzzle_input):
    tiles = {}
    for line in puzzle_input:
        flip_tiles(tiles, line)

    result = 0
    for value in tiles.values():
        if value: result += 1
    return result

def part2(puzzle_input):
    tiles = {}
    for line in puzzle_input:
        flip_tiles(tiles, line)

    for _ in range(100):
        flip_by_day(tiles)


    result = 0
    for value in tiles.values():
        if value: result += 1
    return result


print("Part 1:", part1(tiles_to_flip))
print("Part 2:", part2(tiles_to_flip))
