from common import puzzle_input_by_newlines

groups = puzzle_input_by_newlines(6)

def unique_answers_by_group(group_answers):
    unique_answers = set()
    for line in group_answers:
        unique_answers |= set(line)
    return len(unique_answers)

def common_answers_by_group(group_answers):
    common_answers = set(group_answers[0])
    for line in group_answers:
        common_answers &= set(line)
    return len(common_answers)


def part1(group_answers):
    total_count = 0
    for group in group_answers:
        total_count += unique_answers_by_group(group)
    return total_count


def part2(group_answers):
    total_count = 0
    for group in group_answers:
        total_count += common_answers_by_group(group)
    return total_count

print("Part 1:", part1(groups))
print("Part 2:", part2(groups))
