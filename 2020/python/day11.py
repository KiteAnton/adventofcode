import copy
from common import puzzle_input_as_str

lines = puzzle_input_as_str(11)
def calculate_occupied_adjacent_seats(row, col, layout):
    occupied_seats = 0
    for r_i in range(max(0, row-1), min(len(layout), row+2)):
        for c_i in range(max(0, col-1), min(len(layout[0]), col+2)):
            if r_i != row or c_i != col:
                if layout[r_i][c_i] == '#':
                    occupied_seats += 1
    return occupied_seats

def calculate_occupied_seats_in_line(row, col, layout):
    occupied_seats = 0
    layout_max_y = len(layout) - 1
    layout_max_x = len(layout[0]) - 1
    sight_vectors = [(-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)]

    for vector in sight_vectors:
        occupied = False
        move_y, move_x = vector
        x = col + move_x
        y = row + move_y

        while 0 <= y <= layout_max_y and 0 <= x <= layout_max_x:
            char = layout[y][x]
            if char == '#':
                occupied = True
                break
            elif char == 'L':
                break
            y += move_y
            x += move_x
        if occupied:
            occupied_seats += 1

    return occupied_seats

def calculate_occupied_seats(layout):
    occupied_seats = 0
    for row in layout:
        for col in row:
            if col == '#':
                occupied_seats += 1
    return occupied_seats

def run_seat_iteration(seat_layout,
                       occupied_function=calculate_occupied_adjacent_seats,
                       occupied_threshold=4):
    changed = False
    seat_iteration = copy.deepcopy(seat_layout)

    for line_index, line in enumerate(seat_iteration):
        for char_index, char in enumerate(line):
            if char == ".":
                continue
            adjacent_occupied = occupied_function(line_index, char_index, seat_iteration)
            if char == '#' and adjacent_occupied >= occupied_threshold:
                seat_layout[line_index][char_index] = 'L'
                changed = True
            elif char == 'L' and adjacent_occupied == 0:
                seat_layout[line_index][char_index] = '#'
                changed = True
    return changed


def layout_from_raw(raw_input):
    seat_layout = []
    for line in raw_input:
        seat_layout.append(list(line))
    return seat_layout


def part1(seat_layout_raw):
    seat_layout = layout_from_raw(seat_layout_raw)
    layout_changed = True
    while layout_changed:
        layout_changed = run_seat_iteration(seat_layout, calculate_occupied_adjacent_seats)
    return calculate_occupied_seats(seat_layout)

def part2(seat_layout_raw):
    seat_layout = layout_from_raw(seat_layout_raw)
    layout_changed = True
    while layout_changed:
        layout_changed = run_seat_iteration(seat_layout,
                                            calculate_occupied_seats_in_line,
                                            occupied_threshold=5)
    return calculate_occupied_seats(seat_layout)

print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
