from common import puzzle_input_by_newlines

train_tickets = puzzle_input_by_newlines(16)

def define_class_ranges(class_ranges):
    defined_class_ranges = {}
    for c_r in class_ranges:
        name, ranges = c_r.split(": ")
        defined_class_ranges[name] = []
        for range in ranges.split(' or '):
            min_value, max_value = range.split('-')
            defined_class_ranges[name].append([int(min_value), int(max_value)])
    return defined_class_ranges

def in_valid_range(value, ranges):
    for c_r in ranges.values():
        for c_r_r in c_r:
            if c_r_r[0] <= value <= c_r_r[1]:
                return True
    return False

def sum_invalid_fields(tickets, class_ranges):
    sum_fields = 0
    for ticket in tickets:
        ticket_values = [int(value) for value in ticket.split(',')]
        for value in ticket_values:
            if not in_valid_range(value, class_ranges):
                sum_fields += value
    return sum_fields

def remove_invalid_tickets(tickets, class_ranges):
    valid_tickets = []
    for ticket in tickets:
        ticket_values = [int(value) for value in ticket.split(',')]
        for value in ticket_values:
            if not in_valid_range(value, class_ranges):
                break
        else:
            valid_tickets.append(ticket_values)
    return valid_tickets

def value_in_range(value, ranges):
    for value_range in ranges:
        if value_range[0] <= value <= value_range[1]:
            return True
    return False

def remove_from_all_except_one(field_matches, index):
    reduced = False
    value_to_exclude = field_matches[index][0]
    for field, values in field_matches.items():
        if field == index:
            continue
        if value_to_exclude in values:
            values.remove(value_to_exclude)
            reduced = True
    return reduced


def match_fields(tickets, class_ranges):
    class_names = list(class_ranges.keys())
    field_matches = {index: class_names.copy() for index in range(len(tickets[0]))}
    for ticket in tickets:
        for index,value in enumerate(ticket):
            if len(field_matches[index]) > 1:
                for class_name in field_matches[index]:
                    if not value_in_range(value, class_ranges[class_name]):
                        field_matches[index].remove(class_name)
                        # if len(field_matches[index]) == 1:
                        #     remove_from_all_except_one(field_matches, index)
    return field_matches


def reduce_singles(pos_vs_name):
    reduced = False
    for pos, names in pos_vs_name.items():
        if len(names) == 1:
            reduced = reduced or remove_from_all_except_one(pos_vs_name, pos)
    return reduced


def part1(train_tickets):
    class_ranges = define_class_ranges(train_tickets[0])
    # your_ticket = train_tickets[1][1]
    other_tickets = train_tickets[2][1:]
    return sum_invalid_fields(other_tickets, class_ranges)


def part2(train_tickets):
    class_ranges = define_class_ranges(train_tickets[0])
    your_ticket = [int(value) for value in train_tickets[1][1].split(',')]
    other_tickets = train_tickets[2][1:]
    tickets_for_validation = remove_invalid_tickets(other_tickets, class_ranges)
    tickets_for_validation.append(your_ticket)
    field_pos_vs_name = match_fields(tickets_for_validation, class_ranges)

    while reduce_singles(field_pos_vs_name):
        pass

    result = 1
    for pos, name in field_pos_vs_name.items():
        if name[0][:9] == 'departure':
            result *= your_ticket[pos]

    return result

print("Part 1", part1(train_tickets))
print("Part 2", part2(train_tickets))
