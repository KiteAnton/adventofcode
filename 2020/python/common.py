import os

def read_puzzle_input(day, test=None):
    try:
        file_extra = ''
        if test:
            file_extra = f"_test_{test}"
            
        with open(os.path.join('../input/', f"{day:02d}{file_extra}"), 'r') as file_input:
            return file_input.readlines()
    except FileNotFoundError:
        return []


def puzzle_input_as_int(day, test=None):
    try:
        return [int(n) for n in read_puzzle_input(day, test)]
    except (ValueError, TypeError):
        return []

def puzzle_input_as_str(day, test=None):
    return [line.strip() for line in read_puzzle_input(day, test)]

def puzzle_input_by_newlines(day, test=None):
    result = []
    new_result = []
    lines = puzzle_input_as_str(day, test)
    for line in lines:
        if not line:
            result.append(new_result)
            new_result = []
            continue
        new_result.append(line)
    result.append(new_result)
    return result
        

def find_key_value_pairs(input_string):
    key_values = {}
    key_pairs = input_string.split()
    for key_pair in key_pairs:
        key, _, value = key_pair.partition(':')
        key_values[key] = value

    return key_values


def split_line(line):
    splits = line.split(' ')
    return splits[0], int(splits[1])
