from common import puzzle_input_by_newlines

puzzle_input = puzzle_input_by_newlines(20)

def find_edges(tile):
    tile['edges'] = []
    tile['edges'].append(list(tile['pattern'][0]))
    tile['edges'].append(list(tile['pattern'][-1]))
    left_edge = []
    right_edge = []
    for row in tile['pattern']:
        left_edge.append(row[0])
        right_edge.append(row[-1])
    tile['edges'].append(left_edge)
    tile['edges'].append(right_edge)


def parse_tiles(tiles_raw):
    tiles = {}
    for t_r in tiles_raw:
        tile = {}
        tile_id = int(t_r[0][5:9])
        tile['id'] = tile_id
        tile['pattern'] = t_r[1:]
        find_edges(tile)
        tiles[tile_id] = tile
    return tiles


def find_corner_tiles(tiles):
    corner_tiles = {}

    for tile in tiles.values():
        matching_edges = [0, 0, 0, 0]
        for other_tile in tiles.values():
            if other_tile['id'] == tile['id']:
                continue
            for i, edge in enumerate(tile['edges']):
                for _, other_edge in enumerate(other_tile['edges']):
                    if edge == other_edge or edge == list(reversed(other_edge)):
                        matching_edges[i] = other_tile['id']
            tile['matching_edges'] = matching_edges
        if matching_edges.count(0) == 2:
            corner_tiles[tile['id']] = tile

    return corner_tiles


def flip_tile_x(tile):
    for i, _ in enumerate(tile['pattern']):
        tile['pattern'][i] = tile['pattern'][i][::-1]
    find_edges(tile)
    if 'matching_edges' in tile:
        tile['matching_edges'][2], tile['matching_edges'][3] = tile['matching_edges'][3], tile['matching_edges'][2]


def flip_tile_y(tile):
    for i in range(len(tile['pattern'])//2):
        tile['pattern'][i], tile['pattern'][-i-1] = tile['pattern'][-i-1], tile['pattern'][i]
    find_edges(tile)
    if 'matching_edges' in tile:
        tile['matching_edges'][0], tile['matching_edges'][1] = tile['matching_edges'][1], tile['matching_edges'][0]


def rotate_tile(tile):
    new_pattern = []
    old_pattern = tile['pattern']
    row_size = len(old_pattern)
    col_size = len(old_pattern[0])
    for i in range(row_size):
        new_row = []
        for j in range(col_size):
            new_row.append(old_pattern[j][row_size - i - 1])
        new_pattern.append(''.join(new_row))
    tile['pattern'] = new_pattern
    find_edges(tile)
    if 'matching_edges' in tile:
        tile['matching_edges'][0], tile['matching_edges'][1], tile['matching_edges'][2], tile['matching_edges'][3] = \
            tile['matching_edges'][3], tile['matching_edges'][2], tile['matching_edges'][0], tile['matching_edges'][1]


def find_next_tile(current_tile, direction, tiles):
    if direction == 'right':
        first_edge = 3
        second_edge = 2
    elif direction == 'down':
        first_edge = 1
        second_edge = 0
    else:
        return None

    if not current_tile['matching_edges'][first_edge]:
        return None

    next_tile = tiles[current_tile['matching_edges'][first_edge]]


    # Rotate tile to line up edges
    while next_tile['matching_edges'][second_edge] != current_tile['id']:
        rotate_tile(next_tile)

    # Flip edge if needed
    if current_tile['edges'][first_edge] == list(reversed(next_tile['edges'][second_edge])):
        if direction == 'right':
            flip_tile_y(next_tile)
        else:
            flip_tile_x(next_tile)
    return next_tile


def find_next_tile_right(current_tile, tiles):
    return find_next_tile(current_tile, 'right', tiles)


def find_next_tile_down(current_tile, tiles):
    return find_next_tile(current_tile, 'down', tiles)


def arrange_tiles(tiles):
    arranged_tiles = []
    edge_tiles = find_corner_tiles(tiles)

    starting_tile = list(edge_tiles.values())[0]
    # Flip tile until matching edges are "inward"
    if starting_tile['matching_edges'][0]:
        flip_tile_y(starting_tile)
    if starting_tile['matching_edges'][2]:
        flip_tile_x(starting_tile)

    current_tile = starting_tile

    arranged_row = []
    arranged_row.append(current_tile)
    tile_index = 1
    next_tile = find_next_tile_right(current_tile, tiles)
    while tile_index < len(tiles):
        while next_tile:
            arranged_row.append(next_tile)
            tile_index += 1
            next_tile = find_next_tile_right(next_tile, tiles)
        arranged_tiles.append(arranged_row)
        first_in_row = arranged_row[0]
        arranged_row = []
        if tile_index < len(tiles):
            next_tile = find_next_tile_down(first_in_row, tiles)

    return arranged_tiles


def create_image_from_tiles(arranged_tiles):
    combined_image = {}
    combined_image['pattern'] = []
    # Remove borders from each tile
    for row in arranged_tiles:
        row_image = []

        for tile in row:
            for i in range(1, len(tile['pattern']) -1):
                if len(row_image) < i:
                    row_image.append('')
                row_image[i-1] += tile['pattern'][i][1:-1]
        for new_row in row_image:
            combined_image['pattern'].append(new_row)
    return combined_image


def find_monsters(tile):
    # Monster coordinates
    monster = [ (0, 18),
                (1, 0), (1, 5), (1, 6), (1, 11), (1, 12), (1, 17), (1, 18), (1, 19),
                (2, 1), (2, 4), (2, 7), (2, 10), (2, 13), (2, 16)]

    row_index = 0

    monster_positions = set()
    monster_count = 0
    while row_index < len(tile['pattern']) - 1:
        for c_i in range(0, len(tile['pattern'][0]) - 19):
            for m_pos in monster:
                m_r, m_c = m_pos

                if (row_index + m_r, c_i + m_c) in monster_positions:
                    break
                if tile['pattern'][row_index + m_r][c_i + m_c] != '#':
                    break
            else:
                monster_count += 1
                for m_pos in monster:
                    m_r, m_c = m_pos
                    monster_positions.add((row_index+m_r, c_i+m_c))
        row_index += 1

    return monster_count, monster_positions


def count_crosses(tile):
    crosses = 0

    for row in tile['pattern']:
        for char in row:
            if char == '#':
                crosses += 1
    return crosses


def part1(tiles_raw):
    tiles = parse_tiles(tiles_raw)
    edge_tiles = find_corner_tiles(tiles)
    product = 1
    for tile_id in edge_tiles.keys():
        product *= tile_id
    return product


def part2(tiles_raw):
    tiles = parse_tiles(tiles_raw)
    arranged_tiles = arrange_tiles(tiles)
    combined_tile = create_image_from_tiles(arranged_tiles)
    monster_count = 0
    iteration = 0
    transformations = [rotate_tile, rotate_tile, rotate_tile, flip_tile_y,
                       rotate_tile, rotate_tile, rotate_tile]

    top_count = 0
    while transformations:
        iteration += 1
        transformation_function = transformations.pop()
        transformation_function(combined_tile)
        monster_count, _ = find_monsters(combined_tile)
        if monster_count > top_count:
            top_count = monster_count
    cross_count = count_crosses(combined_tile)
    crosses_without_monster = cross_count - top_count * 15
    return crosses_without_monster


print("Part 1:", part1(puzzle_input))
print("Part 2:", part2(puzzle_input))
