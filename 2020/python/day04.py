import re
from common import find_key_value_pairs, puzzle_input_by_newlines

def check_required_fields(key_values):
    required_fields = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}
    for field in required_fields:
        if field not in key_values:
            return False
    return True


def validate_inputs(key_values):
    # byr - Four digits; valid range 1920 - 2002
    if not 1920 <= int(key_values['byr']) <= 2002:
        return False

    # iyr - Four digits; valid range 2010 - 2020
    if not 2010 <= int(key_values['iyr']) <= 2020:
        return False

    # eyr - Four digits; valid range 2020 - 2030
    if not 2020 <= int(key_values['eyr']) <= 2030:
        return False

    # hgt - a number followed by either /cm/ or /in/:
    hgt_value = int(key_values['hgt'][:-2])
    hgt_unit = key_values['hgt'][-2:]
    if hgt_unit in ['cm', 'in']:
        # cm, valid range 150 - 193
        if hgt_unit == 'cm':
            if not 150 <= hgt_value <= 193:
                return False
        else:
            # in, valid range 59 - 76
            if not 59 <= hgt_value <= 76:
                return False
    else:
        return False

    # hcl - a # followed by exactly six characters (0-9 or a-f)
    allowed = re.compile('^[#a-f0-9]+$')
    if len(key_values['hcl']) != 7 or key_values['hcl'][0] != '#' \
        or not allowed.match(key_values['hcl']):
        return False

    # ecl - Exactly one of: amb, blu, gry, grn, hzl or oth
    if key_values['ecl'] not in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'):
        return False

    # pid - A nine-digit number, including leading zeros
    allowed = re.compile('^[0-9]+$')
    if len(key_values['pid']) != 9 or not allowed.match(key_values['pid']):
        return False

    # cid - Temporarily ignored/passed
    return True


def count_valid_passports(passport_entries_raw, validate=False):
    valid_passports = 0
    for entry in passport_entries_raw:
        entry = find_key_value_pairs(' '.join(entry))
        if check_required_fields(entry):
            if not validate or validate_inputs(entry):
                valid_passports += 1
    return valid_passports

def part1(passport_entries_raw):
    return count_valid_passports(passport_entries_raw)

def part2(passport_entries_raw):
    return count_valid_passports(passport_entries_raw, validate=True)

passport_entries_raw = puzzle_input_by_newlines(4)
print("Part1:", part1(passport_entries_raw))
print("Part1:", part2(passport_entries_raw))
