import copy
from common import puzzle_input_by_newlines

initial_plane = puzzle_input_by_newlines(17)

def init_cube(initial_plane):
    cube = []
    plane0 = []
    for line in initial_plane:
        plane0.append(list(line))

    cube.append(plane0)
    return cube

def find_active_neighbours_cube(z, y, x, cube):
    active_neighbours = 0
    for z_i in range(max(0, z-1), min(z+2, len(cube))):
        for y_i in range(max(0, y-1), min(y+2, len(cube[0]))):
            for x_i in range(max(0, x-1), min(x+2, len(cube[0][0]))):
                state = cube[z_i][y_i][x_i]
                if (z_i != z or y_i != y or x_i != x) and state == '#':
                    active_neighbours += 1
    return active_neighbours

def find_active_neighbours_hypercube(w, z, y, x, hypercube):
    active_neighbours = 0
    for w_i in range(max(0, w-1), min(w+2, len(hypercube))):
        for z_i in range(max(0, z-1), min(z+2, len(hypercube[0]))):
            for y_i in range(max(0, y-1), min(y+2, len(hypercube[0][0]))):
                for x_i in range(max(0, x-1), min(x+2, len(hypercube[0][0][0]))):
                    state = hypercube[w_i][z_i][y_i][x_i]
                    if (w_i != w or z_i != z or y_i != y or x_i != x) and state == '#':
                        active_neighbours += 1
    return active_neighbours

def empty_layer(y, x):
    new_layer = []
    for _ in range(y):
        new_layer.append(['.'] * x)
    return new_layer

def empty_dimension(z, y, x):
    new_cube = []
    for _ in range(z):
        new_cube.append(empty_layer(y, x))
    return new_cube


def add_new_dimensions_and_layers(cube):
    y_size = len(cube[0]) + 2
    x_size = len(cube[0][0]) + 2

    new_cube = []
    new_cube.append(empty_layer(y_size, x_size))
    for dimension in cube:
        new_dimension = []
        new_dimension.append(['.'] * x_size)
        for layer in dimension:
            new_layer = []
            new_layer.append('.')
            for dot in layer:
                new_layer.append(dot)
            new_layer.append('.')
            new_dimension.append(new_layer)

        new_dimension.append(['.'] * x_size)
        new_cube.append(new_dimension)
    new_cube.append(empty_layer(y_size, x_size))

    return new_cube

def add_new_hyperdimension_and_dimensions(hypercube):
    new_hypercube = []
    z_size = len(hypercube[0]) + 2
    y_size = len(hypercube[0][0]) + 2
    x_size = len(hypercube[0][0][0]) + 2
    new_hypercube.append(empty_dimension(z_size, y_size, x_size))
    for cube in hypercube:
        new_hypercube.append(add_new_dimensions_and_layers(cube))
    new_hypercube.append(empty_dimension(z_size, y_size, x_size))
    return new_hypercube

def change_state(cube):
    cube = add_new_dimensions_and_layers(cube)
    new_cube = copy.deepcopy(cube)
    for z, dimension in enumerate(cube):
        for y, layer in enumerate(dimension):
            for x, dot in enumerate(layer):
                active_neighbours = find_active_neighbours_cube(z, y, x, cube)
                if dot == '#':
                    if active_neighbours not in [2,3]:
                        new_cube[z][y][x] = '.'
                elif dot == '.':
                    if active_neighbours == 3:
                        new_cube[z][y][x] = '#'
    return new_cube

def change_state_hypercube(hypercube):
    hypercube = add_new_hyperdimension_and_dimensions(hypercube)
    new_hypercube = copy.deepcopy(hypercube)
    for w, hyperdimension in enumerate(hypercube):
        for z, dimension in enumerate(hyperdimension):
            for y, layer in enumerate(dimension):
                for x, dot in enumerate(layer):
                    active_neighbours = find_active_neighbours_hypercube(w, z, y, x, hypercube)
                    if dot == '#':
                        if active_neighbours not in [2,3]:
                            new_hypercube[w][z][y][x] = '.'
                    elif dot == '.':
                        if active_neighbours == 3:
                            new_hypercube[w][z][y][x] = '#'
    return new_hypercube

def count_active_stars(cube):
    active_stars = 0
    for dimension in cube:
        for layer in dimension:
            for dot in layer:
                if dot == '#':
                    active_stars += 1

    return active_stars

def count_active_stars_hypercube(hypercube):
    active_stars = 0
    for dimension in hypercube:
        active_stars += count_active_stars(dimension)
    return active_stars


def part1(initial_plane):
    cube = init_cube(initial_plane[0])
    for _ in range(6):
        cube = change_state(cube)
    return count_active_stars(cube)

def part2(initial_plane):
    hypercube = init_cube(initial_plane[0])
    for _ in range(6):
        hypercube = change_state_hypercube(hypercube)
    return count_active_stars_hypercube(hypercube)


print("Part 1:", part1(initial_plane))
print("Part 2:", part2(initial_plane))
