from common import puzzle_input_as_int

lines = puzzle_input_as_int(10)

def fix_adapter_list(adapters):
    adapters_sorted = adapters.copy()
    adapters_sorted.append(max(adapters)+3)
    adapters_sorted.append(0)
    return sorted(adapters_sorted)

def part1(adapters):
    adapters_sorted = fix_adapter_list(adapters)
    one_difference = 0
    three_difference = 0
    prev_value = 0
    for adapter in adapters_sorted:
        if adapter - prev_value == 1:
            one_difference += 1
        elif adapter - prev_value == 3:
            three_difference += 1
        prev_value = adapter
    return one_difference * three_difference

def part2(adapters):
    adapters_sorted = fix_adapter_list(adapters)
    variants = {}
    variants[adapters_sorted[-1]] = 1

    for index, adapter in reversed(list(enumerate(adapters_sorted))):
        if index == len(adapters_sorted) - 1:  # Dont run on last value
            continue

        variants_for_number = 0
        next_numbers = adapters_sorted[index+1:index+4]
        for i in range(1,3+1):
            if adapter+i in next_numbers:
                variants_for_number += variants[adapter+i]


        variants[adapter] = variants_for_number
    return variants[0]

print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
