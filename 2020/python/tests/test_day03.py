from common import puzzle_input_as_str
from day03 import part1, part2


test_slope = puzzle_input_as_str(3, 1)
slope = puzzle_input_as_str(3)

def test_test_part1():
    assert part1(test_slope) == 7

def test_test_part2():
    assert part2(test_slope) == 336

def test_final_part1():
    assert part1(slope) == 207

def test_final_part2():
    assert part2(slope) == 2655892800
