from common import puzzle_input_as_str
from day13 import part1, part2

bus_notes = puzzle_input_as_str(13)
test_bus_notes = puzzle_input_as_str(13, 1)


def test_test_part1():
    assert part1(test_bus_notes) == 295

def test_final_part1():
    assert part1(bus_notes) == 4722

def test_test_part2():
    assert part2(test_bus_notes) == 1068781

    test_input = "17,x,13,19"
    assert part2([0, test_input]) == 3417
    test_input = "67,7,59,61"
    assert part2([0, test_input]) == 754018
    test_input = "67,x,7,59,61"
    assert part2([0, test_input]) == 779210
    test_input = "67,7,x,59,61"
    assert part2([0, test_input]) == 1261476
    test_input = "1789,37,47,1889"
    assert part2([0, test_input]) == 1202161486


def test_final_part2():
    assert part2(bus_notes) == 825305207525452
