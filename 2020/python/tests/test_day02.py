from common import puzzle_input_as_str
from day02 import part1, part2 


test_passwords = puzzle_input_as_str(2, 1)
passwords = puzzle_input_as_str(2)

def test_test_part1():
    assert part1(test_passwords) == 2

def test_test_part2():
    assert part2(test_passwords) == 1

def test_final_part1():
    assert part1(passwords) == 600
    
def test_final_part2():
    assert part2(passwords) == 245
