from common import puzzle_input_as_str
from day18 import part1, part2, evaluate_simple_expression, evaluate_complex_expression

math_expressions = puzzle_input_as_str(18)

def test_evaluate_simple_expression():
    assert evaluate_simple_expression("1 + 2 * 3 + 4 * 5 + 6") == 71

def test_evaluate_complex_expression():
    assert evaluate_complex_expression("1 + 2 * 3 + 4 * 5 + 6") == 71
    assert evaluate_complex_expression("1 + (2 * 3) + (4 * (5 + 6))") == 51
    assert evaluate_complex_expression("2 * 3 + (4 * 5)") == 26
    assert evaluate_complex_expression("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 437
    assert evaluate_complex_expression("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 12240
    assert evaluate_complex_expression("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") == 13632

def test_final_part1():
    assert part1(math_expressions) == 3348222486398

def test_evaluate_simple_expression_advanced():
    assert evaluate_simple_expression("1 + 2 * 3 + 4 * 5 + 6", advanced=True) == 231

def test_evaluate_complex_expression_advanced():
    assert evaluate_complex_expression("1 + 2 * 3 + 4 * 5 + 6", advanced=True) == 231
    assert evaluate_complex_expression("1 + (2 * 3) + (4 * (5 + 6))", advanced=True) == 51
    assert evaluate_complex_expression("2 * 3 + (4 * 5)", advanced=True) == 46
    assert evaluate_complex_expression("5 + (8 * 3 + 9 + 3 * 4 * 3)", advanced=True) == 1445
    assert evaluate_complex_expression("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", advanced=True) == 669060
    assert evaluate_complex_expression("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", advanced=True) == 23340


def test_final_part2():
    assert part2(math_expressions) == 43423343619505
