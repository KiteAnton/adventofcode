from common import puzzle_input_by_newlines
from day19 import part1, part2, parse_rules, is_valid

rules_raw, messages = puzzle_input_by_newlines(19)
test_rules_and_messages1 = puzzle_input_by_newlines(19, 1)
test_rules_and_messages2 = puzzle_input_by_newlines(19, 2)
test_rules3, test_messages = puzzle_input_by_newlines(19, 3)

def test_match_rules():
    rules = parse_rules(test_rules_and_messages1[0])
    assert is_valid(rules, "aab")
    assert is_valid(rules, "aba")
    assert not is_valid(rules, "aaa")

    rules = parse_rules(test_rules_and_messages2[0])
    assert is_valid(rules, "aaaabb")
    assert is_valid(rules, "aaabab")
    assert is_valid(rules, "abbabb")
    assert is_valid(rules, "abbbab")
    assert is_valid(rules, "aabaab")
    assert is_valid(rules, "aabbbb")
    assert is_valid(rules, "abaaab")
    assert is_valid(rules, "ababbb")
    assert not is_valid(rules, "axxxxx")

def test_test_part1():
    assert part1(test_rules3, test_messages) == 2

def test_final_part1():
    assert part1(rules_raw, messages) == 134

def test_final_part2():
    assert part2(rules_raw, messages) == 377
