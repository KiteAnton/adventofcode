from common import puzzle_input_by_newlines
from day22 import part1, part2, play_round, play_game_recursive_combat

starting_cards = puzzle_input_by_newlines(22)
test_startings_cards = puzzle_input_by_newlines(22, 1)
test_startings_cards2 = puzzle_input_by_newlines(22, 2)


def test_play_round():
    deck1 = [int(card) for card in test_startings_cards[0][1:]]
    deck2 = [int(card) for card in test_startings_cards[1][1:]]

    while deck1 and deck2:
        play_round(deck1, deck2)
    assert not deck1
    assert deck2 == [3, 2, 10, 6, 8, 5, 9, 4, 7, 1]

def test_test_part1():
    assert part1(test_startings_cards) == 306

def test_final_part1():
    assert part1(starting_cards) == 35013

def test_play_round_recursive_combat():
    deck1 = [int(card) for card in test_startings_cards[0][1:]]
    deck2 = [int(card) for card in test_startings_cards[1][1:]]

    assert play_game_recursive_combat(deck1, deck2) == 2
    assert not deck1
    assert deck2 == [7, 5, 6, 2, 4, 1, 10, 8, 9, 3]


def test_test_part2():
    assert part2(test_startings_cards) == 291

def test_final_part2():
    assert part2(starting_cards) == 32806
