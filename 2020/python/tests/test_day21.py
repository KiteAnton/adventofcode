from common import puzzle_input_as_str
from day21 import part1, part2, split_ingredients_allergens, find_non_allergen_ingredients

ingredients_and_allergens = puzzle_input_as_str(21)
test_ingredients_and_allergens = puzzle_input_as_str(21, 1)


def test_split_ingredients_allergens():
    expected_ingredients = [
        {'mxmxvkd', 'kfcds', 'sqjhc', 'nhms'},
        {'trh', 'fvjkl', 'sbzzf', 'mxmxvkd'},
        {'sqjhc', 'fvjkl'},
        {'sqjhc', 'mxmxvkd', 'sbzzf'}]
    expected_allergens = [
        ['dairy', 'fish'],
        ['dairy'],
        ['soy'],
        ['fish']]

    for i, string in enumerate(test_ingredients_and_allergens):
        ingredients, allergens = split_ingredients_allergens(string)
        assert ingredients == expected_ingredients[i]
        assert allergens == expected_allergens[i]

def test_find_ingredients_without_allergens():
    non_allergen_ingredients, _ = find_non_allergen_ingredients(test_ingredients_and_allergens)
    assert 'kfcds' in non_allergen_ingredients
    assert 'nhms' in non_allergen_ingredients
    assert 'sbzzf' in non_allergen_ingredients
    assert 'trh' in non_allergen_ingredients


def test_test_part1():
    assert part1(test_ingredients_and_allergens) == 5

def test_final_part1():
    assert part1(ingredients_and_allergens) == 2517

def test_test_part2():
    assert part2(test_ingredients_and_allergens) == "mxmxvkd,sqjhc,fvjkl"

def test_final_part2():
    assert part2(ingredients_and_allergens) == "rhvbn,mmcpg,kjf,fvk,lbmt,jgtb,hcbdb,zrb"
