from common import puzzle_input_as_int
from day09 import part1, part2

numbers = puzzle_input_as_int(9)
test_numbers = puzzle_input_as_int(9, 1)


def test_test_part1():
    assert part1(test_numbers, preamble_size=5) == 127


def test_final_part1():
    assert part1(numbers) == 23278925


def test_test_part2():
    assert part2(test_numbers, preamble_size=5) == 62


def test_final_part2():
    assert part2(numbers) == 4011064
