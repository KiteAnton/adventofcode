from common import puzzle_input_as_str
from day07 import bag_contained_in, can_contain, bag_rules
from day07 import part1, part2

bag_rules_raw = puzzle_input_as_str(7)
test_bag_rules_raw = puzzle_input_as_str(7, 1)


def test_test_part1():
    contained_in = bag_contained_in(test_bag_rules_raw)
    assert "bright white" in contained_in['shiny gold']
    assert "muted yellow" in contained_in['shiny gold']
    bag_alternatives = can_contain(contained_in, "shiny gold")
    assert len(bag_alternatives) == 4
    assert "bright white" in bag_alternatives
    assert "muted yellow" in bag_alternatives
    assert "dark orange" in bag_alternatives
    assert "light red" in bag_alternatives
    assert part1(test_bag_rules_raw) == 4


def test_final_part1():
    assert part1(bag_rules_raw) == 238
    

def test_test_part2():
    br = bag_rules(test_bag_rules_raw)
    assert 'dark olive' in br['shiny gold']
    assert br['shiny gold']['dark olive'] == 1
    assert 'vibrant plum' in br['shiny gold']
    assert br['shiny gold']['vibrant plum'] == 2
    assert part2(test_bag_rules_raw) == 32


def test_final_part2():
    assert part2(bag_rules_raw) == 82930
