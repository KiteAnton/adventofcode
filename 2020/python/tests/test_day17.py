from common import puzzle_input_by_newlines
from day17 import part1, part2

initial_pocket = puzzle_input_by_newlines(17)
test_initial_pocket = puzzle_input_by_newlines(17, 1)


def test_test_part1():
    assert part1(test_initial_pocket) == 112

def test_final_part1():
    assert part1(initial_pocket) == 252

def test_test_part2():
    assert part2(test_initial_pocket) == 848

def test_final_part2():
    assert part2(initial_pocket) == 2160
