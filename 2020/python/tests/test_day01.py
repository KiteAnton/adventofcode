from common import puzzle_input_as_int
from day01 import part1, part2 


test_numbers = puzzle_input_as_int(1, 1)
numbers = puzzle_input_as_int(1)

def test_test_part1():
    assert part1(test_numbers) == 514579

def test_test_part2():
    assert part2(test_numbers) == 241861950

def test_final_part1():
    assert part1(numbers) == 444019

def test_final_part2():
    assert part2(numbers) == 29212176
