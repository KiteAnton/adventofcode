from common import puzzle_input_by_newlines
from day06 import unique_answers_by_group, common_answers_by_group
from day06 import part1, part2

group_answers = puzzle_input_by_newlines(6)
group_test_answers = puzzle_input_by_newlines(6, 1)


def test_test_part1():
    assert unique_answers_by_group(["abc"]) == 3
    assert unique_answers_by_group(["a", "b", "c"]) == 3
    assert unique_answers_by_group(["ab", "bc"]) == 3
    assert unique_answers_by_group(["a", "a", "a", "a"]) == 1
    assert part1(group_test_answers) == 11


def test_final_part1():
    assert part1(group_answers) == 6161


def test_test_part2():
    assert common_answers_by_group(["abc"]) == 3
    assert common_answers_by_group(["a", "b", "c"]) == 0
    assert common_answers_by_group(["ab", "bc"]) == 1
    assert common_answers_by_group(["a", "a", "a", "a"]) == 1
    assert common_answers_by_group(["b"]) == 1
    assert part2(group_test_answers) == 6

def test_final_part2():
    assert part2(group_answers) == 2971
