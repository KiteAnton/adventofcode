from common import puzzle_input_as_str
from day14 import part1, part2, apply_memory_mask, fix_floating_addresses

init_instructions = puzzle_input_as_str(14)
test_init_instructions = puzzle_input_as_str(14, 1)
test_init_instructions2 = puzzle_input_as_str(14, 2)


def test_test_part1():
    assert part1(test_init_instructions) == 165

def test_final_part1():
    assert part1(init_instructions) == 15018100062885

def test_mask_address():
    mask = "000000000000000000000000000000X1001X"
    memory_addresses = apply_memory_mask(mask, 42)
    assert len(memory_addresses) == 4
    assert 26 in memory_addresses
    assert 27 in memory_addresses
    assert 58 in memory_addresses
    assert 59 in memory_addresses

    mask = "00000000000000000000000000000000X0XX"
    memory_addresses = apply_memory_mask(mask, 26)
    assert len(memory_addresses) == 8
    assert 16 in memory_addresses
    assert 17 in memory_addresses
    assert 18 in memory_addresses
    assert 19 in memory_addresses
    assert 24 in memory_addresses
    assert 25 in memory_addresses
    assert 26 in memory_addresses
    assert 27 in memory_addresses


def test_fix_floating_addresses():
    fixed_addresses = fix_floating_addresses('X01')
    assert 5 in fixed_addresses
    assert 1 in fixed_addresses

    fixed_addresses = fix_floating_addresses('X01X')
    assert 11 in fixed_addresses
    assert 3 in fixed_addresses
    assert 10 in fixed_addresses
    assert 2 in fixed_addresses

def test_test_part2():
    assert part2(test_init_instructions2) == 208

def test_final_part2():
    assert part2(init_instructions) == 5724245857696
