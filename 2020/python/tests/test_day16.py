from common import puzzle_input_by_newlines
from day16 import part1, part2

train_tickets = puzzle_input_by_newlines(16)
test_train_tickets = puzzle_input_by_newlines(16, 1)
test_train_tickets2 = puzzle_input_by_newlines(16, 2)


def test_test_part1():
    assert part1(test_train_tickets) == 71

def test_final_part1():
    assert part1(train_tickets) == 27898

def test_final_part2():
    assert part2(train_tickets) == 2766491048287
