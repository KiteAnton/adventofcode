from common import puzzle_input_as_str
from day11 import run_seat_iteration, layout_from_raw, calculate_occupied_seats, calculate_occupied_seats_in_line
from day11 import part1, part2

seat_layout_raw = puzzle_input_as_str(11)
test_seat_layout_raw = puzzle_input_as_str(11, 1)
test_seat_layout_raw2 = puzzle_input_as_str(11, 2)
test_seat_layout_raw3 = puzzle_input_as_str(11, 3)
test_seat_layout_raw4 = puzzle_input_as_str(11, 4)


def test_run_iteration():
    seat_layout = layout_from_raw(test_seat_layout_raw)
    assert run_seat_iteration(seat_layout)
    assert run_seat_iteration(seat_layout)
    assert run_seat_iteration(seat_layout)
    assert run_seat_iteration(seat_layout)
    assert run_seat_iteration(seat_layout)
    assert not run_seat_iteration(seat_layout)
    assert calculate_occupied_seats(seat_layout) == 37


def test_test_part1():
    assert part1(test_seat_layout_raw) == 37

def test_final_part1():
    assert part1(seat_layout_raw) == 2204

def test_calculate_occupied_in_line():
    seat_layout = layout_from_raw(test_seat_layout_raw2)
    occupied = calculate_occupied_seats_in_line(4, 3, seat_layout)
    assert occupied == 8

    seat_layout = layout_from_raw(test_seat_layout_raw3)
    occupied = calculate_occupied_seats_in_line(1, 1, seat_layout)
    assert occupied == 0

    seat_layout = layout_from_raw(test_seat_layout_raw4)
    occupied = calculate_occupied_seats_in_line(3, 3, seat_layout)
    assert occupied == 0

def test_test_part2():
    assert part2(test_seat_layout_raw) == 26

def test_final_part2():
    assert part2(seat_layout_raw) == 1986
