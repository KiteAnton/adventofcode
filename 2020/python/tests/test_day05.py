import pytest
from common import puzzle_input_as_str
from day05 import find_column, find_row, find_seat_id
from day05 import part1, part2


boarding_passes = puzzle_input_as_str(5)

@pytest.mark.parametrize("bp, row, column, seat_id", [
    ("FBFBBFFRLR", 44, 5, 357),
    ("BFFFBBFRRR", 70, 7, 567),
    ("FFFBBBFRRR", 14, 7, 119),
    ("BBFFBBFRLL", 102, 4, 820)
])
def test_test_part1(bp, row, column, seat_id):
    assert find_column(bp) == column
    assert find_row(bp) == row
    assert find_seat_id(bp) == seat_id

def test_final_part1():
    assert part1(boarding_passes) == 835

def test_final_part2():
    assert part2(boarding_passes) == 649
