from common import puzzle_input_as_int
from day10 import part1, part2

adapters = puzzle_input_as_int(10)
test_adapters = puzzle_input_as_int(10, 1)
test_adapters2 = puzzle_input_as_int(10, 2)


def test_test_part1():
    assert part1(test_adapters) == 35
    assert part1(test_adapters2) == 220


def test_final_part1():
    assert part1(adapters) == 2343


def test_test_part2():
    assert part2(test_adapters) == 8
    assert part2(test_adapters2) == 19208


def test_final_part2():
    assert part2(adapters) == 31581162962944
