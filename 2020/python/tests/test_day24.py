from common import puzzle_input_as_str
from day24 import part1, part2, coordinates_from_string


tiles_to_switch = puzzle_input_as_str(24)
test_tiles_to_switch = puzzle_input_as_str(24, 1)

def test_coordinates_from_string():
    assert coordinates_from_string("esenee") == (0, 6)
    assert coordinates_from_string("esew") == (-1, 1)
    assert coordinates_from_string("nwwswee") == (0, 0)


def test_test_part1():
    assert part1(test_tiles_to_switch) == 10


def test_final_part1():
    assert part1(tiles_to_switch) == 495


def test_test_part2():
    assert part2(test_tiles_to_switch) == 2208


def test_final_part2():
    assert part2(tiles_to_switch) == 4012
