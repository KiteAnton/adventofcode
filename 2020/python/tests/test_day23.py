from common import puzzle_input_as_str
from day23 import part1, part2


starting_cups = puzzle_input_as_str(23)
test_starting_cups = puzzle_input_as_str(23, 2)


def test_test_part1():
    assert part1(test_starting_cups[0]) == '67384529'


def test_final_part1():
    assert part1(starting_cups[0]) == '28946753'


def test_test_part2():
    assert part2(test_starting_cups[0]) == 149245887792


def test_final_part2():
    assert part2(starting_cups[0]) == 519044017360
