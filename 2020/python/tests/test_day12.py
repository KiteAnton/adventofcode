from common import puzzle_input_as_str
from day12 import part1, part2

directions = puzzle_input_as_str(12)
test_directions = puzzle_input_as_str(12, 1)


def test_test_part1():
    assert part1(test_directions) == 25

def test_final_part1():
    assert part1(directions) == 415

def test_test_part2():
    assert part2(test_directions) == 286

def test_final_part2():
    assert part2(directions) == 29401
