from common import puzzle_input_as_str
from day25 import part1, calculate_loop_size


door_card_public_keys = puzzle_input_as_str(25)
test_door_card_public_keys = puzzle_input_as_str(25, 1)

def test_calculate_loop_size():
    card_key = test_door_card_public_keys[0]
    door_key = test_door_card_public_keys[1]

    assert calculate_loop_size(card_key) == 8
    assert calculate_loop_size(door_key) == 11

def test_test_part1():
    assert part1(test_door_card_public_keys) == 14897079


def test_final_part1():
    assert part1(door_card_public_keys) == 12227206
