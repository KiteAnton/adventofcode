from common import puzzle_input_as_str
from day08 import part1, part2

instructions = puzzle_input_as_str(8)
test_instructions = puzzle_input_as_str(8, 1)


def test_test_part1():
    assert part1(test_instructions) == 5


def test_final_part1():
    assert part1(instructions) == 1671


def test_test_part2():
    assert part2(test_instructions) == 8


def test_final_part2():
    assert part2(instructions) == 892
