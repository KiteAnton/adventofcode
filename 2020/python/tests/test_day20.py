from common import puzzle_input_by_newlines
from day20 import part1, part2, parse_tiles, find_corner_tiles, arrange_tiles
from day20 import flip_tile_x, flip_tile_y, rotate_tile, create_image_from_tiles
from day20 import find_monsters

tiles_raw = puzzle_input_by_newlines(20)
test_tiles_raw = puzzle_input_by_newlines(20, 1)
test_tiles_raw2 = puzzle_input_by_newlines(20, 2)


def test_parse_tiles():
    tiles = parse_tiles(test_tiles_raw)
    assert len(tiles) == 9

    assert tiles[2311]['id'] == 2311
    assert len(tiles[2311]['edges']) == 4


def test_find_edge_tiles():
    tiles = parse_tiles(test_tiles_raw)

    corner_tiles = find_corner_tiles(tiles)
    assert len(corner_tiles) == 4
    assert 1951 in corner_tiles
    assert 3079 in corner_tiles
    assert 2971 in corner_tiles
    assert 1171 in corner_tiles


def test_test_part1():
    assert part1(test_tiles_raw) == 20899048083289

def test_final_part1():
    assert part1(tiles_raw) == 16937516456219

def test_flip_tile_x():
    tiles = parse_tiles(test_tiles_raw2)
    tiles[1112]['matching_edges'] = [1, 2, 3, 4]
    flip_tile_x(tiles[1112])
    assert tiles[1112]['matching_edges'] == [1, 2, 4, 3]
    assert tiles[1112]['edges'][0] == ['C', 'B', 'A']
    assert tiles[1112]['edges'][1] == ['I', 'H', 'G']
    assert tiles[1112]['edges'][2] == ['C', 'F', 'I']
    assert tiles[1112]['edges'][3] == ['A', 'D', 'G']
    assert tiles[1112]['pattern'] == ['CBA', 'FED', 'IHG']


def test_flip_tile_y():
    tiles = parse_tiles(test_tiles_raw2)
    tiles[1112]['matching_edges'] = [1, 2, 3, 4]
    flip_tile_y(tiles[1112])
    assert tiles[1112]['matching_edges'] == [2, 1, 3, 4]
    assert tiles[1112]['edges'][0] == ['G', 'H', 'I']
    assert tiles[1112]['edges'][1] == ['A', 'B', 'C']
    assert tiles[1112]['edges'][2] == ['G', 'D', 'A']
    assert tiles[1112]['edges'][3] == ['I', 'F', 'C']
    assert tiles[1112]['pattern'] == ['GHI', 'DEF', 'ABC']

def test_rotate_tile():
    tiles = parse_tiles(test_tiles_raw2)
    tiles[1112]['matching_edges'] = [1, 2, 3, 4]
    rotate_tile(tiles[1112])

    assert tiles[1112]['matching_edges'] == [4, 3, 1, 2]
    assert tiles[1112]['edges'][0] == ['C', 'F', 'I']
    assert tiles[1112]['edges'][1] == ['A', 'D', 'G']
    assert tiles[1112]['edges'][2] == ['C', 'B', 'A']
    assert tiles[1112]['edges'][3] == ['I', 'H', 'G']
    assert tiles[1112]['pattern'] == ['CFI', 'BEH', 'ADG']


def test_arrange_tiles():
    tiles = parse_tiles(test_tiles_raw)
    arranged_tiles = arrange_tiles(tiles)
    assert len(arranged_tiles) == 3
    assert len(arranged_tiles[0]) == 3
    assert arranged_tiles[0][0]['id'] == 1951
    assert arranged_tiles[0][1]['id'] == 2311
    assert arranged_tiles[0][2]['id'] == 3079
    assert arranged_tiles[1][0]['id'] == 2729
    assert arranged_tiles[1][1]['id'] == 1427
    assert arranged_tiles[1][2]['id'] == 2473
    assert arranged_tiles[2][0]['id'] == 2971
    assert arranged_tiles[2][1]['id'] == 1489
    assert arranged_tiles[2][2]['id'] == 1171


def test_create_combined_image():
    tiles = parse_tiles(test_tiles_raw)
    arranged_tiles = arrange_tiles(tiles)
    combined_tile = create_image_from_tiles(arranged_tiles)

    test_tiles_raw3 = puzzle_input_by_newlines(20, 3)
    for i, line in enumerate(test_tiles_raw3[0]):
        assert combined_tile['pattern'][i] == line


def test_found_monsters():
    tiles = parse_tiles(test_tiles_raw)
    arranged_tiles = arrange_tiles(tiles)
    combined_tile = create_image_from_tiles(arranged_tiles)

    rotate_tile(combined_tile)
    flip_tile_y(combined_tile)
    _, monster_positions = find_monsters(combined_tile)

    image = []
    for row in combined_tile['pattern']:
        image.append(list(row))
    for pos in monster_positions:
        m_r, m_c = pos
        image[m_r][m_c] = 'O'
    test_tiles_raw4 = puzzle_input_by_newlines(20, 4)[0]
    for i, row in enumerate(image):
        image_row = ''.join(row)
        assert test_tiles_raw4[i] == image_row

def test_test_part2():
    assert part2(test_tiles_raw) == 273

def test_final_part2():
    assert part2(tiles_raw) == 1858
