from common import puzzle_input_as_str
from day15 import part1, part2, next_sequence_number

starting_numbers = puzzle_input_as_str(15)
test_starting_numbers = puzzle_input_as_str(15, 1)

def test_next_number():
    # first sequence, 0,3,6
    assert next_sequence_number(test_starting_numbers[0], 4) == 0
    assert next_sequence_number(test_starting_numbers[0], 5) == 3  # 4 - 1
    assert next_sequence_number(test_starting_numbers[0], 6) == 3  # 5 - 2
    assert next_sequence_number(test_starting_numbers[0], 7) == 1  # 6 - 5
    assert next_sequence_number(test_starting_numbers[0], 8) == 0  # First time
    assert next_sequence_number(test_starting_numbers[0], 9) == 4  # 8 - 4
    assert next_sequence_number(test_starting_numbers[0], 10) == 0 # First time


def test_test_part1():
    assert part1(test_starting_numbers) == 436


def test_final_part1():
    assert part1(starting_numbers) == 706


def test_next_number_long():
    # first sequence, 0,3,6
    assert next_sequence_number(test_starting_numbers[0], 30000000) == 175594


def test_final_part2():
    assert part2(starting_numbers) == 19331
