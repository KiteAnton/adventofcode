from common import puzzle_input_by_newlines
from day04 import part1, part2


test_passports = puzzle_input_by_newlines(4, 1)
passports = puzzle_input_by_newlines(4)

def test_test_part1():
    assert part1(test_passports) == 2

def test_test_part2():
    assert part2(test_passports) == 2

def test_final_part1():
    assert part1(passports) == 196

def test_final_part2():
    assert part2(passports) == 114
