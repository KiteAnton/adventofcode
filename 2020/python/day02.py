from common import puzzle_input_as_str
import re

lines = puzzle_input_as_str(2)

def part1(lines):
    valid_passwords = 0
    for line in lines:
        rule, _, password = line.partition(':')
        min_chars, _, remains = rule.partition('-')
        max_chars, _, letter = remains.partition(' ')

        occurrences = len([m.start() for m in re.finditer(letter, password)])
        if int(min_chars) <= occurrences <= int(max_chars):
            valid_passwords += 1
    return valid_passwords

def part2(lines):
    valid_passwords = 0
    for line in lines:
        rule, _, password = line.partition(':')
        pos1, _, remains = rule.partition('-')
        pos2, _, letter = remains.partition(' ')

        password = password.strip()
        pos1_found = int(password[int(pos1) - 1] == letter)
        pos2_found = int(password[int(pos2) - 1] == letter)
        if pos1_found + pos2_found == 1:
            valid_passwords += 1
    return valid_passwords

print("Part1 valid passwords:", part1(lines))
print("Part2 valid passwords:", part2(lines))
