from common import puzzle_input_as_str

starting_numbers = puzzle_input_as_str(15)

def next_sequence_number(starting_numbers, sequence_number):
    number_sequence = [int(number) for number in starting_numbers.split(',')]
    previous_numbers = {value: position for position, value in enumerate(number_sequence)}
    for i in range(len(number_sequence), sequence_number):
        value = number_sequence[i-1]
        if value not in previous_numbers:
            previous_numbers[value] = i - 1
        number_sequence.append(i - previous_numbers[value] - 1)
        previous_numbers[number_sequence[-2]] = i-1

    return number_sequence[sequence_number-1]


def part1(starting_numbers):
    return next_sequence_number(starting_numbers[0], 2020)

def part2(starting_numbers):
    return next_sequence_number(starting_numbers[0], 30000000)


print("Part 1:", part1(starting_numbers))
print("Part 2:", part2(starting_numbers))
