from common import puzzle_input_as_str, split_line

lines = puzzle_input_as_str(8)

def run_until_duplicate_visits(instructions):
    hit_end = False
    accumulator = 0
    current_line = 0
    visited_lines = set()
    while current_line not in visited_lines and current_line < len(instructions):
        visited_lines.add(current_line)
        operation, argument = split_line(instructions[current_line])
        if operation == 'acc':
            accumulator += argument
        if operation == 'jmp':
            current_line += argument
        else:
            current_line += 1
    if current_line == len(instructions):
        hit_end = True
    return accumulator, hit_end

def part1(instructions):
    accumulator, _ = run_until_duplicate_visits(instructions)
    return accumulator


def part2(instructions):
    current_line = 1
    instruction_set = instructions.copy()
    accumulator = None
    while current_line < len(instructions):
        accumulator, hit_end = run_until_duplicate_visits(instruction_set)
        if hit_end:
            break
        operation = None
        while operation != 'jmp':
            current_line += 1
            operation, _ = split_line(instructions[current_line])
        instruction_set = instructions.copy()
        instruction_set[current_line] = instruction_set[current_line].replace('jmp', 'nop')

    return accumulator


print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
