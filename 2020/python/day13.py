from common import puzzle_input_as_str

bus_notes = puzzle_input_as_str(13)

def part1(bus_notes):
    earliest_departure = int(bus_notes[0])
    bus_ids = sorted([int(bus_id) for bus_id in bus_notes[1].split(',') if bus_id != 'x'])


    min_waiting_time = None
    bus_id_min_waiting_time = 0
    for bus_id in bus_ids:
        waiting_time = bus_id - earliest_departure % bus_id
        if not min_waiting_time or waiting_time < min_waiting_time:
            min_waiting_time = waiting_time
            bus_id_min_waiting_time = bus_id

    if min_waiting_time:
        return bus_id_min_waiting_time * min_waiting_time
    return None

def part2(bus_notes):
    bus_ids = list(bus_notes[1].split(','))

    next_departure = 0
    step_size = 1

    for index, bus in enumerate(bus_ids):
        if bus == 'x':
            continue
        bus = int(bus)

        while (index + next_departure) % bus:
            next_departure += step_size
        step_size *= bus

    return next_departure


print("Part 1:", part1(bus_notes))
print("Part 2:", part2(bus_notes))
