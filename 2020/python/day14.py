import re
from common import puzzle_input_as_str

init_instructions = puzzle_input_as_str(14)

def apply_mask(value, mask):
    for index, bit_value in enumerate(mask):
        if bit_value == '1':
            mask_value = 1 << (35-index)
            value = int(value) | mask_value
        elif bit_value == '0':
            mask_value = ~(1 << (35-index))
            value = int(value) & mask_value
    return value

def binary(number):
    number = bin(number)[2:]
    return number.zfill(36)

def fix_floating_addresses(address):
    return_addresses = []
    address = list(address)
    for index, char in enumerate(address):
        if char == 'X':
            add1 = address.copy()
            add2 = address.copy()
            add1[index] = '0'
            add2[index] = '1'
            return_addresses.extend(fix_floating_addresses(''.join(add1)))
            return_addresses.extend(fix_floating_addresses(''.join(add2)))
            return return_addresses
    return [int(''.join(address), 2)]


def apply_memory_mask(mask, address):
    address = list(binary(address))
    for index, char in enumerate(mask):
        if char == '0':
            continue
        elif char in ['1', 'X']:
            address[index] = char
    return fix_floating_addresses(''.join(address))

def part1(init_instructions):
    mask = 0
    memory = {}
    for line in init_instructions:
        operation, _, argument = line.split()
        if operation == 'mask':
            mask = argument
            continue
        memory_address = int(re.findall(r"\d+", operation)[0])
        value = apply_mask(argument, mask)
        memory[memory_address] = int(value)

    return_value = 0
    for value in memory.values():
        return_value += value

    return return_value

def part2(init_instructions):
    mask = 0
    memory = {}
    for line in init_instructions:
        operation, _, value = line.split()
        if operation == 'mask':
            mask = value
            continue
        memory_address = int(re.findall(r"\d+", operation)[0])
        memory_addresses = apply_memory_mask(mask, memory_address)
        for address in memory_addresses:
            memory[address] = int(value)

    return_value = 0
    for value in memory.values():
        return_value += value
    return return_value


print("Part 1:", part1(init_instructions))
print("Part 2:", part2(init_instructions))
