from common import puzzle_input_as_int

lines = puzzle_input_as_int(9)

def is_valid(number_to_test, preamble):
    for i in range(len(preamble)):
        for j in range(i+1, len(preamble)):
            if preamble[i] != preamble[j] and preamble[i] + preamble[j] == number_to_test:
                return True
    return False

def find_invalid_number(numbers, preamble_size=25):
    for i in range(preamble_size, len(numbers)):
        number_to_test = numbers[i]
        preamble = numbers[i-preamble_size:i]
        if is_valid(number_to_test, preamble):
            continue
        return number_to_test

def find_contiguous_range(numbers, number_to_find):
    for i in range(len(numbers)):
        cont_range = []
        for j in range(i+1, len(numbers)):
            cont_range.append(numbers[j])
            if sum(cont_range) > number_to_find:
                break
            if sum(cont_range) == number_to_find:
                return cont_range
    return []


def part1(numbers, preamble_size=25):
    return find_invalid_number(numbers, preamble_size)

def part2(numbers, preamble_size=25):
    invalid_number = find_invalid_number(numbers, preamble_size)
    number_range = find_contiguous_range(numbers, invalid_number)
    return min(number_range) + max(number_range)


print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
