from common import puzzle_input_as_str

lines = puzzle_input_as_str(12)


class Ship:

    def __init__(self):
        self.lat = 0
        self.long = 0
        self.heading = 0

    @property
    def direction(self):
        if self.heading == 0:
            return 'E'
        elif self.heading == 90:
            return 'S'
        elif self.heading == 180:
            return 'W'
        elif self.heading == 270:
            return 'N'

class Waypoint:

    def __init__(self, start_lat, start_long):
        self.lat = start_lat
        self.long = start_long


def move_waypoint(waypoint, direction, argument):
    if direction == 'N':
        waypoint.lat += argument
    elif direction == 'S':
        waypoint.lat -= argument
    elif direction == 'E':
        waypoint.long += argument
    elif direction == 'W':
        waypoint.long -= argument

def rotate_waypoint(waypoint, direction, argument):
    if argument == 180:
        waypoint.lat, waypoint.long = -waypoint.lat, -waypoint.long
    elif (direction == 'R' and argument == 90) or (direction == 'L' and argument == 270):
        waypoint.lat, waypoint.long = -waypoint.long, waypoint.lat
    elif (direction == 'L' and argument == 90) or (direction == 'R' and argument == 270):
        waypoint.lat, waypoint.long = waypoint.long, -waypoint.lat

def move_towards_waypoint(ship, waypoint, argument):
    ship.lat += waypoint.lat * argument
    ship.long += waypoint.long * argument


def move_ship(ship, direction, argument):
    if direction == 'N':
        ship.lat += argument
    elif direction == 'S':
        ship.lat -= argument
    elif direction == 'E':
        ship.long += argument
    elif direction == 'W':
        ship.long -= argument


def calculate_manhattan_distance(posX, posY):
    return abs(posX) + abs(posY)


def part1(directions):
    ship = Ship()
    for line in directions:
        operation = line[0]
        argument = int(line[1:])
        if operation == 'F':
            move_ship(ship, ship.direction, argument)
        elif operation in ['N', 'S', 'W', 'E']:
            move_ship(ship, operation, argument)
        elif operation == 'L':
            ship.heading = (ship.heading - argument) % 360
        elif operation == 'R':
            ship.heading = (ship.heading + argument) % 360

    return calculate_manhattan_distance(ship.lat, ship.long)


def part2(directions):
    ship = Ship()
    waypoint = Waypoint(1, 10)

    for line in directions:
        operation = line[0]
        argument = int(line[1:])
        if operation == 'F':
            move_towards_waypoint(ship, waypoint, argument)
        elif operation in ['N', 'S', 'W', 'E']:
            move_waypoint(waypoint, operation, argument)
        elif operation in ['L', 'R']:
            rotate_waypoint(waypoint, operation, argument)

    return calculate_manhattan_distance(ship.lat, ship.long)

print("Part 1:", part1(lines))
print("Part 2:", part2(lines))
