from common import puzzle_input_as_str

input_ingredients_and_allergens = puzzle_input_as_str(21)

def split_ingredients_allergens(string):
    ingredients, allergens = string.split(' (')
    ingredients = {ingredient for ingredient in ingredients.split()}
    allergens = allergens[9:-1].split(', ')
    return ingredients, allergens


def find_non_allergen_ingredients(ingredients_and_allergens):
    non_allergen_ingredients = []
    allergens_in = {}
    ingredients_has_allergen = {}
    for line in ingredients_and_allergens:
        ingredients, allergens = split_ingredients_allergens(line)
        for ingredient in ingredients:
            if ingredient not in ingredients_has_allergen:
                ingredients_has_allergen[ingredient] = None
        for allergen in allergens:
            if allergen not in allergens_in:
                allergens_in[allergen] = []
            allergens_in[allergen].append(ingredients)

    list_changed = True
    while list_changed:
        list_changed = False
        for allergen, contain_candidates in allergens_in.items():
            allergen_candidates = contain_candidates[0]
            for candidates in contain_candidates:
                if isinstance(candidates, set):
                    allergen_candidates = allergen_candidates.intersection(candidates)
                    if len(allergen_candidates) == 1:
                        item = allergen_candidates.pop()
                        ingredients_has_allergen[item] = allergen
                        remove_item_from_other_candidates(item, allergen, allergens_in)
                        list_changed = True
                        break

    for ingredient, allergen in ingredients_has_allergen.items():
        if not allergen:
            non_allergen_ingredients.append(ingredient)

    return non_allergen_ingredients, ingredients_has_allergen


def remove_item_from_other_candidates(item, allergen, allergens_in):
    for a_key, a_candidates in allergens_in.items():
        if a_key == allergen:
            allergens_in[a_key] = item
            continue
        for candidate_list in a_candidates:
            if isinstance(candidate_list, set):
                candidate_list.discard(item)


def part1(puzzle_input):
    non_allergen_ingredients, _ = find_non_allergen_ingredients(puzzle_input)

    found_times = 0
    for ingredient in non_allergen_ingredients:
        for line in puzzle_input:
            words = line.split()
            for word in words:
                if word == ingredient:
                    found_times += 1

    return found_times

def part2(puzzle_input):
    _, ingredients = find_non_allergen_ingredients(puzzle_input)
    ingredients = {key:value for key, value in ingredients.items() if value}
    sorted_by_allergen = sorted(ingredients.items(), key=lambda x:x[1])
    return ','.join([ingredient for ingredient,_ in sorted_by_allergen])


print("Part 1:", part1(input_ingredients_and_allergens))
print("Part 2:", part2(input_ingredients_and_allergens))
