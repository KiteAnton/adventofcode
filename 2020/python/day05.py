from common import puzzle_input_as_str

boarding_passes = puzzle_input_as_str(5)

def find_row(inputstring):
    range_min = 0
    range_max = 127
    for char in list(inputstring):
        range_step = (range_max - range_min + 1)/2
        if char == 'F':
            range_max -= range_step
        elif char == 'B':
            range_min += range_step
    return int(range_min)


def find_column(inputstring):
    range_min = 0
    range_max = 7
    for char in list(inputstring):
        range_step = (range_max - range_min + 1)/2
        if char == 'L':
            range_max -= range_step
        elif char == 'R':
            range_min += range_step
    return int(range_min)


def find_seat_id(boarding_pass):
    return find_row(boarding_pass[0:7]) * 8 + find_column(boarding_pass[7:10])


def find_taken_seats(boarding_passes):
    taken_seats = set()
    for line in boarding_passes:
        seat_id = find_row(line[0:7]) * 8 + find_column(line[7:10])
        taken_seats.add(seat_id)
    return taken_seats


def part1(boarding_passes):
    return max(find_taken_seats(boarding_passes))


def part2(boarding_passes):
    taken_seats = find_taken_seats(boarding_passes)
    all_seats = set(range(min(taken_seats), max(taken_seats)+1))
    your_seat = all_seats.difference(taken_seats)
    return min(your_seat)


print("Part1:", part1(boarding_passes))
print("Part2:", part2(boarding_passes))
