from common import puzzle_input_by_newlines

rules_raw, messages = puzzle_input_by_newlines(19)

def parse_rules(rules_raw):
    rules = {}
    for line in rules_raw:
        rule_index, rule = line.split(": ")
        if rule[0] == '"':
            rule = rule[1]
        else:
            rule = [rule.split(' ') if ' ' in rule else [rule]
                    for rule in (rule.split(' | ') if ' | 'in rule else [rule])]
        rules[rule_index] = rule
    return rules

def check_sequence(rules, sequence, message):
    if not sequence:
        yield message
    else:
        index, *sequence = sequence
        for s in check_rule(rules, index, message):
            yield from check_sequence(rules, sequence, s)


def check_alternatives(rules, alternatives, message):
    for sequence in alternatives:
        yield from check_sequence(rules, sequence, message)


def check_rule(rules, rule_index, message):
    if isinstance(rules[rule_index], list):
        yield from check_alternatives(rules, rules[rule_index], message)
    if message and message[0] == rules[rule_index]:
        yield message[1:]

def is_valid(rules, message):
    return any(not string
               for string in check_rule(rules, '0', message))

def part1(rules_raw, messages):
    rules = parse_rules(rules_raw)
    return sum(is_valid(rules, message) for message in messages)

def part2(rules_raw, messages):
    rules = parse_rules(rules_raw)
    rules['8'] = [['42'], ['42', '8']]
    rules['11'] = [['42', '31'], ['42', '11', '31']]
    return sum(is_valid(rules, message) for message in messages)


print("Part 1:", part1(rules_raw, messages))
print("Part 2:", part2(rules_raw, messages))
