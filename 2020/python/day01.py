from common import puzzle_input_as_int

def part1(numbers):
    for num1 in numbers:
        for num2 in numbers:
            if num1 + num2 == 2020:
                return num1*num2
        else:
            continue

def part2(numbers):
    for num1 in numbers:
        for num2 in numbers:
            for num3 in numbers:
                if num1 + num2 + num3 == 2020:
                    return num1*num2*num3
            else:
                continue
        else:
            continue


numbers = puzzle_input_as_int(1)

print("Answer part1: ", part1(numbers))
print("Answer part2: ", part2(numbers))
