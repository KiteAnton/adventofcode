import numpy as np
from common import puzzle_input_as_str

lines = puzzle_input_as_str(3)

def walk_slope(lines, step):
    tree_counter = 0
    position = np.array([0, 0])
    while (position[0] < len(lines)):
        next_position = position + step
        next_position[1] = next_position[1] % len(lines[0])
        if next_position[0] < len(lines):
            if lines[next_position[0]][next_position[1]] == '#':
                tree_counter += 1
        position = next_position
    return tree_counter

def part1(lines):
    step = np.array([1, 3]) # 1 down, 3 right
    return walk_slope(lines, step)

def part2(lines):
    slopes = [
        [1, 1],
        [1, 3],
        [1, 5],
        [1, 7],
        [2, 1]
    ]

    slope_product = 1
    for slope in slopes:
        slope_product *= walk_slope(lines, slope)
    return slope_product

print("Part1:", part1(lines))
print("Part2:", part2(lines))
