import heapq

from common import puzzle_input_as_str, is_inside, move_pos

DAY = 17

puzzle_input = puzzle_input_as_str(day=DAY)


def add_to_queue(
    heat_map: list,
    node: tuple,
    direction: tuple,
    steps: int,
    to_visit: list,
    boundaries: tuple,
    heat_loss: int,
):
    next_node = move_pos(node, direction)
    if is_inside(next_node, boundaries):
        heapq.heappush(
            to_visit,
            (
                heat_loss + heat_map[next_node[0]][next_node[1]],
                next_node,
                direction,
                steps,
            ),
        )


def process_turning_nodes(
    heat_map: list,
    node: tuple,
    direction: tuple,
    to_visit: list,
    boundaries: tuple,
    heat_loss: int,
):
    for new_direction in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
        if new_direction != direction and new_direction != (
            -direction[0],
            -direction[1],
        ):
            add_to_queue(
                heat_map, node, new_direction, 1, to_visit, boundaries, heat_loss
            )


def solve(heat_map: list, continue_straight, allow_turning) -> int:
    start, end = (0, 0), (len(heat_map) - 1, len(heat_map[0]) - 1)
    boundaries = ((0, len(heat_map) - 1), (0, len(heat_map[0]) - 1))
    to_visit = [(0, start, (0, 0), 0)]
    visited = set()

    while to_visit:
        heat_loss, node, direction, steps = heapq.heappop(to_visit)
        if node == end:
            return heat_loss
        if (node, direction, steps) in visited:
            continue
        visited.add((node, direction, steps))

        if continue_straight(steps, direction):
            add_to_queue(
                heat_map, node, direction, steps + 1, to_visit, boundaries, heat_loss
            )

        if allow_turning(steps, direction):
            process_turning_nodes(
                heat_map,
                node,
                direction,
                to_visit,
                boundaries,
                heat_loss,
            )
    return 0


def parse_input(puzzle_input: list) -> tuple:
    return tuple(tuple(int(x) for x in row) for row in puzzle_input)


def main():
    heat_map = parse_input(puzzle_input)
    p1 = solve(
        heat_map, lambda steps, dir: steps < 3 and dir != (0, 0), lambda _, __: True
    )
    print("Part 1:", p1)
    p2 = solve(
        heat_map,
        lambda steps, dir: steps < 10 and dir != (0, 0),
        lambda steps, dir: steps >= 4 or dir == (0, 0),
    )
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
