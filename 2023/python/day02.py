from collections import defaultdict

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=2)


def update_smallest_set(turn: str, smallest_set: dict):
    cubes = turn.split(",")
    for cube in cubes:
        _, amount, color = cube.split(" ")
        smallest_set[color] = max(smallest_set[color], int(amount))


def is_within_start_set(smallest_set: dict, start_set: dict) -> bool:
    return all(smallest_set[color] <= start_set[color] for color in start_set)


def main():
    start_set_p1 = {"red": 12, "green": 13, "blue": 14}
    p1_possible = 0
    p2_score = 0

    for line in puzzle_input:
        game, plays = line.split(":")
        game_number = int(game.split()[1])
        turns = plays.split(";")

        smallest_set = defaultdict(int)
        for turn in turns:
            update_smallest_set(turn, smallest_set)

        p2_score += smallest_set["red"] * smallest_set["green"] * smallest_set["blue"]
        if is_within_start_set(smallest_set, start_set_p1):
            p1_possible += game_number

    print("Part 1:", p1_possible)
    print("Part 2:", p2_score)


if __name__ == "__main__":
    main()
