from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=9)


def next_in_range(history: list, reverse: bool = False) -> int:
    if reverse:
        history.reverse()

    diff_range = [history[i + 1] - history[i] for i in range(len(history) - 1)]

    if all(val == 0 for val in diff_range):
        return history[-1]
    else:
        next_val = next_in_range(diff_range)
        return history[-1] + next_val


def calculate_sum(oasis_reports: list, reverse: bool = False) -> int:
    return sum(next_in_range(report, reverse) for report in oasis_reports)


def parse_values(oasis_report: list) -> list:
    return [[int(value) for value in line.split()] for line in oasis_report]


def main():
    oasis_reports = parse_values(puzzle_input)
    print("Part 1:", calculate_sum(oasis_reports))
    print("Part 2:", calculate_sum(oasis_reports, reverse=True))


if __name__ == "__main__":
    main()
