import sympy

from common import puzzle_input_as_str

DAY = 24

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> list:
    return [
        [int(val) for val in line.replace("@", ",").split(",")] for line in puzzle_input
    ]


def part1(hailstones: list) -> int:
    def calculate_line2d(hailstone: list) -> tuple:
        px, py, _, vx, vy, _ = hailstone
        k = vy / vx
        m = py - k * px
        return (k, m)

    def intersection_point(hailstone_A: list, hailstone_B: list) -> tuple:
        def is_past(hailstone: list, x: int) -> bool:
            px, _, _, vx, _, _ = hailstone
            return (vx < 0 and x > px) or (vx > 0 and x < px)

        a, b = calculate_line2d(hailstone_A)
        c, d = calculate_line2d(hailstone_B)

        if a == c:
            return None, None

        x = (d - b) / (a - c)

        if is_past(hailstone_A, x) or is_past(hailstone_B, x):
            return None, None

        y = a * x + b
        return (x, y)

    def is_inside_intersection(hailstone_A: list, hailstone_B: list) -> bool:
        inside = (200_000_000_000_000, 400_000_000_000_000)
        x, y = intersection_point(hailstone_A, hailstone_B)
        return (
            x is not None
            and inside[0] <= x <= inside[1]
            and inside[0] <= y <= inside[1]
        )

    result = 0
    for i, hailstone_A in enumerate(hailstones):
        for j, hailstone_B in enumerate(hailstones[i + 1 :]):
            if is_inside_intersection(hailstone_A, hailstone_B):
                result += 1

    return result


def part2(hailstones: list) -> int:
    def build_equations(
        hailstones: list, x_rock, y_rock, z_rock, vx_rock, vy_rock, vz_rock
    ) -> list:
        equations = []

        for x_h, y_h, z_h, vx_h, vy_h, vz_h in hailstones:
            equations.extend(
                [
                    (x_rock - x_h) * (vy_h - vy_rock)
                    - (y_rock - y_h) * (vx_h - vx_rock),
                    (y_rock - y_h) * (vz_h - vz_rock)
                    - (z_rock - z_h) * (vy_h - vy_rock),
                ]
            )
        return equations

    symbols = sympy.symbols("x_rock, y_rock, z_rock, vx_rock, vy_rock, vz_rock")

    # Rock and hailstone collide at `t` when
    # x_hailstone + t * vx_hailstone == x_rock + t * vx_rock
    # y_hailstone + t * vy_hailstone == y_rock + t * vy_rock
    # z_hailstone + t * vz_hailstone == z_rock + t * vz_rock
    # i.e. t = (x_rock - x_hailstone) / (vx_hailstone - vx_rock)
    # and similar for y and z
    #
    # Can be setup as 2 equations (for each hailstone)
    # (x_r - x_h) * (vy_h - vy_r) - (y_r - y_h) * (vx_h - vx_r) = 0
    # (y_r - y_h) * (vz_h - vz_r) - (z_r - z_h) * (vy_h - vy_r) = 0

    # 6 unknown variables, assume first 3 hailstones gives unique solution
    equations = build_equations(hailstones[:4], *symbols)
    solutions = sympy.solve(equations)
    assert len(solutions) == 1
    solution = solutions[0]

    return sum(solution[symbol] for symbol in symbols[:3])


def main():
    hailstones = parse_input(puzzle_input)
    print("Part 1:", part1(hailstones))
    print("Part 2:", part2(hailstones))


if __name__ == "__main__":
    main()
