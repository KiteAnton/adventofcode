from common import puzzle_input_as_str, move_pos, shoelace_area

DAY = 18

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> list:
    return [
        [direction, int(steps), color[2:-1]]
        for direction, steps, color in (line.split() for line in puzzle_input)
    ]


def instruction_from_hex(color: str) -> tuple:
    direction_map = {
        "0": "R",
        "1": "D",
        "2": "L",
        "3": "U",
    }
    steps = int(color[:-1], 16)
    return direction_map.get(color[-1]), steps


def lagoon_area(dig_map: list) -> int:
    coordinates = [(0, 0)]
    directions = {"R": (0, 1), "D": (1, 0), "L": (0, -1), "U": (-1, 0)}
    current = (0, 0)
    # coordinates.append(current)

    # Since coordinates is center of each square shoelace formula (below) don't give full area.
    # Each straigh square/block gives an extra 1/2 area, and each corner an additional 3/4 area if inward and 1/4 if outward. Since the trench is closed, each outward corner will be responded with an inward corner and in the end there will be an extra 4 inward corners.
    # This result in total area of "shoelace area" + number of bloks / 2 + 1 (4 corners)

    area = 0
    for direction, steps, *_ in dig_map:
        direction = directions.get(direction)
        current = move_pos(current, (direction[0] * steps, direction[1] * steps))
        area += steps
        coordinates.append(current)

    return area // 2 + 1 + shoelace_area(coordinates)


def main():
    dig_map = parse_input(puzzle_input)
    print("Part 1:", lagoon_area(dig_map))
    print(
        "Part 2:",
        lagoon_area(instruction_from_hex(instruction[2]) for instruction in dig_map),
    )


if __name__ == "__main__":
    main()
