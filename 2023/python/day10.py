from common import puzzle_input_as_str, shoelace_area

puzzle_input = puzzle_input_as_str(day=10)


def run_map(start_position: tuple, puzzle_input: list) -> list:
    def get_connections(char: str, row: int, col: int) -> tuple:
        directions = {
            "|": ((row - 1, col), (row + 1, col)),
            "-": ((row, col - 1), (row, col + 1)),
            "L": ((row - 1, col), (row, col + 1)),
            "J": ((row - 1, col), (row, col - 1)),
            "7": ((row + 1, col), (row, col - 1)),
            "F": ((row + 1, col), (row, col + 1)),
        }
        return directions[char]

    row, col = start_position
    visited = [start_position]
    previous = start_position

    if puzzle_input[row][col + 1] in "7-J":
        col += 1
    elif puzzle_input[row][col - 1] in "L-F":
        col -= 1
    elif puzzle_input[row + 1][col] in "L|J":
        row += 1

    while (row, col) != start_position:
        visited.append((row, col))
        for new_pos in get_connections(puzzle_input[row][col], row, col):
            if new_pos != previous:
                break
        previous = (row, col)
        row, col = new_pos

    return visited


def pipe_area(coordinates: list) -> int:
    pipe_length = len(coordinates)
    coordinates.append(coordinates[0])
    return shoelace_area(coordinates) - pipe_length // 2 + 1


def find_start(puzzle_input: list) -> tuple:
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if char == "S":
                return (row, col)
    return (None, None)


def main():
    start_pos = find_start(puzzle_input)
    visited = run_map(start_pos, puzzle_input)
    p1 = len(visited) // 2
    p2 = pipe_area(visited)
    print("Part 1:", p1)
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
