from common import puzzle_input_as_str
from collections import defaultdict

DAY = 12

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> tuple:
    springs, damaged_springs = [], []
    for line in puzzle_input:
        spring_row, damaged_row = line.split()
        damaged_springs.append(list(map(int, damaged_row.split(","))))
        springs.append(spring_row)

    return springs, damaged_springs


visited = defaultdict(int)


def nr_arrangements(springs: str, damaged_springs: list) -> int:
    # Exit conditions
    if not springs:
        return int(not damaged_springs)
    if not damaged_springs:
        if "#" not in springs:
            return 1
        return 0

    key = (springs, tuple(damaged_springs))

    if key in visited:
        return visited[key]

    # Try options
    char = springs[0]
    arrangements = 0
    if char in "#?":
        if (
            damaged_springs[0] <= len(springs)
            and "." not in springs[: damaged_springs[0]]
            and (
                damaged_springs[0] == len(springs) or springs[damaged_springs[0]] != "#"
            )
        ):
            arrangements += nr_arrangements(
                springs[damaged_springs[0] + 1 :], damaged_springs[1:]
            )

    if char in ".?":
        arrangements += nr_arrangements(springs[1:], damaged_springs)

    visited[key] = arrangements
    return arrangements


def solve(springs: str, damaged_springs: list, extend: int = 1) -> int:
    result = 0
    for spring_row, damaged_row in zip(springs, damaged_springs):
        result += nr_arrangements("?".join([spring_row] * extend), damaged_row * extend)

    return result


def main():
    springs, damaged_springs = parse_input(puzzle_input)
    print("Part 1:", solve(springs, damaged_springs))
    print("Part 2:", solve(springs, damaged_springs, extend=5))


if __name__ == "__main__":
    main()
