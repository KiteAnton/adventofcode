from collections import deque

from common import puzzle_input_as_str

DAY = 19


puzzle_input = puzzle_input_as_str(day=DAY, separator="\n\n")


def parse_input(puzzle_input: list) -> tuple:
    rules = parse_rules(puzzle_input[0])
    ratings = parse_ratings(puzzle_input[1])
    return rules, ratings


def parse_rules(rules_input: list) -> dict:
    rules = {}
    for rule in rules_input:
        name, rest = rule.split("{")
        rules_for_name = rest[:-1].split(",")
        rules[name] = [parse_rule_condition(rn) for rn in rules_for_name]
    return rules


def parse_ratings(ratings_input: list) -> list:
    return [
        {
            name: int(value)
            for name, value in (vp.split("=") for vp in rating[1:-1].split(","))
        }
        for rating in ratings_input
    ]


def parse_rule_condition(rule_condition: str) -> list:
    if ":" in rule_condition:
        condition, result = rule_condition.split(":")
        cond, value = condition.split("<") if "<" in condition else condition.split(">")
        return [cond, "<" if "<" in condition else ">", int(value), result]
    return rule_condition


def parse_rule(rules: list, rating: list) -> str:
    for rule in rules:
        if not isinstance(rule, list):
            return rule
        val_name, cond, value, result = rule
        if cond == "<" and rating[val_name] < value:
            return result
        elif cond == ">" and rating[val_name] > value:
            return result
    return False


def part1(rules: dict, ratings: list) -> int:
    result = 0
    for rating in ratings:
        res = parse_rule(rules["in"], rating)
        while res not in "AR":
            res = parse_rule(rules[res], rating)
        if res == "A":
            result += sum(rating.values())

    return result


def part2(rules: dict) -> int:
    queue = deque([("in", (1, 4000), (1, 4000), (1, 4000), (1, 4000))])

    p2_result = 0
    while queue:
        rule_name, x, m, a, s = queue.popleft()
        if rule_name == "R":
            continue
        if rule_name == "A":
            p2_result += (
                (x[1] - x[0] + 1)
                * (m[1] - m[0] + 1)
                * (a[1] - a[0] + 1)
                * (s[1] - s[0] + 1)
            )
            continue

        next_range = None
        for rule in rules[rule_name]:
            if isinstance(rule, list):
                val_name, cond, value, result = rule
                if next_range:
                    x, m, a, s = next_range
                pass_range, next_range = range_for_cond(
                    val_name, cond, value, (x, m, a, s)
                )
                if pass_range:
                    queue.append((result, *pass_range))
            else:
                if next_range:
                    queue.append((rule, *next_range))

    return p2_result


def new_ranges(
    current_min: int, current_max: int, value: int, less_than: bool
) -> tuple:
    assert current_min < value < current_max
    if less_than:
        return (current_min, min(current_max, value - 1)), (
            max(current_min, value),
            current_max,
        )
    return (max(current_min, value + 1), current_max), (
        current_min,
        min(current_max, value),
    )


def range_for_cond(val_name: str, cond: str, value: int, current_range: tuple) -> tuple:
    x, m, a, s = current_range
    x2, m2, a2, s2 = current_range
    if val_name == "x":
        x, x2 = new_ranges(x[0], x[1], value, cond == "<")

    elif val_name == "m":
        m, m2 = new_ranges(m[0], m[1], value, cond == "<")

    elif val_name == "a":
        a, a2 = new_ranges(a[0], a[1], value, cond == "<")

    elif val_name == "s":
        s, s2 = new_ranges(s[0], s[1], value, cond == "<")

    return ((x, m, a, s), (x2, m2, a2, s2))


def main():
    rules, ratings = parse_input(puzzle_input)

    p1 = part1(rules, ratings)
    p2 = part2(rules)
    print("Part 1:", p1)
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
