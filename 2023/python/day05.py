from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=5, separator="\n\n")


def merge_offset(seeds: list) -> list:
    return [(start, start + offset - 1) for start, offset in seeds]


def location_from_seed(seed: int, converters: list) -> int:
    for converter in converters:
        for start, end, offset in converter:
            if start <= seed <= end:
                seed += offset
                break

    return seed


def merge_ranges(ranges: list) -> list:
    if not ranges:
        return []
    merged = [ranges[0]]
    for start, end in ranges[1:]:
        last_start, last_end = merged[-1]
        if start - 1 <= last_end:
            merged[-1] = (last_start, max(end, last_end))
        else:
            merged.append((start, end))
    return merged


def fix_converter_maps(maps: list) -> list:
    converters = []
    for line in maps:
        converter_group = []
        for new_map in line[1:]:
            dest, start, length = map(int, new_map.split())
            converter_group.append((start, start + length - 1, dest - start))

        converter_group.sort()
        converters.append(converter_group)
    return converters


def split_range(seed_range: list, converter_map: list) -> list:
    new_ranges = []
    seed_start, seed_end = seed_range
    for conv_start, conv_end, offset in converter_map:
        if conv_end < seed_start:
            continue
        if conv_start > seed_end:
            break
        if conv_start > seed_start:
            new_ranges.append((seed_start, conv_start - 1))
        new_range_end = min(conv_end, seed_end) + offset
        new_ranges.append((seed_start + offset, new_range_end))
        seed_start = min(conv_end, seed_end) + 1
    if seed_end >= seed_start:
        new_ranges.append((seed_start, seed_end))
    return new_ranges


def part1(seeds: list, converters: list) -> int:
    return min(location_from_seed(seed, converters) for seed in seeds)


def part2(seeds: list, converters: list) -> int:
    for converter_map in converters:
        new_entries = [
            subrange
            for entry in seeds
            for subrange in split_range(entry, converter_map)
        ]
        new_entries.sort()
        seeds = merge_ranges(new_entries)
        if not seeds:
            return 0
    return seeds[0][0]


def main():
    seeds = [int(seed) for seed in puzzle_input[0][0].split()[1:]]
    converters = fix_converter_maps(puzzle_input[1:])
    print("Part1: ", part1(seeds, converters))

    seed_pairs = zip(seeds[::2], seeds[1::2])
    print("Part2: ", part2(merge_offset(seed_pairs), converters))


if __name__ == "__main__":
    main()
