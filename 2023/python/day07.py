from common import (
    puzzle_input_as_str,
    char_histogram,
)

puzzle_input = puzzle_input_as_str(day=7)


def rank_for_cards(cards: str, use_joker: bool) -> int:
    char_occurrences = char_histogram(cards)
    jokers = char_occurrences["J"] if use_joker else 0
    char_occurrences["J"] = 0 if use_joker else char_occurrences["J"]

    sorted_values = sorted(char_occurrences.values(), reverse=True)
    if len(sorted_values) < 2:
        return 6

    first, second = sorted_values[:2]
    first += jokers

    rank_mapping = {
        5: 6,
        4: 5,
        3: 4 if second == 2 else 3,
        2: 2 if second == 2 else 1,
    }
    return rank_mapping.get(first, 0)


def points_for_cards(cards: str, use_joker: bool) -> list:
    points = []
    card_to_points = {"A": 14, "K": 13, "Q": 12, "J": 11, "T": 10}
    if use_joker:
        card_to_points["J"] = 1

    for card in cards:
        if card in card_to_points:
            points.append(card_to_points[card])
        else:
            points.append(int(card))

    return points


def sort_hands(hands: list, use_joker: bool = False) -> list:
    sorted_hands = []

    for hand in hands:
        cards, bid = hand.split()
        cards_rank = rank_for_cards(cards, use_joker)
        cards_score = points_for_cards(cards, use_joker)
        sorted_hands.append((cards_rank, cards_score, cards, bid))

    return sorted(sorted_hands)


def main():
    hands = sort_hands(puzzle_input)
    p1 = sum((index + 1) * int(bid) for index, (*_, bid) in enumerate(hands))
    print("Part 1:", p1)

    hands = sort_hands(puzzle_input, use_joker=True)
    p2 = sum((index + 1) * int(bid) for index, (*_, bid) in enumerate(hands))
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
