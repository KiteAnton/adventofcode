from collections import deque
from functools import cache

from common import find_adjacent_positions_2d, puzzle_input_as_str

DAY = 21

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> tuple:
    garden_map = set()
    start = (0, 0)
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if char == "#":
                garden_map.add((row, col))
            elif char == "S":
                start = (row, col)
    return garden_map, start


def plots_reachable(
    garden_map: set, start: tuple, grid_size: int, steps_needed: int
) -> int:
    visited = set(start)

    queue = deque([(start, steps_needed)])

    end_pos = set()

    while queue:
        position, steps = queue.popleft()

        # Positions with odd number of steps cannot be reached on last step (that is even)
        if steps % 2 == 0:
            end_pos.add(position)
        if steps == 0:
            continue

        for pos in find_adjacent_positions_2d(position):
            pos_wrapped = (pos[0] % grid_size, pos[1] % grid_size)
            if pos_wrapped in garden_map or pos in visited:
                continue
            queue.append((pos, steps - 1))
            visited.add(pos)

    return len(end_pos)


def part2(garden_map: set, start: tuple, grid_size: int) -> int:
    # Reachable plots is a function of number of grids,
    # ax**2 + bx + c
    # Where x is a function of 2 * grid_size + one half grid (since starting in the centre)
    @cache
    def f(x):
        steps_needed = 2 * x * grid_size + grid_size // 2
        return plots_reachable(garden_map, start, grid_size, steps_needed)

    # Values for a,b,c can be retrieved by solving f(0), f(1), f(2), i.e:
    x_values = [2 * x * grid_size + grid_size // 2 for x in range(3)]

    c = f(0)
    a = (f(2) - 2 * f(1) + c) / 2
    b = f(1) - c - a

    def final_solution(x):
        return a * x**2 + b * x + c

    total_steps = 26501365  # Given
    return int(final_solution(total_steps // (2 * grid_size)))


def main():
    garden_map, start = parse_input(puzzle_input)
    print("Part 1:", plots_reachable(garden_map, start, len(puzzle_input), 64))
    print("Part 2:", part2(garden_map, start, len(puzzle_input)))


if __name__ == "__main__":
    main()
