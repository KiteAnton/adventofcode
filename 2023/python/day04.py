from collections import defaultdict

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=4)


def extract_cards(cards: str) -> set:
    return set(map(int, cards.split()))


def main():
    hand_counter = defaultdict(int)
    total_score = 0

    for line in puzzle_input:
        card_info, numbers_info = line.split(":")
        card_number = int(card_info.split()[1])
        hand_counter[card_number] += 1
        winning_numbers, our_numbers = map(extract_cards, numbers_info.split(" | "))
        score = len(our_numbers & winning_numbers)
        if score:
            total_score += 2 ** (score - 1)
            for i in range(card_number + 1, card_number + score + 1):
                hand_counter[i] += 1 * hand_counter[card_number]

    print("Part 1:", total_score)
    print("Part 2:", sum(hand_counter.values()))


if __name__ == "__main__":
    main()
