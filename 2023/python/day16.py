from collections import defaultdict, deque

from common import puzzle_input_as_str, move_pos

DAY = 16

puzzle_input = puzzle_input_as_str(day=DAY)


def outside_grid(pos: tuple, boundaries: tuple) -> bool:
    return not (0 <= pos[0] < boundaries[0] and 0 <= pos[1] < boundaries[1])


def get_next_direction(tile: str, direction: tuple) -> tuple:
    if tile == "/":
        return -direction[1], -direction[0]
    elif tile == "\\":
        return direction[1], direction[0]

    elif tile == "|" and not direction[0]:
        return direction[1], direction[0]
    elif tile == "-" and not direction[1]:
        return direction[1], direction[0]
    return direction


def get_alternative_direction(tile: str, direction: tuple) -> tuple:
    if tile == "|" and not direction[0]:
        return -direction[1], direction[0]
    elif tile == "-" and not direction[1]:
        return direction[1], -direction[0]
    else:
        return None, None


def solve(grid: dict, boundaries: tuple, current: tuple, direction: tuple) -> int:
    visited = set()
    move_queue = deque([(current, direction)])

    while move_queue:
        current, direction = move_queue.popleft()
        if outside_grid(current, boundaries) or (current, direction) in visited:
            continue

        visited.add((current, direction))

        tile = grid.get(current, "")
        next_direction = get_next_direction(tile, direction)
        next_pos = move_pos(current, next_direction)

        if tile in "|-":
            alternative_direction = get_alternative_direction(tile, direction)
            if alternative_direction:
                move_queue.append(
                    (move_pos(current, alternative_direction), alternative_direction)
                )

        move_queue.append((next_pos, next_direction))

    return len(set(pos for pos, _ in visited))


def parse_input(puzzle_input: list) -> tuple:
    grid = {
        (row, col): char
        for row, line in enumerate(puzzle_input)
        for col, char in enumerate(line)
        if char in "|-/\\"
    }
    return grid, (len(puzzle_input), len(puzzle_input[0]))


def main():
    grid, boundaries = parse_input(puzzle_input)
    print("Part 1:", solve(grid, boundaries, (0, 0), (0, 1)))

    p2 = 0
    for row in range(boundaries[0]):
        p2 = max(p2, solve(grid, boundaries, (row, 0), (0, 1)))
        p2 = max(p2, solve(grid, boundaries, (row, boundaries[1] - 1), (0, -1)))

    for col in range(boundaries[1]):
        p2 = max(p2, solve(grid, boundaries, (0, col), (1, 0)))
        p2 = max(p2, solve(grid, boundaries, (boundaries[0] - 1, col), (-1, 0)))

    print("Part 2:", p2)


if __name__ == "__main__":
    main()
