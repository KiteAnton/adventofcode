from collections import defaultdict, deque

from common import puzzle_input_as_str, find_adjacent_positions_2d


DAY = 23

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> tuple:
    forest, slopes = set(), defaultdict(str)
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            assert char in ".#<>v"

            if char == "#":
                forest.add((row, col))
            elif char in "<>v":
                slopes[(row, col)] = char

    end = (len(puzzle_input) - 1, len(puzzle_input[0]) - 2)
    return forest, slopes, end


def longest_trail(forest: set, slopes: dict, end: tuple) -> int:
    def next_position(position: tuple) -> list:
        if position in slopes:
            dx, dy = {"<": (0, -1), ">": (0, 1), "v": (1, 0)}.get(
                slopes[position], (0, 0)
            )
            return [(position[0] + dx, position[1] + dy)]
        else:
            return find_adjacent_positions_2d(
                position, moves=4, boundaries=((0, end[0] + 1), (0, end[1] + 1))
            )

    def compressed_graph_trails() -> dict:
        trails = defaultdict(dict)
        junctions = {start, end}
        for row in range(end[0] + 1):
            for col in range(end[1] + 1):
                position = (row, col)
                if (
                    position in forest
                    or len(
                        [
                            opt
                            for opt in find_adjacent_positions_2d(position)
                            if opt not in forest
                        ]
                    )
                    < 3
                ):
                    continue
                junctions.add(position)

        for junction in junctions:
            queue = deque([(0, junction)])
            visited = set(junction)

            while queue:
                length, pos = queue.popleft()

                if pos in junctions and length > 0 and pos != junction:
                    trails[junction][pos] = length

                else:
                    for option in next_position(pos):
                        if option not in visited and option not in forest:
                            queue.append((length + 1, option))
                            visited.add(option)
        return trails

    visited = set()

    def dfs(node: tuple) -> int:
        if node == end:
            return 0

        visited.add(node)

        best_hike = -1e9
        for next_trail in trails[node]:
            if next_trail in visited:
                continue
            best_hike = max(best_hike, dfs(next_trail) + trails[node][next_trail])
        visited.discard(node)

        return best_hike

    start = (0, 1)
    trails = compressed_graph_trails()

    return dfs(start)


def main():
    forest, slopes, end = parse_input(puzzle_input)
    print("Part 1:", longest_trail(forest, slopes, end))
    print("Part 2:", longest_trail(forest, set(), end))


if __name__ == "__main__":
    main()
