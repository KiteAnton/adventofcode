from tqdm import tqdm
import string
from collections import defaultdict, deque
from copy import copy, deepcopy
from pprint import pprint

from common import (
    find_common_char_in_strings,
    manhattan_distance,
    puzzle_input_as_int,
    puzzle_input_as_str,
)

DAY=14

puzzle_input = puzzle_input_as_str(day=DAY)
puzzle_input = puzzle_input_as_str(day=DAY, test=1)

puzzle_input = puzzle_input_as_str(day=DAY, separator="\n\n")
puzzle_input = puzzle_input_as_str(day=DAY, separator="\n\n", test=1)

puzzle_input = puzzle_input_as_int(day=DAY)
puzzle_input = puzzle_input_as_int(day=DAY, test=1)

puzzle_input = puzzle_input_as_int(day=DAY, separator="\n\n")
puzzle_input = puzzle_input_as_int(day=DAY, separator="\n\n", test=1)


def part1(puzzle_input):
    return


def part2(puzzle_input):
    return


def main():
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))


if __name__ == "__main__":
    main()
main()
