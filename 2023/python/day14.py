from common import puzzle_input_as_str

DAY = 14

puzzle_input = puzzle_input_as_str(day=DAY)


def calculate_weight(grid: list) -> int:
    return sum((i + 1) * row.count("O") for i, row in enumerate(reversed(grid)))


def print_map(grid: list):
    for row in grid:
        print("".join(row))


def move(grid: list):
    cols = [list(row) for row in zip(*grid)]
    for col in cols:
        for i in range(1, len(col)):
            if col[i] == "O":
                for j in range(i - 1, -1, -1):
                    if col[j] in "#O":
                        col[i] = "."
                        col[j + 1] = "O"
                        break
                else:
                    col[0] = "O"
                    col[i] = "."

    return list(zip(*cols))


def rotate(grid: list) -> list:
    return ["".join(row[i] for row in reversed(grid)) for i in range(len(grid))]


def cycle(grid: list) -> list:
    for _ in range(4):
        grid = move(grid)
        grid = rotate(grid)
    return grid


def main():
    grid = move(puzzle_input)
    print("Part 1:", calculate_weight(grid))

    iterations = 1_000_000_000
    visited = {}
    steps = 0
    while steps < iterations:
        grid = cycle(grid)
        state = tuple(tuple(row) for row in grid)
        if state in visited:
            delta = steps - visited[state]
            steps += ((iterations - steps) // delta) * delta
        else:
            visited[state] = steps
        steps += 1

    print("Part 2:", calculate_weight(grid))


if __name__ == "__main__":
    main()
