from collections import defaultdict
import os

fill_char = "█"


def read_puzzle_input(day: int, test: int) -> list:
    try:
        file_extra = ""
        if test:
            file_extra = f"_test_{test}"

        with open(
            os.path.join("../input/", f"{day:02d}{file_extra}"), "r"
        ) as file_input:
            return file_input.readlines()
    except FileNotFoundError:
        return []


def puzzle_input_as_int(day: int, separator: str = "", test: int = 0) -> list:
    inputs = read_puzzle_input(day, test)
    if not separator:
        try:
            return [int(line) for line in inputs]
        except (ValueError, TypeError):
            return []
    grouped_result = []
    for group in ("".join(inputs)).split(separator):
        try:
            grouped_result.append([int(line) for line in group.split("\n") if line])
        except (ValueError, TypeError) as e:
            grouped_result.append([])

    return grouped_result


def puzzle_input_as_str(day: int, separator: str = "", test: int = 0) -> list:
    inputs = read_puzzle_input(day, test)
    if not separator:
        return [line.rstrip() for line in inputs]

    grouped_result = []
    for group in ("".join(inputs)).split(separator):
        try:
            grouped_result.append([line.rstrip() for line in group.split("\n") if line])
        except (ValueError, TypeError) as e:
            grouped_result.append([])

    return grouped_result


def is_inside(position: tuple, boundaries: tuple) -> bool:
    ((row_min, row_max), (col_min, col_max)) = boundaries
    (row, col) = position

    if row_min <= row <= row_max and col_min <= col <= col_max:
        return True
    return False


def move_pos(pos: tuple, direction: tuple) -> tuple:
    return pos[0] + direction[0], pos[1] + direction[1]


def find_common_char_in_strings(str1, str2, str3=""):
    for char in str1:
        if str3:
            if char in str2 and char in str3:
                return char
        else:
            if char in str2:
                return char
    return ""


def char_histogram(chars: str) -> dict:
    histogram = defaultdict(int)
    for char in chars:
        histogram[char] += 1
    return histogram


def manhattan_distance(value1: list, value2: list) -> int:
    return abs(value1[0] - value2[0]) + abs(value1[1] - value2[1])


def find_adjacent_positions_2d(
    current_position: tuple, moves: int = 4, boundaries: tuple = ()
) -> list:
    assert moves in [4, 8]

    row, col = current_position

    if moves == 4:
        possible_movements = [[-1, 0], [0, 1], [1, 0], [0, -1]]
    else:
        possible_movements = [
            [-1, 0],
            [-1, 1],
            [0, 1],
            [1, 1],
            [1, 0],
            [1, -1],
            [0, -1],
            [-1, -1],
        ]
    options = []
    for row_offset, col_offset in possible_movements:
        new_row = row + row_offset
        new_col = col + col_offset
        if boundaries:
            ((min_row, max_row), (min_col, max_col)) = boundaries
            if min_row <= new_row <= max_row and min_col <= new_col <= max_col:
                options.append((new_row, new_col))
        else:
            options.append((new_row, new_col))

    return options


def find_adjacent_positions_3d(
    current_position: tuple, boundaries: tuple, moves: int
) -> list:
    assert moves in [6, 26]
    if moves == 6:
        possible_movements = [
            [-1, 0, 0],
            [1, 0, 0],
            [0, -1, 0],
            [0, 1, 0],
            [0, 0, -1],
            [0, 0, 1],
        ]
    else:
        raise NotImplementedError

    options = []
    cur_x, cur_y, cur_z = current_position
    ((x_min, x_max), (y_min, y_max), (z_min, z_max)) = boundaries
    for x_offset, y_offset, z_offset in possible_movements:
        x, y, z = cur_x + x_offset, cur_y + y_offset, cur_z + z_offset
        if x_min <= x <= x_max and y_min <= y <= y_max and z_min <= z <= z_max:
            options.append((x, y, z))

    return options


def find_boundaries(positions: set) -> tuple:
    rows = [row for row, _ in positions]
    cols = [col for _, col in positions]
    return ((min(rows), max(rows)), (min(cols), max(cols)))

def shoelace_area(coordinates: list) -> int:
    assert coordinates[0] == coordinates[-1]
    area = 0
    for i in range(len(coordinates) - 1):
        area += coordinates[i][0] * coordinates[i + 1][1]
        area -= coordinates[i][1] * coordinates[i + 1][0]
    return abs(area) // 2
