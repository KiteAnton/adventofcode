from collections import defaultdict

from common import puzzle_input_as_str

DAY = 15

puzzle_input = puzzle_input_as_str(day=DAY)


def calculate_hash(sequence: str) -> int:
    current = 0
    for char in sequence:
        current += ord(char)
        current *= 17
        current = current % 256
    return current


def update_boxes(boxes: dict, label_hash: int, label: str, value: str, subtract: bool):
    if subtract:
        boxes[label_hash] = [
            (existing_label, val)
            for existing_label, val in boxes[label_hash]
            if existing_label != label
        ]
        return
    for i, (existing_label, _) in enumerate(boxes[label_hash]):
        if existing_label == label:
            boxes[label_hash][i] = (label, value)
            return
    boxes[label_hash].append((label, value))


def process_sequences(sequences: list) -> dict:
    boxes = defaultdict(list)
    for sequence in sequences:
        subtract = "-" in sequence
        label, value = sequence.split("-" if subtract else "=")
        label_hash = calculate_hash(label)
        update_boxes(boxes, label_hash, label, value, subtract)
    return boxes


def main():
    sequences = puzzle_input[0].split(",")
    p1 = sum(calculate_hash(sequence) for sequence in sequences)
    print("Part 1:", p1)

    boxes = process_sequences(sequences)
    p2 = sum(
        (box_index + 1) * (item_index + 1) * int(value)
        for box_index, items in boxes.items()
        for item_index, (_, value) in enumerate(items)
    )
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
