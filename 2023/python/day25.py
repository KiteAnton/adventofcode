from collections import defaultdict, deque

from common import puzzle_input_as_str

DAY = 25

puzzle_input = puzzle_input_as_str(day=DAY)


def wiring_from_input(puzzle_input: list, cuts: list) -> dict:
    def is_cut(node1: str, node2: str, cuts: list) -> bool:
        return any((node1, node2) == cut or (node2, node1) == cut for cut in cuts)

    wiring = defaultdict(set)
    for line in puzzle_input:
        node, *connections = line.replace(":", "").split()
        for connection in connections:
            if not is_cut(node, connection, cuts):
                wiring[node].add(connection)
                wiring[connection].add(node)
    return wiring

def node_group_size(start: str, wiring: dict) -> int:
    group, queue = {start}, deque([start])
    while queue:
        node = queue.popleft()
        for connection in wiring[node]:
            if connection not in group:
                queue.append(connection)
                group.add(connection)

    return len(group)


def main():
    # The cuts can be found by inspecting the graph visually.
    # E.g. by using graphviz.
    # for e.g. the test input the graphviz input would be
    # graph G {
    #   jqt -- rhn,xhk,nvd
    #   rsh -- frs,pzl,lsr
    #   xhk --  hfx
    #   cmg --  qnr,nvd,lhk,bvb
    #   rhn --  xhk,bvb,hfx
    #   bvb --  xhk,hfx
    #   pzl --  lsr,hfx,nvd
    #   qnr --  nvd
    #   ntq --  jqt,hfx,bvb,xhk
    #   nvd --  lhk
    #   lsr --  lhk
    #   rzs --  qnr,cmg,lsr,rsh
    #   frs --  qnr,lhk,lsr
    # }
    # and then create graph with
    #  > dot -Tsvg <input_file> -o out.svg -Kneato

    # For test input
    # cuts = [("pzl", "hfx"), ("cmg", "bvb"), ("jqt", "nvd")]
    # For real input
    cuts = [("pgl", "mtl"), ("lkf", "scf"), ("zxb", "zkv")]

    wiring = wiring_from_input(puzzle_input, cuts)
    result = node_group_size(cuts[0][0], wiring)  * node_group_size(cuts[0][1], wiring)

    print("Part 1:", result)


if __name__ == "__main__":
    main()
