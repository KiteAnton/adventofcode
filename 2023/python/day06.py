from common import puzzle_input_as_str
from math import prod

puzzle_input = puzzle_input_as_str(day=6)


def parse_input(race_result: list) -> tuple:
    times = [int(time) for time in puzzle_input[0].split() if time.isdigit()]
    distances = [
        int(distance) for distance in puzzle_input[1].split() if distance.isdigit()
    ]
    return times, distances


def run_race(time: int, distance_to_beat: int) -> int:
    distances = [(time - hold) * hold for hold in range(time)]
    return sum(distance > distance_to_beat for distance in distances)


def run_race_approx(time: int, distance_to_beat: int) -> int:
    for hold in range(time):
        distance = (time - hold) * hold
        if distance >= distance_to_beat:
            return time - hold * 2 + 1
    return 0


def part1(race_times: list, distances: list) -> int:
    return prod(
        run_race(time, distance) for time, distance in zip(race_times, distances)
    )


def part2(race_times: list, distances: list) -> int:
    race_time = int("".join(map(str, race_times)))
    race_distance = int("".join(map(str, distances)))

    return run_race_approx(race_time, race_distance)


def main():
    race_times, distances = parse_input(puzzle_input)
    print("Part 1:", part1(race_times, distances))
    print("Part 2:", part2(race_times, distances))


if __name__ == "__main__":
    main()
