from collections import defaultdict, deque
from enum import Enum
from functools import cache

from common import puzzle_input_as_str

DAY = 22

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_brick(line: str) -> tuple:
    line = line.replace("~", ",")
    brick_positions = tuple(int(pos) for pos in line.split(","))
    return (brick_positions[:3]), (brick_positions[3:])


def parse_input(puzzle_input: list) -> list:
    bricks = [parse_brick(line) for line in puzzle_input]
    bricks.sort(key=lambda brick: brick[0][2])
    return bricks


@cache
def cube_occupation(cube_positions: tuple) -> set:
    occupied = set()
    (x1, y1, z1), (x2, y2, z2) = cube_positions

    for x in range(min(x1, x2), max(x1, x2) + 1):
        for y in range(min(y1, y2), max(y1, y2) + 1):
            for z in range(min(z1, z2), max(z1, z2) + 1):
                occupied.add((x, y, z))

    return occupied


class Direction(Enum):
    UP = 1
    DOWN = -1


@cache
def brick_up_down(brick: tuple, direction: Direction = Direction.DOWN) -> tuple:
    b_pos1, b_pos2 = brick[0], brick[1]
    return (
        (b_pos1[0], b_pos1[1], b_pos1[2] + direction.value),
        (b_pos2[0], b_pos2[1], b_pos2[2] + direction.value),
    )


def move_bricks(bricks: set) -> list:
    occupied = set()
    new_bricks = []

    def add_brick(brick: tuple):
        new_bricks.append(brick)
        occupied.update(cube_occupation(brick))

    def move_brick_down(brick: tuple) -> tuple:
        while True:
            if is_brick_at_bottom(brick) or is_space_occupied(brick):
                return brick
            brick = brick_up_down(brick, Direction.DOWN)

    def is_brick_at_bottom(brick: tuple) -> bool:
        return min(brick[0][2], brick[1][2]) <= 1

    def is_space_occupied(brick: tuple) -> bool:
        next_volume = cube_occupation(brick_up_down(brick, Direction.DOWN))
        return next_volume.intersection(occupied)

    for brick in bricks:
        new_brick = move_brick_down(brick)
        add_brick(new_brick)

    new_bricks.sort(key=lambda brick: brick[0][2])
    return new_bricks


def find_neighboring_bricks(bricks: set) -> tuple:
    supported_by = defaultdict(set)
    supports = defaultdict(set)
    for i, brick in enumerate(bricks):
        volume_down = cube_occupation(brick_up_down(brick))
        volume_up = cube_occupation(brick_up_down(brick, Direction.UP))
        for j, other_brick in enumerate(bricks):
            if brick == other_brick:
                continue
            if volume_down.intersection(cube_occupation(other_brick)):
                supported_by[i].add(j)
            if volume_up.intersection(cube_occupation(other_brick)):
                supports[i].add(j)

    return supports, supported_by


def find_can_be_disintigrated(supported_by: dict, nr_bricks: int) -> int:
    can_be_disintegrated = set(brick for brick in range(nr_bricks))

    for brick in [brick for brick, *rest in supported_by.values() if not rest]:
        can_be_disintegrated.discard(brick)

    return len(can_be_disintegrated)


def part2(supports: dict, supported_by: dict, nr_bricks: int) -> int:
    result = 0

    for i in range(nr_bricks):
        queue = deque(brick for brick in supports[i] if len(supported_by[brick]) == 1)
        non_supported_bricks = set(queue)

        while queue:
            brick = queue.popleft()
            for next_brick in set(
                next_brick
                for next_brick in supports[brick]
                if next_brick not in non_supported_bricks
            ):
                if supported_by[next_brick].issubset(non_supported_bricks):
                    queue.append(next_brick)
                    non_supported_bricks.add(next_brick)

        result += len(non_supported_bricks)
    return result


def main():
    bricks = parse_input(puzzle_input)
    bricks = move_bricks(bricks)
    supports, supported_by = find_neighboring_bricks(bricks)

    print("Part 1:", find_can_be_disintigrated(supported_by, len(bricks)))
    print("Part 2:", part2(supports, supported_by, len(bricks)))


if __name__ == "__main__":
    main()

# import cProfile
# cProfile.run('main()')
