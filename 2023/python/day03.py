from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=3)


class SerialNumber:
    def __init__(self, row: int, col: int):
        self.value = 0
        self.row = row
        self.col_start = col
        self.col_end = col + 1

    def update_value(self, char: str, col: int):
        self.value = self.value * 10 + int(char)
        self.col_end = col


def find_numbers_and_symbols(engine_schematics):
    numbers, symbols = [], {}
    current = None

    for row, line in enumerate(engine_schematics):
        for col, char in enumerate(line):
            if char.isdigit():
                if not current:
                    current = SerialNumber(row, col)

                current.update_value(char, col)
            else:
                if current:
                    numbers.append(current)
                    current = None
                if char != ".":
                    symbols[(row, col)] = char

        if current:
            numbers.append(current)
            current = None

    return numbers, symbols


def adjacent_set(range_row: range, range_col: range) -> set:
    return set((r, c) for r in range_row for c in range_col)


def part1(numbers: list, symbols_pos: set) -> int:
    return sum(
        number.value
        for number in numbers
        if adjacent_set(
            range(number.row - 1, number.row + 2),
            range(number.col_start - 1, number.col_end + 2),
        )
        & symbols_pos
    )


def part2(numbers: list, symbols: dict) -> int:
    gear_symbols = {
        (row, col) for (row, col), symbol in symbols.items() if symbol == "*"
    }
    result = 0

    for symbol_pos in gear_symbols:
        found_numbers = [
            number
            for number in numbers
            if symbol_pos
            in adjacent_set(
                range(number.row - 1, number.row + 2),
                range(number.col_start - 1, number.col_end + 2),
            )
        ]
        if len(found_numbers) == 2:
            result += found_numbers[0].value * found_numbers[1].value

    return result


def main():
    numbers, symbols = find_numbers_and_symbols(puzzle_input)
    symbols_pos = set(symbols.keys())
    print("Part 1:", part1(numbers, symbols_pos))
    print("Part 2:", part2(numbers, symbols))


if __name__ == "__main__":
    main()
