from common import puzzle_input_as_str

DAY = 13

puzzle_input = puzzle_input_as_str(day=DAY, separator="\n\n")


def find_reflection_line(pattern: list, smudge_count: int = 0) -> int:
    for step in range(1, len(pattern)):
        if mismatches(reversed(pattern[:step]), pattern[step:]) == smudge_count:
            return step

    return 0


def mismatches(a, b) -> int:
    mismatches = 0
    for row_a, row_b in zip(a, b):
        mismatches += sum([1 if c1 != c2 else 0 for c1, c2 in zip(row_a, row_b)])

    return mismatches


def solve(puzzle_input: list, smudge_count: int = 0) -> int:
    result = 0
    for rows in puzzle_input:
        # Rows
        result += find_reflection_line(rows, smudge_count=smudge_count) * 100
        # Columns
        result += find_reflection_line(list(zip(*rows)), smudge_count=smudge_count)
    return result


def main():
    print("Part 1:", solve(puzzle_input))
    print("Part 2:", solve(puzzle_input, smudge_count=1))


if __name__ == "__main__":
    main()
