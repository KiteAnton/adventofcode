import math

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=8)


def parse_input(puzzle_input: list) -> tuple:
    instructions = [
        int(char.replace("L", "0").replace("R", "1")) for char in puzzle_input[0]
    ]

    nodes = {
        row.split(" = ")[0]: tuple(row.split(" = ")[1].strip("()").split(", "))
        for row in puzzle_input[2:]
    }
    return instructions, nodes


def navigate_maze(
    instructions: list, nodes: dict, start_node: str, target_suffix: str
) -> int:
    current_node = nodes[start_node]
    for step in range(100_000):
        direction = instructions[step % len(instructions)]
        next_node = current_node[direction]
        if next_node.endswith(target_suffix):
            return step + 1
        current_node = nodes[next_node]
    return 0


def main():
    instructions, nodes = parse_input(puzzle_input)
    p1 = navigate_maze(instructions, nodes, start_node="AAA", target_suffix="ZZZ")
    print("Part 1:", p1)
    ghost_nodes = [node for node in nodes if node.endswith("Z")]
    ghost_paths = [
        navigate_maze(instructions, nodes, start_node=node, target_suffix="Z")
        for node in ghost_nodes
    ]
    p2 = math.lcm(*ghost_paths)
    print("Part 2:", p2)


if __name__ == "__main__":
    main()
