from itertools import product

from common import manhattan_distance, puzzle_input_as_str

DAY = 11

puzzle_input = puzzle_input_as_str(day=DAY)


def find_distance(
    a: tuple, b: tuple, empty_rows: set, empty_cols: set, expansion: int
) -> int:
    a_r, a_c = a
    b_r, b_c = b
    passes_empty_row = sum(
        [row in empty_rows for row in range(min(a_r, b_r), max(a_r, b_r) + 1)]
    )
    passes_empty_col = sum(
        [col in empty_cols for col in range(min(a_c, b_c), max(a_c, b_c) + 1)]
    )
    return (
        manhattan_distance(a, b)
        + passes_empty_row * (expansion - 1)
        + passes_empty_col * (expansion - 1)
    )


def total_distance(
    galaxy_pairs: set, empty_rows: set, empty_cols: set, expansion: int = 2
) -> int:
    return sum(
        find_distance(a, b, empty_rows, empty_cols, expansion)
        for a, b in galaxy_pairs
        if a != b
    )


def parse_input(puzzle_input: list) -> tuple:
    galaxies = set()
    empty_rows = set(range(len(puzzle_input)))
    empty_cols = set(range(len(puzzle_input[0])))

    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if char == "#":
                galaxies.add((row, col))
                empty_rows.discard(row)
                empty_cols.discard(col)

    return galaxies, empty_rows, empty_cols


def main():
    galaxies, empty_rows, empty_cols = parse_input(puzzle_input)
    galaxy_pairs = {tuple(sorted(pair)) for pair in product(galaxies, repeat=2)}

    print("Part 1:", total_distance(galaxy_pairs, empty_rows, empty_cols))
    print(
        "Part 2:",
        total_distance(galaxy_pairs, empty_rows, empty_cols, expansion=1_000_000),
    )


if __name__ == "__main__":
    main()
