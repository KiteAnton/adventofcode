import re
from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=1)


def find_printed_numbers(line: str) -> list:
    number_words = [
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ]
    return [
        (n.start(), number_words.index(word) + 1)
        for word in number_words
        for n in re.finditer(word, line)
    ]


def sum_first_and_last(numbers: list) -> int:
    return sum([row[0][1] * 10 + row[-1][1] for row in numbers])


def main():
    digits = [
        [(index, int(char)) for index, char in enumerate(line) if char.isdigit()]
        for line in puzzle_input
    ]
    printed_numbers = [find_printed_numbers(line) for line in puzzle_input]
    combined = [sorted(a + b) for a, b in zip(digits, printed_numbers)]

    print("Part 1:", sum_first_and_last(digits))
    print("Part 1:", sum_first_and_last(combined))


if __name__ == "__main__":
    main()
