from collections import defaultdict, deque
from pprint import pprint
from math import lcm


from common import puzzle_input_as_str

DAY = 20

puzzle_input = puzzle_input_as_str(day=DAY)


def parse_input(puzzle_input: list) -> tuple:
    modules = defaultdict(dict)
    signal_broker = SignalBroker()

    for line in puzzle_input:
        name, receivers = line.split(" -> ")
        module_type, name = (
            (None, name) if name == "broadcaster" else (name[0], name[1:])
        )
        module_class = {"%": FlipFlop, "&": Conjunction}.get(module_type, Module)
        modules[name] = module_class(name, receivers.split(", "), signal_broker)

    for module in modules.values():
        module.register(modules)

    return modules, signal_broker


class Signal:
    def __init__(self, sender: str, receiver: str, value: bool):
        self.sender = sender
        self.receiver = receiver
        self.value = value

    def __repr__(self):
        signal_text = "-high->" if self.value else "-low->"
        return f"{self.sender} {signal_text} {self.receiver}"


class SignalBroker:
    def __init__(self):
        self.signals = deque([])
        self.low_signals = 0
        self.high_signals = 0

    def add_signal(self, signal: Signal):
        self.signals.append(signal)

    def broadcast_signals(self, modules: dict, checker=None):
        while self.signals:
            signal = self.signals.popleft()
            if callable(checker):
                checker(signal)
            if signal.value:
                self.high_signals += 1
            else:
                self.low_signals += 1
            if signal.receiver in modules:
                receiver = modules[signal.receiver]
                receiver.signal(signal)


class Module:
    def __init__(self, name: str, receivers: list, signal_broker: SignalBroker):
        self.name = name
        self.receivers = receivers
        self.signal_broker = signal_broker

    def signal(self, signal: Signal):
        self.add_signal(signal.value)

    def add_signal(self, value: bool):
        for receiver in self.receivers:
            self.signal_broker.add_signal(
                Signal(sender=self.name, receiver=receiver, value=value)
            )

    def register(self, modules: dict):
        for receiver in self.receivers:
            if receiver not in modules:
                continue
            receiver_module = modules[receiver]
            if isinstance(receiver_module, Conjunction):
                receiver_module.add_as_sender(self.name)

    def __repr__(self):
        return self.name + ",receivers=" + ",".join(self.receivers)


class FlipFlop(Module):
    def __init__(self, name: str, receivers: list, modules: dict):
        super().__init__(name, receivers, modules)
        self.state = False

    def signal(self, signal: Signal):
        if not signal.value:
            self.state = not self.state
            self.add_signal(self.state)

    def __repr__(self):
        return "FlipFlop:" + super().__repr__() + ", " + f"{self.state=}"


class Conjunction(Module):
    def __init__(self, name: str, receivers: list, modules: dict):
        super().__init__(name, receivers, modules)
        self.senders = defaultdict(bool)

    def all_high(self) -> bool:
        return all([signal for signal in self.senders.values()])

    def signal(self, signal: Signal):
        self.senders[signal.sender] = signal.value
        if self.all_high():
            self.add_signal(False)
        else:
            self.add_signal(True)

    def add_as_sender(self, name: str):
        self.senders[name] = False

    def __repr__(self):
        all_high = self.all_high()
        return (
            "Conjunction:" + super().__repr__() + ", " + f"{all_high=} {self.senders=}"
        )


def push_button(modules: dict, signal_broker: SignalBroker, checker=None):
    signal_broker.add_signal(Signal(sender=None, receiver="broadcaster", value=False))
    signal_broker.broadcast_signals(modules, checker)


def part1(modules: dict, signal_broker: SignalBroker) -> int:
    for _ in range(1000):
        push_button(modules, signal_broker)
    return signal_broker.low_signals * signal_broker.high_signals


def part2(modules: dict, signal_broker: SignalBroker) -> int:
    # rx has only a signal sender, this is a conjunction node
    rx_sender = next(
        sender for sender, module in modules.items() if "rx" in module.receivers
    )
    # rx_sender will send low pulse once it receives a high pulse from all its "senders"
    # Keep track of when each sending node sends a high pulse and calculate lcm for all.
    seen = defaultdict(int)

    steps = 0

    def checker(signal: Signal):
        if signal.receiver == "tg" and signal.value and not seen[signal.sender]:
            seen[signal.sender] = steps

    while len(seen) < len(modules[rx_sender].senders):
        steps += 1
        push_button(modules, signal_broker, checker)

    return lcm(*seen.values())


def main():
    modules, signal_broker = parse_input(puzzle_input)
    print("Part 1:", part1(modules, signal_broker))

    modules, signal_broker = parse_input(puzzle_input)
    print("Part 2:", part2(modules, signal_broker))


if __name__ == "__main__":
    main()
