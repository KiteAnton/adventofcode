#+TITLE: Day 07: The Treachery of Whales
#+STARTUP: overview

[[https://adventofcode.com/2021/day/7][https://adventofcode.com/2021/day/7]]


* Problem part 1
Puzzle input consists of horizontal positions for a lot of crab submarines(!). The problem is to move them all into the same horizontal position. Find the positions with the least overall moves.

Moving one horizontal step costs 1 fuel.

** Test input part 1
With the input:
#+begin_src
16,1,2,0,4,2,7,1,2,14
#+end_src
The ending horizontal position for this seris would be 2.

- Move from 16 to 2: 14 fuel
- Move from 1 to 2: 1 fuel
- Move from 2 to 2: 0 fuel
- Move from 0 to 2: 2 fuel
- Move from 4 to 2: 2 fuel
- Move from 2 to 2: 0 fuel
- Move from 7 to 2: 5 fuel
- Move from 1 to 2: 1 fuel
- Move from 2 to 2: 0 fuel
- Move from 14 to 2: 12 fuel

 This then costs total of 37 fuel.
 Some other solutions (but with higher cost) would be position 1 (41 fuel), position 3 (39 fuel) and position 10 (71 fuel).

** Solution part 1
Input file specified in *input/07*
Split up the input by ',' and add each value to a list.

- Find the minimum and maximum for series.
- Loop through each position from minimum and maximum.
  - Calculate the cost of moving all the crabs towards that position. Add the absolute value with 'current crab position' - 'target position'. Sum all those together.
    - Compare the result with previous and store only if lower than the previous.

Return the position with the lowest cost.

Answer to part1: 333755

* Problem part 2
For part 2 the cost of moving has changed.
For each move there is an additional cost of 1. I.e. first step costs 1 fuel, second step costs 2 fuel, third step costs 3 steps and so on.

I.e. moving 3 steps will cost 1+2+3 = 6 fuel
** Test input
With the same test input as for part1. The cheapest position will be 5, with the following moves.
#+begin_src
- Move from 16 to 5: 66 fuel
- Move from 1 to 5: 10 fuel
- Move from 2 to 5: 6 fuel
- Move from 0 to 5: 15 fuel
- Move from 4 to 5: 1 fuel
- Move from 2 to 5: 6 fuel
- Move from 7 to 5: 3 fuel
- Move from 1 to 5: 10 fuel
- Move from 2 to 5: 6 fuel
- Move from 14 to 5: 45 fuel
#+end_src
This will cost a total of 168 fuel.

** Solution part 2
Solution similar as for part 1 but instead of a fixed cost * distance the cost must be as a function of distance.

Calculate the cost for distance X as the cost for distance X-1 + X.
This value should be stored/cached in a dictionary/lookup table.

- This large numbers of this problem makes this infeasible to implement using recursive function.
  - Instead when searching for cost for X, if it is not already calculated then calculate all the values up until X.

Answer to part2: 94017638

* Finished solutions
- [[file:python/day07.py][Python 2021 Day 7]]
