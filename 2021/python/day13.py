from common import puzzle_input_by_newlines

paper_instructions = puzzle_input_by_newlines(13)


def setup_paper(paper_coordinates):
    paper = set()
    for line in paper_coordinates:
        x, y = list(map(int, line.split(',')))
        paper.add((x,y))

    return paper


def fold(paper, fold_method):
    *_, fold_spot = fold_method.split(' ')
    direction, value = fold_spot.split('=')

    value = int(value)
    old_copy = paper.copy()
    for dot in old_copy:
        x, y = dot
        if direction == 'y':
            if y > value:
                new_dot = (x, 2*value - y)
                if new_dot != dot:
                    paper.add(new_dot)
                    paper.remove(dot)
        elif direction == 'x':
            if x > value:
                new_dot = (2*value - x, y)
                if new_dot != dot:
                    paper.add(new_dot)
                    paper.remove(dot)

def visualize_fold(paper):
    visual_representation = "\n"
    x_max = 0
    y_max = 0

    for dot in paper:
        x, y = dot
        x_max = max(x_max, x)
        y_max = max(y_max, y)

    for row in range(y_max + 1):
        new_row = ''
        for col in range(x_max + 1):
            if (col, row) in paper:
                new_row += '#'
            else:
                new_row += '.'

        visual_representation += new_row + "\n"

    return visual_representation



def part1(puzzle_input):
    paper = setup_paper(puzzle_input[0])

    fold(paper, puzzle_input[1][0])

    return len(paper)

def part2(puzzle_input):
    paper = setup_paper(puzzle_input[0])
    for folds in puzzle_input[1]:
        fold(paper, folds)

    return visualize_fold(paper)


print("Part 1:", part1(paper_instructions))
print("Part 2:", part2(paper_instructions))
