from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(24)
puzzle_input_test = puzzle_input_as_str(24, 1)


def read_input(arguments, variables, user_input):
    variables[arguments[0]] = int(user_input[0])
    return user_input[1:]


def multiply(arguments, variables, user_input):
    variables[arguments[0]] *= value_to_use(arguments[1], variables)
    return user_input


def add(arguments, variables, user_input):
    variables[arguments[0]] += value_to_use(arguments[1], variables)
    return user_input


def divide(arguments, variables, user_input):
    variables[arguments[0]] //= value_to_use(arguments[1], variables)
    return user_input


def modulo(arguments, variables, user_input):
    variables[arguments[0]] %= value_to_use(arguments[1], variables)
    return user_input


def equal(arguments, variables, user_input):
    if variables[arguments[0]] == value_to_use(arguments[1], variables):
        variables[arguments[0]] = 1
    else:
        variables[arguments[0]] = 0
    return user_input


def value_to_use(identifier, variables):
    if identifier in variables:
        return variables[identifier]
    return int(identifier)


def extract_key_values(instructions):
    key_values = []
    for i in range(14):
        inp_offset = i * 18
        assert instructions[inp_offset] == "inp w"
        if instructions[inp_offset + 4] == "div z 1":
            A = None
            B = int(instructions[inp_offset + 15].split(" ")[-1])
        else:
            A = int(instructions[inp_offset + 5].split(" ")[-1])
            B = None
        key_values.append((A, B))

    return key_values


def parse_instructions(instructions, user_input):

    function_mapping = {
        "inp": read_input,
        "mul": multiply,
        "add": add,
        "div": divide,
        "mod": modulo,
        "eql": equal,
    }

    variables = {"w": 0, "x": 0, "y": 0, "z": 0}

    for instruction in instructions:
        operation, *arguments = instruction.split()
        user_input = function_mapping[operation](arguments, variables, user_input)

    return variables


def solve(input_numbers, key_values):
    input_numbers = list(input_numbers)
    answer = []
    z = 0

    input_index = 0
    for k_value in key_values:
        check, offset = k_value
        if not check:
            z = 26 * z + input_numbers[input_index] + offset
            answer.append(input_numbers[input_index])
            input_index += 1
            continue
        proposal = z % 26 + check
        if not 1 <= proposal <= 9:
            return False, answer, input_index - 1
        z //= 26
        answer.append(proposal)

    return True, answer, None


def decrease(input_values, position, diff=1):
    input_values[position] -= diff
    input_values[position + 1 :] = [9] * (len(input_values) - position - 1)
    for i in range(len(input_values)):
        if input_values[-i - 1] < 1:
            input_values[-i - 1] = 9
            if i < len(input_values) - 1:
                input_values[-i - 2] -= 1
            else:
                return False

    return input_values


def increase(input_values, position, diff=1):
    input_values[position] += diff
    input_values[position + 1 :] = [1] * (len(input_values) - position - 1)
    for i in range(len(input_values)):
        if input_values[-i - 1] > 9:
            input_values[-i - 1] = 1
            if i < len(input_values) - 1:
                input_values[-i - 2] += 1
            else:
                return False

    return input_values


def part1(part1_input):
    key_values = extract_key_values(part1_input)
    input_values = [9] * 7

    while True:
        success, answer, fault_index = solve(input_values, key_values)
        if success:
            variables = parse_instructions(part1_input, answer)
            assert not variables["z"]
            return "".join(map(str, answer))

        if not decrease(input_values, fault_index):
            return None


def part2(part2_input):
    key_values = extract_key_values(part2_input)
    input_values = [1] * 7

    while True:
        success, answer, fault_index = solve(input_values, key_values)
        if success:
            variables = parse_instructions(part2_input, answer)
            assert not variables["z"]
            return "".join(map(str, answer))

        if not increase(input_values, fault_index):
            return None


if __name__ == "__main__":
    print("Part 1: ", part1(puzzle_input))
    print("Part 2: ", part2(puzzle_input))

    # import cProfile
    # cProfile.run('print("Part 1:", part1(puzzle_input))')
    # cProfile.run('print("Part 2:", part2(puzzle_input))')
