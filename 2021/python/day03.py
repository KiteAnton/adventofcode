from common import puzzle_input_as_str

diagnostic_codes = puzzle_input_as_str(3)


def most_common_bit(binary_numbers, position):
    ones = 0
    for number in binary_numbers:
        ones += int(number[position])

    if ones >= len(binary_numbers)/2:
        return '1'
    return '0'


def part1(puzzle_input):
    nr_bits = len(puzzle_input[0])

    gamma_rate = []
    epsilon_rate = []
    for i in range(nr_bits):
        most_common = most_common_bit(puzzle_input, i)
        gamma_rate.append(most_common)
        if most_common == '1':
            epsilon_rate.append('0')
        else:
            epsilon_rate.append('1')


    return int(''.join(gamma_rate), 2) * int(''.join(epsilon_rate), 2)

def part2(puzzle_input):

    # Oxygen generator rating
    remaining_values = puzzle_input.copy()
    for i in range(len(puzzle_input[0])):
        remaining_values = remaining_values.copy()
        if len(remaining_values) == 1:
            break
        most_common = most_common_bit(remaining_values, i)
        values_to_be_removed = []
        for value in remaining_values:
            if value[i] != most_common:
                values_to_be_removed.append(value)

        for value in values_to_be_removed:
            remaining_values.remove(value)
    oxygen_generator_rating = int(remaining_values[0], 2)

    # CO2 scrubber rating
    remaining_values = puzzle_input.copy()
    for i in range(len(puzzle_input[0])):
        remaining_values = remaining_values.copy()
        if len(remaining_values) == 1:
            break
        most_common = most_common_bit(remaining_values, i)
        values_to_be_removed = []
        for value in remaining_values:
            if value[i] == most_common:
                values_to_be_removed.append(value)

        for value in values_to_be_removed:
            remaining_values.remove(value)

    co2_scrubber_rating = int(remaining_values[0], 2)

    return oxygen_generator_rating * co2_scrubber_rating

print("Part 1:", part1(diagnostic_codes))
print("Part 2:", part2(diagnostic_codes))
