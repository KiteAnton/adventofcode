from copy import deepcopy
from collections import deque
from common import puzzle_input_by_newlines

scanner_beacons = puzzle_input_by_newlines(19)

def setup_scanner_beacons(puzzle_input_1):
    puzzle_input = deepcopy(puzzle_input_1)
    scanner_beacons = {}

    for scanner_data in puzzle_input:
        scanner_id = int(scanner_data.pop(0)[12:].split(' ')[0])
        scanner_beacons[scanner_id] = []
        for scan in scanner_data:
            scanner_beacons[scanner_id].append(
                tuple(int(value) for value in scan.split(',')))

    return scanner_beacons

def calculate_hyp_distance(point1, point2):
    b1_x, b1_y, b1_z = point1
    b2_x, b2_y, b2_z = point2
    return (b1_x - b2_x) * (b1_x - b2_x) \
        + (b1_y - b2_y) * (b1_y - b2_y) \
        + (b1_z - b2_z) * (b1_z - b2_z)

def calculate_beacon_distances(scanner_beacons):
    beacon_distances = {}
    if not isinstance(scanner_beacons, list):
        scanner_beacons = list(scanner_beacons)

    for i, beacon_i in enumerate(scanner_beacons):
        for j in range(i+1, len(scanner_beacons)):
            beacon_j = scanner_beacons[j]
            b_distance = calculate_hyp_distance(beacon_i, beacon_j)
            beacon_distances[b_distance] = (beacon_i, beacon_j)

    scanner_beacons = set(scanner_beacons)
    return beacon_distances

def find_matching_beacons(scanner1_beacons, scanner2_beacons):
    scanner1_distances = calculate_beacon_distances(scanner1_beacons)
    scanner2_distances = calculate_beacon_distances(scanner2_beacons)

    matching_scanner1 = set()
    matching_scanner2 = set()

    matching_distances = set(scanner1_distances.keys()) \
        .intersection(set(scanner2_distances.keys()))

    for dist in matching_distances:
        matching_scanner1.update(list(scanner1_distances[dist]))
        matching_scanner2.update(list(scanner2_distances[dist]))

    return matching_scanner1, matching_scanner2


def find_scanner_transformation(scanner1_beacons, scanner2_beacons):
    matching_scanner1, matching_scanner2 = \
       find_matching_beacons(scanner1_beacons, scanner2_beacons)

    if len(matching_scanner1) < 12:
        return None, None, None

    sc1_sorted = sorted(list(matching_scanner1))
    sc2_sorted = sorted(list(matching_scanner2))

    offset = []
    sign_flip = [False, False, False]
    shift_position = [0, 0, 0]
    position_done = [False, False, False]

    diff_adjustment = 0
    while sum(position_done) < 3:
        sc2_sorted = sorted(list(matching_scanner2))
        for k, position in enumerate(sc2_sorted):
            position = list(position)
            for n in range(3):
                if sign_flip[n]:
                    position[n] = -sc2_sorted[k][(n + shift_position[n]) % 3]
                else:
                    position[n] = sc2_sorted[k][(n + shift_position[n]) % 3]
            sc2_sorted[k] = tuple(position)
        sc2_sorted = sorted(sc2_sorted)
        for i in range(3):
            if position_done[i]:
                continue
            diff_vs_position_sc1 = []
            diff_vs_position_sc2 = []

            prev_value_sc1 = sc1_sorted[0][i]
            prev_value_sc2 = sc2_sorted[0][i]
            for j, position in enumerate(sc1_sorted):
                sc1_pos_diff = position[i] - prev_value_sc1
                sc2_pos_diff = sc2_sorted[j][i] - prev_value_sc2
                diff_vs_position_sc1.append(sc1_pos_diff)
                diff_vs_position_sc2.append(sc2_pos_diff)
                prev_value_sc1 = position[i]
                prev_value_sc2 = sc2_sorted[j][i]

            allow_once = True
            for k, diff in enumerate(diff_vs_position_sc1):
                if not allow_once and diff_adjustment:
                    tmp_diff_adjustment = 0
                else:
                    tmp_diff_adjustment = diff_adjustment

                if tmp_diff_adjustment == -1 and k < 2:
                    continue
                if k+tmp_diff_adjustment == len(diff_vs_position_sc2):
                    break
                if diff != diff_vs_position_sc2[max(0, k+tmp_diff_adjustment)]:
                    if not diff_adjustment:
                        if k > 0 and diff == diff_vs_position_sc2[k-1]:
                            diff_adjustment = -1
                            continue
                        if k < len(diff_vs_position_sc1) - 1 \
                             and diff == diff_vs_position_sc2[k+1]:
                            diff_adjustment = 1
                            continue
                    elif allow_once:
                        allow_once = False
                        continue
                    if not sign_flip[i]:
                        sign_flip[i] = True
                        break
                    if shift_position[i] < 2:
                        shift_position[i] += 1
                        sign_flip[i] = False
                        break
                    else:
                        pass
            else:
                position_done[i] = True
                continue
            break

    sc1_b_pos = max(0, -diff_adjustment)
    sc2_b_pos = max(0, diff_adjustment)

    offset = [sc1_sorted[sc1_b_pos][0] - sc2_sorted[sc2_b_pos][0],
            sc1_sorted[sc1_b_pos][1] - sc2_sorted[sc2_b_pos][1],
            sc1_sorted[sc1_b_pos][2] - sc2_sorted[sc2_b_pos][2]]

    return offset, sign_flip, shift_position


def transform_scanner_result(scanner_beacons,
                             offset, flip_sign, shift_positions):

    for i, position in enumerate(scanner_beacons):
        position = list(position)
        for k in range(3):
            if flip_sign[k]:
                position[k] = - scanner_beacons[i][(k + shift_positions[k]) % 3]
            else:
                position[k] = scanner_beacons[i][(k + shift_positions[k]) % 3]
            position[k] += offset[k]
        scanner_beacons[i] = tuple(position)

def all_beacons(scanner_beacons):

    scanners = deque(scanner_beacons.values())

    done_scanners = []
    done_scanners.append(scanners.popleft())

    all_offsets = []

    while scanners:
        sc_new = scanners.popleft()

        for scanner in done_scanners:
            offset, sign_flip, shift_position = \
                find_scanner_transformation(scanner, sc_new)
            if not offset:
                continue
            transform_scanner_result(sc_new, offset, sign_flip, shift_position)
            done_scanners.append(sc_new)
            all_offsets.append(offset)
            break
        else:
            scanners.append(sc_new)

    all_beacons = set()
    for scanner in done_scanners:
        all_beacons.update(scanner)

    return all_beacons, all_offsets

def part1(puzzle_input):
    scanner_beacons = setup_scanner_beacons(puzzle_input)

    all_unique_beacons, _ = all_beacons(scanner_beacons)

    return len(all_unique_beacons)

def part2(puzzle_input):
    scanner_beacons = setup_scanner_beacons(puzzle_input)

    _, all_offsets = all_beacons(scanner_beacons)

    max_manhattan = 0
    for offset_1 in all_offsets:
        x, y, z = offset_1
        for offset_2 in all_offsets:
            x2, y2, z2 = offset_2
            manhattan_distance = abs(x-x2) + abs(y-y2) + abs(z-z2)
            max_manhattan = max(manhattan_distance, max_manhattan)

    return max_manhattan

print("Part 1:", part1(scanner_beacons))
print("Part 2:", part2(scanner_beacons))
