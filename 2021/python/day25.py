from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(25)
puzzle_input_test = puzzle_input_as_str(25, 1)


def setup_map(map_input):
    east_facing = set()
    south_facing = set()

    for row_index, row in enumerate(map_input):
        for col, char in enumerate(row):
            if char == ">":
                east_facing.add((row_index, col))
            elif char == "v":
                south_facing.add((row_index, col))

    return east_facing, south_facing, (len(map_input), len(map_input[0]))


def move_step(east_facing, south_facing, map_constraints):
    occupied = east_facing.union(south_facing)
    rows, cols = map_constraints
    no_change = True

    additions = set()
    deletes = set()

    for row, col in east_facing:
        if (row, (col + 1) % cols) not in occupied:
            no_change = False
            additions.add((row, (col + 1) % cols))
            deletes.add((row, col))

    if additions:
        for new in additions:
            east_facing.add(new)
        for old in deletes:
            east_facing.remove(old)

    additions = set()
    deletes = set()

    occupied = east_facing.union(south_facing)
    for row, col in south_facing:
        if ((row + 1) % rows, col) not in occupied:
            no_change = False
            additions.add(((row + 1) % rows, col))
            deletes.add((row, col))

    if additions:
        for new in additions:
            south_facing.add(new)
        for old in deletes:
            south_facing.remove(old)

    return no_change


def part1(part1_input):
    east_facing, south_facing, map_constraints = setup_map(part1_input)
    iterations = 0
    no_change = False
    while not no_change and iterations < 10000:
        no_change = move_step(east_facing, south_facing, map_constraints)
        iterations += 1

    return iterations


if __name__ == "__main__":
    print("Part 1: ", part1(puzzle_input))

    # import cProfile
    # cProfile.run('print("Part 1:", part1(puzzle_input))')
    # cProfile.run('print("Part 2:", part2(puzzle_input))')
