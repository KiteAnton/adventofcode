from common import puzzle_input_as_int


sonar_sweeps = puzzle_input_as_int(1)


def part1(puzzle_input):
    increments = 0
    prev_value = puzzle_input[0]
    for value in puzzle_input:
        if value > prev_value:
            increments += 1
        prev_value = value

    return increments

def part2(puzzle_input):
    increments = 0
    prev_value = puzzle_input[0] + puzzle_input[1] + puzzle_input[2]

    for i in range(len(puzzle_input)-2):
        if puzzle_input[i] + puzzle_input[i+1] + puzzle_input[i+2] > prev_value:
            increments += 1
        prev_value = puzzle_input[i] + puzzle_input[i+1] + puzzle_input[i+2]

    return increments


print("Part 1:", part1(sonar_sweeps))
print("Part 2:", part2(sonar_sweeps))
