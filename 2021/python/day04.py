from common import puzzle_input_by_newlines


bingo_values_and_boards = puzzle_input_by_newlines(4)

def setup_boards(board_input):
    boards = []

    for board in board_input:
        board_rows = []
        for row in board:
            board_rows.append([int(value) for value in row.split()])
        for i in range(len(board_rows)):
            new_row = []
            for j in range(len(board)):
                new_row.append(board_rows[j][i])
            board_rows.append(new_row)
        boards.append(board_rows)
    return boards


def part1(puzzle_input):
    bingo_values = [int(value) for value in puzzle_input[0][0].split(',')]

    boards = setup_boards(puzzle_input[1:])

    for value in bingo_values:
        winner = False
        remaining = set()
        for board in boards:
            for row in board:
                if value in row:
                    row.remove(value)
                if not row:
                    winner = True
            if winner:
                for row in board:
                    remaining.update(row)
                return sum(remaining) * value

    return 0

def part2(puzzle_input):
    bingo_values = [int(value) for value in puzzle_input[0][0].split(',')]

    boards = setup_boards(puzzle_input[1:])
    winners = []

    for value in bingo_values:
        winner = None
        remaining = set()
        for board in boards:
            for row in board:
                if value in row:
                    row.remove(value)
                if not row:
                    winners.append(board)
            if winners:
                if len(boards) == 1:
                    for row in board:
                        remaining.update(row)
                    return sum(remaining) * value
        while winners:
            winner = winners.pop()
            if winner in boards:
                boards.remove(winner)

    return 0


print("Part 1:", part1(bingo_values_and_boards))
print("Part 2:", part2(bingo_values_and_boards))
