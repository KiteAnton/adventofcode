from common import puzzle_input_as_str

reboot_sequence = puzzle_input_as_str(22)


def setup_reboot_sequences(puzzle_input):
    reboot_sequences = []

    for sequence in puzzle_input:
        command, ranges_raw = sequence.split()
        new_sequence = {"command": command}
        ranges_raw = ranges_raw.split(",")
        for range_input in ranges_raw:
            axis, min_max = range_input.split("=")
            range_min, range_max = min_max.split("..")
            new_sequence[axis] = [int(range_min), int(range_max)]
        reboot_sequences.append(new_sequence)

    return reboot_sequences


def set_states(reactor_cubes, sequence, init=True):
    for x in range(sequence["x"][0], sequence["x"][1] + 1):
        if init and x < -50 or x > 50:
            break
        for y in range(sequence["y"][0], sequence["y"][1] + 1):
            if init and y < -50 or y > 50:
                break
            for z in range(sequence["z"][0], sequence["z"][1] + 1):
                if init and z < -50 or z > 50:
                    break
                if sequence["command"] == "on":
                    reactor_cubes.add((x, y, z))
                else:
                    reactor_cubes.discard((x, y, z))


def is_init(sequence):
    range_values = sequence["x"].copy()
    range_values += sequence["y"]
    range_values += sequence["z"]
    for value in range_values:
        if not -50 <= value <= 50:
            return False
    return True


def find_intersection(sequence, core):
    x = [max(sequence["x"][0], core["x"][0]), min(sequence["x"][1], core["x"][1])]
    y = [max(sequence["y"][0], core["y"][0]), min(sequence["y"][1], core["y"][1])]
    z = [max(sequence["z"][0], core["z"][0]), min(sequence["z"][1], core["z"][1])]

    if x[0] > x[1] or y[0] > y[1] or z[0] > z[1]:
        return None
    if core["command"] == "on":
        command = "off"
    else:
        command = "on"

    return {"command": command, "x": x, "y": y, "z": z}


def reboot_reactor(reboot_sequences, init=True):
    reactor_cores = []
    for sequence in reboot_sequences:
        new_cores = []
        if init and not is_init(sequence):
            continue
        if sequence["command"] == "on":
            new_cores.append(sequence)

        for core in reactor_cores:
            intersection = find_intersection(sequence, core)
            if intersection:
                new_cores.append(intersection)

        reactor_cores += new_cores

    # count volumes
    on_cubes = 0
    for core in reactor_cores:
        volume = (
            (core["x"][1] - core["x"][0] + 1)
            * (core["y"][1] - core["y"][0] + 1)
            * (core["z"][1] - core["z"][0] + 1)
        )
        if core["command"] == "on":
            on_cubes += volume
        else:
            on_cubes -= volume

    return on_cubes


def part1(puzzle_input):
    reboot_sequences = setup_reboot_sequences(puzzle_input)
    return reboot_reactor(reboot_sequences)


def part2(puzzle_input):
    reboot_sequences = setup_reboot_sequences(puzzle_input)
    return reboot_reactor(reboot_sequences, init=False)


if __name__ == "__main__":
    print("Part1: ", part1(reboot_sequence))
    print("Part2: ", part2(reboot_sequence))
