import math
from common import puzzle_input_as_str

target_area = puzzle_input_as_str(17)

def setup_target_area(target_raw):
    *_, x_part, y_part = target_raw.split()
    target = []
    target.append([int(y) for y in y_part[2:].split('..')])
    target.append([int(x) for x in x_part[2:-1].split('..')])
    return target

def all_solutions(target_area):
    x_min = target_area[1][0]
    x_max = target_area[1][1]
    y_min = target_area[0][0]
    y_max = target_area[0][1]

    # Find all x_v_0 options.
    # Max x_v_0
    x_v0_max = x_max
    # X_v_0 min
    x_v0_min = math.ceil(-0.5 + math.sqrt(1+8*x_min)/2)
    x_options = {}
    for x in range(x_v0_min, x_v0_max+1):
        x_v = x
        x_pos = 0
        iterations = 0
        while x_v > 0:
            x_pos += x_v
            x_v -= 1
            iterations += 1
            if x_min <= x_pos <= x_max:
                if iterations not in x_options:
                    x_options[iterations] = []
                x_options[iterations].append(x)
        if x_min <= x_pos <= x_max:
            if 'inf' not in x_options:
                x_options['inf'] = []
            x_options['inf'].append(x)

    y_v0_min = y_min
    y_v0_max = abs(y_min) - 1

    # Find Y options,
    y_options = {}
    for y in range(y_v0_min, y_v0_max + 1):
        y_v = y
        y_pos = 0
        iterations = 0
        while y_pos >= y_min:
            y_pos += y_v
            y_v -= 1
            iterations += 1
            if y_min <= y_pos <= y_max:
                if iterations not in y_options:
                    y_options[iterations] = []
                y_options[iterations].append(y)


    solutions = []
    for iterations, y_v in y_options.items():
        for y in y_v:
            if iterations in x_options:
                for x in x_options[iterations]:
                    if [x, y] not in solutions:
                        solutions.append([x, y])
            for x_val in [x_val for x_val in x_options['inf'] if x_val <= iterations]:
                if [x_val, y] not in solutions:
                    solutions.append([x_val, y])

    return solutions


def part1(puzzle_input):
    target_area = setup_target_area(puzzle_input[0])
    target_y_min = min(target_area[0])
    yv_0 = abs(target_y_min) - 1
    y_max = yv_0 * (yv_0+1) / 2
    return y_max

def part2(puzzle_input):
    target_area = setup_target_area(puzzle_input[0])
    return len(all_solutions(target_area))

print("Part 1:", part1(target_area))
print("Part 2:", part2(target_area))
