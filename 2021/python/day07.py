from common import puzzle_input_as_str

crab_positions = puzzle_input_as_str(7)

step_vs_cost = {0: 0, 1: 1}
def move_cost(step):

    if step not in step_vs_cost:
        for i in range(step+1):
            if i not in step_vs_cost:
                step_vs_cost[i] = step_vs_cost[i-1] + i

    return step_vs_cost[step]


def move_all_to_position_advanced(positions, target_position):
    total_cost = 0
    for pos in positions:
        total_cost += move_cost(abs(pos - target_position))

    return total_cost


def move_all_to_position(positions, target_position, moving_cost=1):
    total_cost = 0
    for pos in positions:
        total_cost += abs(pos - target_position) * moving_cost

    return total_cost


def setup_crab_positions(positions_raw):
    positions = list(map(int, positions_raw.split(',')))
    return positions

def part1(puzzle_input):
    positions = setup_crab_positions(puzzle_input[0])

    min_pos = min(positions)
    max_pos = max(positions)

    lowest_cost = None
    for i in range(min_pos, max_pos+1):
        cost = move_all_to_position(positions, target_position=i)
        if not lowest_cost or lowest_cost > cost:
            lowest_cost = cost
    return lowest_cost


def part2(puzzle_input):
    positions = setup_crab_positions(puzzle_input[0])

    min_pos = min(positions)
    max_pos = max(positions)

    lowest_cost = None
    for i in range(min_pos, max_pos+1):
        cost = move_all_to_position_advanced(positions, target_position=i)
        if not lowest_cost or lowest_cost > cost:
            lowest_cost = cost
    return lowest_cost


print("Part 1:", part1(crab_positions))
print("Part 2:", part2(crab_positions))
