from common import puzzle_input_as_str

bits_transmission = puzzle_input_as_str(16)

def parse_literal_value(bin_string):
    bin_index = 0
    value_bits = ''
    five_bits = ''
    while not value_bits or five_bits[0] == '1' and bin_index <= len(bin_string) - 5:
        five_bits = bin_string[bin_index:bin_index+5]
        value_bits += five_bits[1:]
        bin_index += 5

    literal_value = int(value_bits, 2)
    return literal_value, bin_string[bin_index:]

def count_version_sums(result):
    version_sum = result.get('version', 0)

    if 'sub_packets' in result:
        for sub_p in result['sub_packets']:
            version_sum += count_version_sums(sub_p)

    return version_sum

def find_sub_packets(bin_string):
    packages = []
    bin_index = 0
    length_type_id = bin_string[0]
    if length_type_id == '0':
        size_bits = 16
    else:
        size_bits = 12

    bin_index += size_bits
    package_length = int(bin_string[1:size_bits], 2)

    if length_type_id == '0':
        remainder = bin_string[bin_index:bin_index+package_length]
        while remainder:
            result, remainder = parse_package(remainder, is_bin=True)
            packages.append(result)

        bin_index += package_length
        return packages, bin_string[bin_index:]

    remainder = bin_string[bin_index:]
    for _ in range(package_length):
        package_details, remainder = parse_package(remainder, is_bin=True)
        packages.append(package_details)

    return packages, remainder

def parse_operator_package(type_id, binary_string):
    sub_packets, remainder = find_sub_packets(binary_string)
    sub_packets_values = [sub_p['value'] for sub_p in sub_packets]

    packet_value = 0
    if type_id == 0:  # Sum packets
        packet_value = sum(sub_packets_values)
    elif type_id == 1:  # Product packets
        packet_value = 1
        for p_v in sub_packets_values:
            packet_value *= p_v
    elif type_id == 2:  # Minimum packets
        packet_value = min(sub_packets_values)
    elif type_id == 3:  # Maximum packets
        packet_value = max(sub_packets_values)
    elif type_id == 5:  # Greater than packets
        packet_value = int(sub_packets_values[0] > sub_packets_values[1])
    elif type_id == 6:  # Less than packets
        packet_value = int(sub_packets_values[0] < sub_packets_values[1])
    elif type_id == 7:  # Equal to packets
        packet_value = int(sub_packets_values[0] == sub_packets_values[1])
    return packet_value, sub_packets, remainder

def parse_package(input_string, is_bin=False):
    if not is_bin:
        bin_string = bin(int(input_string, 16))[2:].zfill(len(input_string) * 4)
    else:
        bin_string = input_string

    version = int(bin_string[0:3], 2)
    type_id = int(bin_string[3:6], 2)

    return_value = { 'version': version, 'type_id': type_id }

    remainder = ''
    if type_id == 4:  # Literal packets
        packet_value, remainder = parse_literal_value(bin_string[6:])
    else:
        packet_value, sub_packets, remainder = parse_operator_package(type_id, bin_string[6:])
        return_value['sub_packets'] = sub_packets

    return_value['value'] = packet_value
    return return_value, remainder


def part1(puzzle_input):
    result, _ = parse_package(puzzle_input[0])
    return count_version_sums(result)

def part2(puzzle_input):
    result, _ = parse_package(puzzle_input[0])
    return result['value']

print("Part 1:", part1(bits_transmission))
print("Part 2:", part2(bits_transmission))
# import cProfile
# cProfile.run('print("Part 1:", part1(bits_transmission))')
# cProfile.run('print("Part 2:", part2(bits_transmission))')
