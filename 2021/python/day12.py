from common import puzzle_input_as_str

input_map = puzzle_input_as_str(12)

def setup_cave_map(map_input):
    cave_map = {}
    for line in map_input:
        start, end = line.split('-')
        if start not in cave_map:
            cave_map[start] = []
        if end not in cave_map:
            cave_map[end] = []
        cave_map[start].append(end)
        cave_map[end].append(start)

    return cave_map

def find_all_paths(cave_map):
    new_paths = find_path('start', ['start'], cave_map)
    return new_paths

def find_all_paths_advanced(cave_map):
    new_paths = find_path_advanced('start', ['start'], cave_map)
    return new_paths

def find_path(start_point, path, cave_map):
    new_paths = []
    try:
        path_options = cave_map[start_point]
        for option in path_options:
            if option.isupper() or option not in path:
                current_path = path.copy()
                current_path.append(option)
                if option == 'end':
                    new_paths.append(current_path)
                    continue
                paths = find_path(option, current_path, cave_map)
                for p in paths:
                    new_paths.append(p)

    except KeyError:
        pass

    return new_paths

def find_path_advanced(start_point, path, cave_map):
    new_paths = []
    try:
        path_options = cave_map[start_point]
        for option in path_options:
            if option.isupper() or option not in path \
               or (option.islower() and option not in ['start', 'end'] and 'use_twice' not in path):
                current_path = path.copy()
                if (option.islower() and option in path):
                    current_path.insert(0, 'use_twice')

                current_path.append(option)
                if option == 'end':
                    new_paths.append(current_path)
                    continue
                paths = find_path_advanced(option, current_path, cave_map)
                for p in paths:
                    new_paths.append(p)


    except KeyError:
        pass

    return new_paths

def part1(puzzle_input):
    cave_map = setup_cave_map(puzzle_input)
    paths = find_all_paths(cave_map)

    return len(paths)

def part2(puzzle_input):
    cave_map = setup_cave_map(puzzle_input)
    paths = find_all_paths_advanced(cave_map)

    return len(paths)

print("Part 1", part1(input_map))
print("Part 2", part2(input_map))
