from common import puzzle_input_as_str


ocean_height_map = puzzle_input_as_str(9)

def find_local_minimums(height_map):
    minimum_positions = []
    minimum_values = []

    for i, line in enumerate(height_map):
        for j, value in enumerate(line):
            value = int(value)
            if i == 0:
                up = 9
            else:
                up = int(height_map[i-1][j])
            if i < len(height_map) - 1:
                down = int(height_map[i+1][j])
            else:
                down = 9
            if j == 0:
                left = 9
            else:
                left = int(line[j-1])
            if j < len(line) - 1:
                right = int(line[j+1])
            else:
                right = 9

            if value < left and \
               value < right and \
               value < up and \
               value < down:
                minimum_positions.append((i,j))
                minimum_values.append(value)

    return minimum_positions, minimum_values


def find_positions_until_high(position, height_map, basin_positions):
    pos_row, pos_col = position
    if position in basin_positions or \
       not (0 <= pos_row < len(height_map)) or \
       not (0 <= pos_col < len(height_map[pos_row])) or \
       int(height_map[pos_row][pos_col]) == 9:
        return

    basin_positions.add(position)

    find_positions_until_high((pos_row, pos_col-1), height_map, basin_positions)
    find_positions_until_high((pos_row, pos_col+1), height_map, basin_positions)
    find_positions_until_high((pos_row-1, pos_col), height_map, basin_positions)
    find_positions_until_high((pos_row+1, pos_col), height_map, basin_positions)


def find_basin_sizes(positions, height_map):
    basins_sizes = []

    for pos in positions:
        basin_positions = set()
        find_positions_until_high(pos, height_map, basin_positions)
        basins_sizes.append(len(basin_positions))

    return basins_sizes


def part1(puzzle_input):
    _, minimum_values = find_local_minimums(puzzle_input)

    return sum(minimum_values) + len(minimum_values)


def part2(puzzle_input):
    product_3_largest_basins = 1

    minimum_positions, _ = find_local_minimums(puzzle_input)

    basins = find_basin_sizes(minimum_positions, puzzle_input)
    basins.sort(reverse=True)

    for value in basins[0:3]:
        product_3_largest_basins *= value

    return product_3_largest_basins


print("Part 1:", part1(ocean_height_map))
print("Part 2:", part2(ocean_height_map))
