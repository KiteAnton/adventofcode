from common import puzzle_input_as_str

hydrothermal_vents = puzzle_input_as_str(5)

def map_vents_to_ocean_floor(vents, keep_diagonal=False):
    ocean_floor = {}
    for line in vents:
        start_pos, end_pos = line.split(' -> ')
        start_x, start_y = list(map(int, start_pos.split(',')))
        end_x, end_y = list(map(int, end_pos.split(',')))
        x = start_x
        y = start_y
        if start_x == end_x or start_y == end_y or keep_diagonal:
            move_x = max(min(end_x - start_x, 1), -1)
            move_y = max(min(end_y - start_y, 1), -1)
            while x != end_x or y != end_y:
                if (x,y) not in ocean_floor:
                    ocean_floor[(x,y)] = 0
                ocean_floor[(x,y)] += 1
                x += move_x
                y += move_y
            if (end_x,end_y) not in ocean_floor:
                ocean_floor[(end_x,end_y)] = 0
            ocean_floor[(end_x,end_y)] += 1
    return ocean_floor

def count_map(ocean_floor, threshold=2):
    result = 0
    for value in ocean_floor.values():
        if value >= threshold:
            result += 1
    return result


def part1(puzzle_input):
    ocean_floor = map_vents_to_ocean_floor(puzzle_input, keep_diagonal=False)
    return count_map(ocean_floor)


def part2(puzzle_input):
    ocean_floor = map_vents_to_ocean_floor(puzzle_input, keep_diagonal=True)
    return count_map(ocean_floor)


print("Part 1:", part1(hydrothermal_vents))
print("Part 2:", part2(hydrothermal_vents))
