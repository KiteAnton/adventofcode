from copy import deepcopy
from common import puzzle_input_by_newlines

puzzle_input = puzzle_input_by_newlines(20)


def setup_image_enhancer_config(img_enh_raw):
    image_enhancer_lookup = set()

    for i, char in enumerate(img_enh_raw[0]):
        if char == '#':
            image_enhancer_lookup.add(i)

    return image_enhancer_lookup

def setup_image(img_raw):
    image_pixels = set()

    for i, row in enumerate(img_raw):
        for j, col in enumerate(row):
            if col == '#':
                image_pixels.add((i, j))

    return image_pixels

def img_string_to_number(img_string):
    number = 0

    for i, char in enumerate(list(reversed(img_string))):
        if char == '#':
            number += pow(2, i)

    return number


def str_from_surrounding_pixels(image, position):
    x, y = position
    img_string = ''

    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if (i, j) in image:
                img_string += '#'
            else:
                img_string += '.'

    return img_string

def new_pixel_value(image, position, image_enhancer):
    img_string = str_from_surrounding_pixels(image, position)
    pos_lookup = img_string_to_number(img_string)
    if pos_lookup in image_enhancer:
        return True
    return False

def find_min_max_from_image(image_pixels):
    min_x = 0
    min_y = 0
    max_x = 0
    max_y = 0
    for pixel in image_pixels:
        x, y = pixel
        min_x = min(min_x, x)
        min_y = min(min_y, y)
        max_x = max(max_x, x)
        max_y = max(max_y, y)

    return min_x, max_x, min_y, max_y


def enhance_image(image_pixels, image_enhancer, iteration=0):
    min_x, max_x, min_y, max_y = find_min_max_from_image(image_pixels)

    # Remove one layer of added 'infinite padding'
    if 0 in image_enhancer and iteration > 0 and iteration % 2:
        min_x += 2
        min_y += 2
        max_x -= 2
        max_y -= 2


    old_copy = deepcopy(image_pixels)
    for i in range(min_x - 1, max_x + 2):
        for j in range(min_y - 1, max_y + 2):
            pixel = (i, j)
            if new_pixel_value(old_copy, pixel, image_enhancer):
                image_pixels.add(pixel)
            else:
                image_pixels.discard(pixel)

    # Cut away outer 'infinite layer':
    if 0 in image_enhancer and iteration > 0 and iteration % 2:
        for x in range(min_x - 2, max_x + 3):
            for y in range(min_y - 2, max_y + 3):
                if x < min_x - 1 or x > max_x + 1:
                    image_pixels.discard((x, y))
                elif y < min_y - 1 or y > max_y + 1:
                    image_pixels.discard((x, y))

    # Pad with 'infinite' #
    if 0 in image_enhancer and not iteration % 2:
        for x in range(min_x - 3, max_x + 4):
            for y in range(min_y - 3, max_x + 4):
                if x < min_x - 1 or x > max_x + 1:
                    image_pixels.add((x, y))
                elif y < min_y - 1 or y > max_y + 1:
                    image_pixels.add((x, y))

def part1(puzzle_input):
    image_pixels = setup_image(puzzle_input[1])
    im_enhancer = setup_image_enhancer_config(puzzle_input[0])

    for i in range(2):
        enhance_image(image_pixels, im_enhancer, iteration=i)

    return len(image_pixels)

def part2(puzzle_input):
    image_pixels = setup_image(puzzle_input[1])
    im_enhancer = setup_image_enhancer_config(puzzle_input[0])

    for i in range(50):
        enhance_image(image_pixels, im_enhancer, iteration=i)

    return len(image_pixels)

if __name__ == "__main__":
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))
