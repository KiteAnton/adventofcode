from common import puzzle_input_as_str
from day06 import part1, part2, simulate_day_growth, setup_fish_pattern

lanternfishes = puzzle_input_as_str(6)
test_lanternfishes = puzzle_input_as_str(6, 1)


def test_test_part1():
    assert part1(test_lanternfishes) == 5934

def test_final_part1():
    assert part1(lanternfishes) == 362346

def simulate_day(i, fishes, expected_result):
    simulate_day_growth(fishes, i)
    assert fishes['total'] == len(expected_result)
    assert fishes['new'][0] == expected_result.count(7)
    assert fishes['new'][1] == expected_result.count(8)

    for w in range(7):
       day = (i+1+w) % 7
       assert fishes[day] == expected_result.count(w)


def test_day_growth():

    fishes_per_day = setup_fish_pattern(test_lanternfishes[0])

    # After  1 day:  2,3,2,0,1
    i = 0
    simulate_day(i, fishes_per_day, [2,3,2,0,1])

    # After  2 days: 1,2,1,6,0,8
    i += 1
    simulate_day(i, fishes_per_day, [1,2,1,6,0,8])

    # After  3 days: 0,1,0,5,6,7,8
    i += 1
    simulate_day(i, fishes_per_day, [0,1,0,5,6,7,8])

    # After  4 days: 6,0,6,4,5,6,7,8,8
    i += 1
    simulate_day(i, fishes_per_day, [6,0,6,4,5,6,7,8,8])

    # After  5 days: 5,6,5,3,4,5,6,7,7,8
    i += 1
    simulate_day(i, fishes_per_day, [5,6,5,3,4,5,6,7,7,8])

    # After  6 days: 4,5,4,2,3,4,5,6,6,7
    i += 1
    simulate_day(i, fishes_per_day, [4,5,4,2,3,4,5,6,6,7])

    # After  7 days: 3,4,3,1,2,3,4,5,5,6
    i += 1
    simulate_day(i, fishes_per_day, [3,4,3,1,2,3,4,5,5,6])

    # After  8 days: 2,3,2,0,1,2,3,4,4,5
    i += 1
    simulate_day(i, fishes_per_day, [2,3,2,0,1,2,3,4,4,5])

    # After  9 days: 1,2,1,6,0,1,2,3,3,4,8
    i += 1
    simulate_day(i, fishes_per_day, [1,2,1,6,0,1,2,3,3,4,8])

    # After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
    i += 1
    simulate_day(i, fishes_per_day, [0,1,0,5,6,0,1,2,2,3,7,8])

    # After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
    i += 1
    simulate_day(i, fishes_per_day, [6,0,6,4,5,6,0,1,1,2,6,7,8,8,8])

    # After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
    i += 1
    simulate_day(i, fishes_per_day, [5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8])

    # After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
    i += 1
    simulate_day(i, fishes_per_day, [4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8])

    # After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
    i += 1
    simulate_day(i, fishes_per_day, [3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8])

    # After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
    i += 1
    simulate_day(i, fishes_per_day, [2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7])

    # After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
    i += 1
    simulate_day(i, fishes_per_day, [1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8])

    # After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
    i += 1
    simulate_day(i, fishes_per_day, [0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8])

    # After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8
    i += 1
    simulate_day(i, fishes_per_day, [6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8])

def test_test_part2():
    assert part2(test_lanternfishes) == 26984457539

def test_final_part2():
    assert part2(lanternfishes) == 1639643057051
