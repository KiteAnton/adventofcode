from common import puzzle_input_as_str
from day12 import part1, part2, setup_cave_map, find_all_paths, find_path
from day12 import find_all_paths_advanced

input_map = puzzle_input_as_str(12)
test_small_cave_map = puzzle_input_as_str(12, 1)
test_medium_cave_map = puzzle_input_as_str(12, 2)
test_large_cave_map = puzzle_input_as_str(12, 3)


def test_start_path():
    cave_map = setup_cave_map(test_small_cave_map)
    paths = find_path('start', ['start'], cave_map)

    assert len(paths) == 10

def test_find_paths_small_map():
    cave_map = setup_cave_map(test_small_cave_map)
    paths = find_all_paths(cave_map)

    assert ["start","A","b","A","c","A","end"] in paths
    assert ["start","A","b","A","end"] in paths
    assert ["start","A","b","end"] in paths
    assert ["start","A","c","A","b","A","end"] in paths
    assert ["start","A","c","A","b","end"] in paths
    assert ["start","A","c","A","end"] in paths
    assert ["start","A","end"] in paths
    assert ["start","b","A","c","A","end"] in paths
    assert ["start","b","A","end"] in paths
    assert ["start","b","end"] in paths

    assert len(paths) == 10

def test_find_paths_medium_map():
    cave_map = setup_cave_map(test_medium_cave_map)
    paths = find_all_paths(cave_map)

    assert ["start","HN","dc","HN","end"] in paths
    assert ["start","HN","dc","HN","kj","HN","end"] in paths
    assert ["start","HN","dc","end"] in paths
    assert ["start","HN","dc","kj","HN","end"] in paths
    assert ["start","HN","end"] in paths
    assert ["start","HN","kj","HN","dc","HN","end"] in paths
    assert ["start","HN","kj","HN","dc","end"] in paths
    assert ["start","HN","kj","HN","end"] in paths
    assert ["start","HN","kj","dc","HN","end"] in paths
    assert ["start","HN","kj","dc","end"] in paths
    assert ["start","dc","HN","end"] in paths
    assert ["start","dc","HN","kj","HN","end"] in paths
    assert ["start","dc","end"] in paths
    assert ["start","dc","kj","HN","end"] in paths
    assert ["start","kj","HN","dc","HN","end"] in paths
    assert ["start","kj","HN","dc","end"] in paths
    assert ["start","kj","HN","end"] in paths
    assert ["start","kj","dc","HN","end"] in paths
    assert ["start","kj","dc","end"] in paths

    assert len(paths) == 19

def test_test_part1():
    assert part1(test_large_cave_map) == 226

def test_final_part1():
    assert part1(input_map) == 4413

def test_find_paths_advanced():
    cave_map = setup_cave_map(test_small_cave_map)
    paths = find_all_paths_advanced(cave_map)

    assert len(paths) == 36

def test_test_part2():
    assert part2(test_large_cave_map) == 3509

def test_final_part2():
    assert part2(input_map) == 118803
