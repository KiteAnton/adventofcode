from common import puzzle_input_as_str
from day10 import part1, part2

navigation_subsystem = puzzle_input_as_str(10)
test_navigation_subsystem = puzzle_input_as_str(10, 1)


def test_test_part1():
    assert part1(test_navigation_subsystem) == 26397

def test_final_part1():
    assert part1(navigation_subsystem) == 436497

def test_test_part2():
    assert part2(test_navigation_subsystem) == 288957

def test_final_part2():
    assert part2(navigation_subsystem) == 2377613374
