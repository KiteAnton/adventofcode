import pytest
from common import puzzle_input_as_str
from day18 import calculate_magnitude, list_structure_from_string_list
from day18 import part1, part2, explode_pairs, reduce_numbers
from day18 import string_to_lists, add_lists, sum_up_from_input

snailfish_input = puzzle_input_as_str(18)
test_snailfish_input = puzzle_input_as_str(18, 1)
test_snailfish_input2 = puzzle_input_as_str(18, 2)

@pytest.mark.parametrize("input_string, expected_result", [
    ["[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"],
    ["[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"],
    ["[[6,[5,[4,[3,2]]]],1]",  "[[6,[5,[7,0]]],3]"],
    ["[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"],
    # ["[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"]
    ["[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]","[[3,[2,[8,0]]],[9,[5,[7,0]]]]"]
])
def test_explode_pairs(input_string, expected_result):
    lists = string_to_lists(input_string)
    explode_pairs(lists)
    expected_list = string_to_lists(expected_result)
    assert lists == expected_list

def test_split_numbers():
    lists = string_to_lists("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]")
    reduce_numbers(lists)

def test_add_to_list():
    first_line = string_to_lists("[1,1]")
    lists = add_lists(first_line, "[2,2]")
    lists = add_lists(lists, "[3,3]")
    lists = add_lists(lists, "[4,4]")
    expected_list = string_to_lists("[[[[1,1],[2,2]],[3,3]],[4,4]]")
    assert lists == expected_list

    lists = add_lists(lists, "[5,5]")
    expected_list = string_to_lists("[[[[3,0],[5,3]],[4,4]],[5,5]]")
    assert lists == expected_list

    lists = add_lists(lists, "[6,6]")
    expected_list = string_to_lists("[[[[5,0],[7,4]],[5,5]],[6,6]]")
    assert lists == expected_list

def test_sum_up_input():
    lists = sum_up_from_input(test_snailfish_input)
    expected_list = \
        string_to_lists("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
    assert lists == expected_list

@pytest.mark.parametrize("input_lists, expected_magnitude", [
    ["[9,1]", 29],
    ["[[9,1],[1,9]]", 129],
    ["[[1,2],[[3,4],5]]", 143],

    ["[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384],
    ["[[[[1,1],[2,2]],[3,3]],[4,4]]", 445],
    ["[[[[3,0],[5,3]],[4,4]],[5,5]]", 791],
    ["[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137],
    ["[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488]
                                                             ])
def test_magnitude(input_lists, expected_magnitude):
    lists = string_to_lists(input_lists)
    real_list = list_structure_from_string_list(lists)

    magnitude = calculate_magnitude(real_list[0])
    assert magnitude == expected_magnitude

def test_test_part1():
    assert part1(test_snailfish_input2) == 4140

def test_final_part1():
    assert part1(snailfish_input) == 4137

def test_test_part2():
    assert part2(test_snailfish_input2) == 3993

def test_final_part2():
    assert part2(snailfish_input) == 4573
