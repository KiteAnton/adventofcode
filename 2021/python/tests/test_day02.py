from common import puzzle_input_as_str
from day02 import part1, part2

submarine_instructions = puzzle_input_as_str(2)
test_submarine_instructions = puzzle_input_as_str(2, 1)


def test_test_part1():
    assert part1(test_submarine_instructions) == 150

def test_final_part1():
    assert part1(submarine_instructions) == 1660158

def test_test_part2():
    assert part2(test_submarine_instructions) == 900

def test_final_part2():
    assert part2(submarine_instructions) == 1604592846
