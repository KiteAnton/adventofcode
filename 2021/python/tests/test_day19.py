from common import puzzle_input_by_newlines
from day19 import find_scanner_transformation, part1, part2, transform_scanner_result
from day19 import calculate_beacon_distances, find_matching_beacons
from day19 import setup_scanner_beacons, all_beacons

scanner_beacons = puzzle_input_by_newlines(19)
test_scanner_beacons = puzzle_input_by_newlines(19, 2)


def test_setup_scanner_beacons():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)

    assert len(scanner_beacons) == 5
    assert len(scanner_beacons[0]) == 25
    assert (-838, 591, 734) in scanner_beacons[0]

def test_calculate_beacon_distances():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)
    distances = calculate_beacon_distances(scanner_beacons[0])
    assert 1 + 81*81 + 163*163 in distances

def test_find_matching_beacons():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)
    matching_scanner0, matching_scanner1 = \
        find_matching_beacons(scanner_beacons[0], scanner_beacons[1])

    assert len(matching_scanner0) == 12
    assert (-618,-824,-621) in matching_scanner0
    assert (-537,-823,-458) in matching_scanner0
    assert (-447,-329,318) in matching_scanner0
    assert (404,-588,-901) in matching_scanner0
    assert (544,-627,-890) in matching_scanner0
    assert (528,-643,409) in matching_scanner0
    assert (-661,-816,-575) in matching_scanner0
    assert (390,-675,-793) in matching_scanner0
    assert (423,-701,434) in matching_scanner0
    assert (-345,-311,381) in matching_scanner0
    assert (459,-707,401) in matching_scanner0
    assert (-485,-357,347) in matching_scanner0

    assert len(matching_scanner1) == 12
    assert (686,422,578) in matching_scanner1
    assert (605,423,415) in matching_scanner1
    assert (515,917,-361) in matching_scanner1
    assert (-336,658,858) in matching_scanner1
    assert (-476,619,847) in matching_scanner1
    assert (-460,603,-452) in matching_scanner1
    assert (729,430,532) in matching_scanner1
    assert (-322,571,750) in matching_scanner1
    assert (-355,545,-477) in matching_scanner1
    assert (413,935,-424) in matching_scanner1
    assert (-391,539,-444) in matching_scanner1
    assert (553,889,-390) in matching_scanner1


def test_find_transformation():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)

    offset, sign_flip, shift_position = \
        find_scanner_transformation(scanner_beacons[0], scanner_beacons[1])

    assert offset == [68, -1246, -43]
    assert sign_flip == [True, False, True]
    assert shift_position == [0, 0, 0]

    offset, sign_flip, shift_position = \
        find_scanner_transformation(scanner_beacons[1], scanner_beacons[4])

    # Positions given relative to Sc0, so need to recalculate based
    # on Sc0 to Sc1 offset from above, previous sign_flip needs to be considered.
    result = [68 + ( - offset[0]), -1246 + offset[1], -43 + (-offset[2])]
    assert result == [-20, -1133, 1061]

def test_transform_scanner_result():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)

    offset, sign_flip, shift_position = \
        find_scanner_transformation(scanner_beacons[0], scanner_beacons[1])

    transform_scanner_result(scanner_beacons[1],
                             offset, sign_flip, shift_position)

    assert (-618,-824,-621) in scanner_beacons[1]
    assert (-537,-823,-458) in scanner_beacons[1]
    assert (-447,-329,318) in scanner_beacons[1]
    assert (404,-588,-901) in scanner_beacons[1]
    assert (544,-627,-890) in scanner_beacons[1]
    assert (528,-643,409) in scanner_beacons[1]
    assert (-661,-816,-575) in scanner_beacons[1]
    assert (390,-675,-793) in scanner_beacons[1]
    assert (423,-701,434) in scanner_beacons[1]
    assert (-345,-311,381) in scanner_beacons[1]
    assert (459,-707,401) in scanner_beacons[1]
    assert (-485,-357,347) in scanner_beacons[1]

    offset, sign_flip, shift_position = \
        find_scanner_transformation(scanner_beacons[1], scanner_beacons[4])

    assert offset == [-20, -1133, 1061]

    offset, sign_flip, shift_position = \
        find_scanner_transformation(scanner_beacons[0], scanner_beacons[3])

    assert offset == None  # No matching beacons between 0 and 3

def test_all_results_part1():
    scanner_beacons = setup_scanner_beacons(test_scanner_beacons)
    all_found_beacons, _ = all_beacons(scanner_beacons)

    assert (-892,524,684) in all_found_beacons
    assert (-876,649,763) in all_found_beacons
    assert (-838,591,734) in all_found_beacons
    assert (-789,900,-551) in all_found_beacons
    assert (-739,-1745,668) in all_found_beacons
    assert (-706,-3180,-659) in all_found_beacons
    assert (-697,-3072,-689) in all_found_beacons
    assert (-689,845,-530) in all_found_beacons
    assert (-687,-1600,576) in all_found_beacons
    assert (-661,-816,-575) in all_found_beacons
    assert (-654,-3158,-753) in all_found_beacons
    assert (-635,-1737,486) in all_found_beacons
    assert (-631,-672,1502) in all_found_beacons
    assert (-624,-1620,1868) in all_found_beacons
    assert (-620,-3212,371) in all_found_beacons
    assert (-618,-824,-621) in all_found_beacons
    assert (-612,-1695,1788) in all_found_beacons
    assert (-601,-1648,-643) in all_found_beacons
    assert (-584,868,-557) in all_found_beacons
    assert (-537,-823,-458) in all_found_beacons
    assert (-532,-1715,1894) in all_found_beacons
    assert (-518,-1681,-600) in all_found_beacons
    assert (-499,-1607,-770) in all_found_beacons
    assert (-485,-357,347) in all_found_beacons
    assert (-470,-3283,303) in all_found_beacons
    assert (-456,-621,1527) in all_found_beacons
    assert (-447,-329,318) in all_found_beacons
    assert (-430,-3130,366) in all_found_beacons
    assert (-413,-627,1469) in all_found_beacons
    assert (-345,-311,381) in all_found_beacons
    assert (-36,-1284,1171) in all_found_beacons
    assert (-27,-1108,-65) in all_found_beacons
    assert (7,-33,-71) in all_found_beacons
    assert (12,-2351,-103) in all_found_beacons
    assert (26,-1119,1091) in all_found_beacons
    assert (346,-2985,342) in all_found_beacons
    assert (366,-3059,397) in all_found_beacons
    assert (377,-2827,367) in all_found_beacons
    assert (390,-675,-793) in all_found_beacons
    assert (396,-1931,-563) in all_found_beacons
    assert (404,-588,-901) in all_found_beacons
    assert (408,-1815,803) in all_found_beacons
    assert (423,-701,434) in all_found_beacons
    assert (432,-2009,850) in all_found_beacons
    assert (443,580,662) in all_found_beacons
    assert (455,729,728) in all_found_beacons
    assert (456,-540,1869) in all_found_beacons
    assert (459,-707,401) in all_found_beacons
    assert (465,-695,1988) in all_found_beacons
    assert (474,580,667) in all_found_beacons
    assert (496,-1584,1900) in all_found_beacons
    assert (497,-1838,-617) in all_found_beacons
    assert (527,-524,1933) in all_found_beacons
    assert (528,-643,409) in all_found_beacons
    assert (534,-1912,768) in all_found_beacons
    assert (544,-627,-890) in all_found_beacons
    assert (553,345,-567) in all_found_beacons
    assert (564,392,-477) in all_found_beacons
    assert (568,-2007,-577) in all_found_beacons
    assert (605,-1665,1952) in all_found_beacons
    assert (612,-1593,1893) in all_found_beacons
    assert (630,319,-379) in all_found_beacons
    assert (686,-3108,-505) in all_found_beacons
    assert (776,-3184,-501) in all_found_beacons
    assert (846,-3110,-434) in all_found_beacons
    assert (1135,-1161,1235) in all_found_beacons
    assert (1243,-1093,1063) in all_found_beacons
    assert (1660,-552,429) in all_found_beacons
    assert (1693,-557,386) in all_found_beacons
    assert (1735,-437,1738) in all_found_beacons
    assert (1749,-1800,1813) in all_found_beacons
    assert (1772,-405,1572) in all_found_beacons
    assert (1776,-675,371) in all_found_beacons
    assert (1779,-442,1789) in all_found_beacons
    assert (1780,-1548,337) in all_found_beacons
    assert (1786,-1538,337) in all_found_beacons
    assert (1847,-1591,415) in all_found_beacons
    assert (1889,-1729,1762) in all_found_beacons
    assert (1994,-1805,1792) in all_found_beacons

def test_test_part1():
    assert part1(test_scanner_beacons) == 79

def test_final_part1():
    result = part1(scanner_beacons)
    assert result == 445

def test_test_part2():
    assert part2(test_scanner_beacons) == 3621

def test_final_part2():
    assert part2(scanner_beacons) == 13225
