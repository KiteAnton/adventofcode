from common import puzzle_input_as_str
from day11 import part1, part2, sim_iteration, setup_map

octopus_map = puzzle_input_as_str(11)
test_octopus_map = puzzle_input_as_str(11, 1)
test_small_octopus_map = puzzle_input_as_str(11, 2)

def test_small_iterations():
    small_map = setup_map(test_small_octopus_map)
    sim_iteration(small_map)
    assert small_map == [[3, 4, 5, 4, 3],
                         [4, 0, 0, 0, 4],
                         [5, 0, 0, 0, 5],
                         [4, 0, 0, 0, 4],
                         [3, 4, 5, 4, 3]]
    sim_iteration(small_map)
    assert small_map == [[4, 5, 6, 5, 4],
                         [5, 1, 1, 1, 5],
                         [6, 1, 1, 1, 6],
                         [5, 1, 1, 1, 5],
                         [4, 5, 6, 5, 4]]

def test_larger_example():
    larger_map = setup_map(test_octopus_map)
    total_flashes = 0
    for _ in range(10):
        total_flashes += sim_iteration(larger_map)
    assert total_flashes == 204

def test_test_part1():
    assert part1(test_octopus_map) == 1656

def test_final_part1():
    assert part1(octopus_map) == 1644

def test_test_part2():
    assert part2(test_octopus_map) == 195

def test_final_part2():
    assert part2(octopus_map) == 229
