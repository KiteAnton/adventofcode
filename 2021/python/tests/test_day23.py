import pytest
from common import puzzle_input_as_str
from day23 import (
    calculate_unoptimizable_cost,
    heuristic,
    part1,
    part2,
    path_free,
    possible_moves,
    room_available,
    setup_map,
)

amphipod_start = puzzle_input_as_str(23)
test_amphipod_start = puzzle_input_as_str(23, 1)


def test_setup_map():
    hallway, rooms = setup_map(test_amphipod_start)

    assert hallway == [""] * 11
    assert len(rooms) == 4
    assert rooms[0] == ["B"]  # A already in place, no nead to move
    assert rooms[1] == ["C", "D"]
    assert rooms[2] == ["B"]  # C already in place, no need to move
    assert rooms[3] == ["D", "A"]


def test_unoptimizable_cost():
    _, rooms = setup_map(test_amphipod_start)
    unoptimizable_cost = calculate_unoptimizable_cost(rooms)

    assert unoptimizable_cost == 6253


@pytest.mark.parametrize(
    "rooms, room_type, is_available",
    [
        [[["C"], ["C", "D"], ["A"], []], "A", False],
        [[["C"], ["C", "D"], ["A"], []], "D", True],
        [[[], ["C", "D"], ["A"], []], "A", True],
        [[[], ["C", "D"], ["A"], []], "C", False],
    ],
)
def test_room_available(rooms, room_type, is_available):
    assert is_available == room_available(rooms, room_type)


@pytest.mark.parametrize(
    "hallway, start, end, is_free",
    [
        [[""] * 11, 1, 3, True],
        [["", "A", "", "B", "", "", ""], 0, 3, False],
        [["", "A", "", "B", "", "", ""], 1, 2, False],
        [["", "A", "", "B", "", "", ""], 2, 0, False],
        [["", "A", "", "B", "", "", ""], 4, 6, True],
        [["", "A", "", "B", "", "", ""], 6, 4, True],
    ],
)
def test_path_free(hallway, start, end, is_free):
    assert is_free == path_free(hallway, start, end)


def test_find_path_to_destination():
    hallway = [""] * 11
    hallway[7] = "A"
    rooms = [[], [], [], []]
    # new_hallway, new_rooms, cost = possible_moves(hallway, rooms)
    new_hallway, new_rooms, cost = next(possible_moves(hallway, rooms))
    assert new_hallway == [""] * 11
    assert new_rooms == rooms
    assert cost == 5

    hallway = [""] * 11
    hallway[3] = "A"
    rooms = [["B"], [], [], []]
    new_hallway, new_rooms, cost = next(possible_moves(hallway, rooms))
    assert new_hallway == ["B", "", "", "A", "", "", "", "", "", "", ""]
    assert new_rooms == [[], [], [], []]
    assert cost == 20

    hallway = ["B", "C", "", "D", "", "", "", "A", "", "", ""]
    rooms = [["B"], ["D"], [], []]
    new_hallway, new_rooms, cost = next(possible_moves(hallway, rooms))

    assert new_hallway == ["B", "C", "", "D", "", "D", "", "A", "", "", ""]
    assert new_rooms == [["B"], [], [], []]
    assert cost == 1000


def test_heuristic():
    hallway = [""] * 11
    rooms = [["B"], ["D"], [], []]
    heuristic_cost = heuristic(hallway, rooms)
    assert heuristic_cost == 4000 + 20

    hallway = ["B", "C", "", "D", "", "", "", "A", "", "", ""]
    rooms = [[], [], [], []]
    heuristic_cost = heuristic(hallway, rooms)
    assert heuristic_cost == 4 * 10 + 5 * 100 + 5 * 1000 + 5

    hallway = ["", "", "", "", "", "", "", "A", "", "", ""]
    rooms = [[], [], ["D"], []]
    heuristic_cost = heuristic(hallway, rooms)
    assert heuristic_cost == 2 * 1000 + 5


def test_test_part1():
    assert part1(test_amphipod_start) == 12521


def test_final_part1():
    assert part1(amphipod_start) == 16489


def test_test_part2():
    assert part2(test_amphipod_start) == 44169


def test_final_part2():
    assert part2(amphipod_start) == 43413
