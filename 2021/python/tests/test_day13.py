from common import puzzle_input_by_newlines
from day13 import part1, setup_paper

paper_instructions = puzzle_input_by_newlines(13)
test_paper_instructions = puzzle_input_by_newlines(13, 1)


def test_setup_paper():
    paper = setup_paper(test_paper_instructions[0])
    assert len(paper) == 18

    assert (6,10) in paper
    assert (0,14) in paper
    assert (9,10) in paper
    assert (0,3) in paper
    assert (10,4) in paper
    assert (4,11) in paper
    assert (6,0) in paper
    assert (6,12) in paper
    assert (4,1) in paper
    assert (0,13) in paper
    assert (10,12) in paper
    assert (3,4) in paper
    assert (3,0) in paper
    assert (8,4) in paper
    assert (1,10) in paper
    assert (2,14) in paper
    assert (8,10) in paper
    assert (9,0) in paper


def test_test_part1():
    assert part1(test_paper_instructions) == 17

def test_final_part1():
    assert part1(paper_instructions) == 790
