from common import puzzle_input_as_str
from day15 import part1, part2, setup_map, setup_map_big

puzzle_input = puzzle_input_as_str(15)
test_puzzle_input = puzzle_input_as_str(15, 1)
test_map_generated = puzzle_input_as_str(15, 3)


def test_test_part1():
    assert part1(test_puzzle_input) == 40

def test_final_part1():
    assert part1(puzzle_input) == 398

def test_generate_map():
    cost_map_generated, *_ = setup_map(test_map_generated)
    cost_map_large, *_ = setup_map_big(test_puzzle_input)

    assert cost_map_generated == cost_map_large

def test_test_part2():
    assert part2(test_puzzle_input) == 315

def test_final_part2():
    assert part2(puzzle_input) == 2817
