from common import puzzle_input_by_newlines
from day04 import part1, part2

bingo_values_and_boards = puzzle_input_by_newlines(4)
test_bingo_values_and_boards = puzzle_input_by_newlines(4, 1)


def test_test_part1():
    assert part1(test_bingo_values_and_boards) == 4512

def test_final_part1():
    assert part1(bingo_values_and_boards) == 2496

def test_test_part2():
    assert part2(test_bingo_values_and_boards) == 1924

def test_final_part2():
    assert part2(bingo_values_and_boards) == 25925
