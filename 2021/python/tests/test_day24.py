import pytest
from common import puzzle_input_as_str
from day24 import (
    decrease,
    extract_key_values,
    increase,
    parse_instructions,
    part1,
    part2,
)

puzzle_input = puzzle_input_as_str(24)
test_puzzle_input = puzzle_input_as_str(24, 1)
test_puzzle_input_2 = puzzle_input_as_str(24, 2)


@pytest.mark.parametrize(
    "test_input, expected_value", [["13", 1], ["26", 1], ["24", 0]]
)
def test_parse_test_1(test_input, expected_value):
    variables = parse_instructions(test_puzzle_input, test_input)
    assert variables["z"] == expected_value


@pytest.mark.parametrize(
    "test_input, expected_value",
    [
        ["3", {"w": 0, "x": 0, "y": 1, "z": 1}],
        ["7", {"w": 0, "x": 1, "y": 1, "z": 1}],
        ["9", {"w": 1, "x": 0, "y": 0, "z": 1}],
        ["2", {"w": 0, "x": 0, "y": 1, "z": 0}],
    ],
)
def test_parse_test_2(test_input, expected_value):
    variables = parse_instructions(test_puzzle_input_2, test_input)
    assert variables == expected_value


def test_extract_key_values():
    key_values = extract_key_values(puzzle_input)
    assert len(key_values) == 14


def test_decrease_values():
    values = [9, 9, 9]
    decrease(values, 1)
    assert values == [9, 8, 9]

    assert decrease([9, 9, 1], 2) == [9, 8, 9]
    assert decrease([9, 8, 1], 2) == [9, 7, 9]
    assert decrease([9, 1, 1], 1) == [8, 9, 9]
    assert decrease([9, 1, 1, 7, 1], 2) == [8, 9, 9, 9, 9]

    assert decrease([1, 1, 1], 2) == False
    assert decrease([1, 1, 1], 0) == False


def test_increase_values():

    assert increase([9, 9, 1], 2) == [9, 9, 2]
    assert increase([9, 8, 9], 2) == [9, 9, 1]
    assert increase([9, 1, 1], 1) == [9, 2, 1]
    assert increase([9, 1, 9, 7, 1], 2) == [9, 2, 1, 1, 1]

    assert increase([9, 9, 9], 2) == False
    assert increase([9, 9, 1], 0) == False


def test_final_part1():
    assert part1(puzzle_input) == "91599994399395"


def test_final_part2():
    assert part2(puzzle_input) == "71111591176151"
