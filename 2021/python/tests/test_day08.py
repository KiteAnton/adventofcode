from common import puzzle_input_as_str
from day08 import part1, part2, find_output_number

display_sequence = puzzle_input_as_str(8)
test_display_sequence = puzzle_input_as_str(8, 1)


def test_test_part1():
    assert part1(test_display_sequence) == 26

def test_final_part1():
    assert part1(display_sequence) == 534

def test_find_outputs():
    test_input = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
    assert find_output_number(test_input) == 5353

def test_test_part2():
    assert part2(test_display_sequence) == 61229

def test_final_part2():
    assert part2(display_sequence) == 1070188
