from common import puzzle_input_as_str
from day05 import part1, part2

hydrothermal_vents = puzzle_input_as_str(5)
test_hydrothermal_vents = puzzle_input_as_str(5, 1)


def test_test_part1():
    assert part1(test_hydrothermal_vents) == 5

def test_final_part1():
    assert part1(hydrothermal_vents) == 6666

def test_test_part2():
    assert part2(test_hydrothermal_vents) == 12

def test_final_part2():
    assert part2(hydrothermal_vents) == 19081
