from common import puzzle_input_by_newlines
from day14 import part1, part2

puzzle_input = puzzle_input_by_newlines(14)
test_puzzle_input = puzzle_input_by_newlines(14, 1)


def test_test_part1():
    assert part1(test_puzzle_input) == 1588

def test_final_part1():
    assert part1(puzzle_input) == 3555

def test_test_part2():
    assert part2(test_puzzle_input) == 2188189693529

def test_final_part2():
    assert part2(puzzle_input) == 4439442043739
