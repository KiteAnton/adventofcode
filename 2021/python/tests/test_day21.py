from common import puzzle_input_as_str
from day21 import dice_result, move_position, part1, part2, setup_player_positions

starting_order = puzzle_input_as_str(21)
test_starting_order = puzzle_input_as_str(21, 1)


def test_move_position():
    assert move_position(4, 6) == 10
    assert move_position(4, 8) == 2
    assert move_position(1, 8) == 9
    assert move_position(1, 80) == 1


def test_dice_result():
    assert dice_result(1) == 6
    assert dice_result(50) == 50 + 51 + 52
    assert dice_result(99) == 99 + 100 + 1


def test_setup_player_positions():
    player1_position, player2_position = setup_player_positions(test_starting_order)
    assert player1_position == 4
    assert player2_position == 8


def test_test_part1():
    assert part1(test_starting_order) == 739785


def test_final_part1():
    assert part1(starting_order) == 432450


def test_test_part2():
    assert part2(test_starting_order) == 444356092776315


def test_final_part2():
    assert part2(starting_order) == 138508043837521
