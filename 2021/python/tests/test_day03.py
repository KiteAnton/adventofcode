from common import puzzle_input_as_str
from day03 import part1, part2

diagnostic_codes = puzzle_input_as_str(3)
test_diagnostic_codes = puzzle_input_as_str(3, 1)


def test_test_part1():
    assert part1(test_diagnostic_codes) == 198

def test_final_part1():
    assert part1(diagnostic_codes) == 2648450

def test_test_part2():
    assert part2(test_diagnostic_codes) == 230

def test_final_part2():
    assert part2(diagnostic_codes) == 2845944
