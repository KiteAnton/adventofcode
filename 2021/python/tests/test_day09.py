from common import puzzle_input_as_str
from day09 import part1, part2

ocean_height_map = puzzle_input_as_str(9)
test_ocean_height_map = puzzle_input_as_str(9, 1)


def test_test_part1():
    assert part1(test_ocean_height_map) == 15

def test_final_part1():
    assert part1(ocean_height_map) == 516

def test_test_part2():
    assert part2(test_ocean_height_map) == 1134

def test_final_part2():
    assert part2(ocean_height_map) == 1023660
