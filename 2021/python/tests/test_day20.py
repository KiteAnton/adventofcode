from common import puzzle_input_by_newlines
from day20 import enhance_image, img_string_to_number, new_pixel_value, part1, part2, setup_image, setup_image_enhancer_config, str_from_surrounding_pixels

puzzle_input = puzzle_input_by_newlines(20)
test_puzzle_input = puzzle_input_by_newlines(20, 1)

def test_setup_image_enhancer_config():
    im_enhancer = setup_image_enhancer_config(test_puzzle_input[0])

    assert len(im_enhancer) == 238
    expected_values = [2, 4, 7, 8, 9, 10, 11, 13, 15, 17, 19, 20, 21]

    for value in expected_values:
        assert value in im_enhancer

def test_img_string_to_number():
    number = img_string_to_number("...#...#.")
    assert number == 34

def test_setup_image():
    image_pixels = setup_image(test_puzzle_input[1])
    assert len(image_pixels) == 10

    assert (0, 0) in image_pixels
    assert (0, 3) in image_pixels
    assert (1, 0) in image_pixels
    assert (2, 0) in image_pixels
    assert (2, 1) in image_pixels
    assert (2, 4) in image_pixels
    assert (3, 2) in image_pixels
    assert (4, 2) in image_pixels
    assert (4, 3) in image_pixels
    assert (4, 4) in image_pixels

def test_img_pos_to_string():
    image_pixels = setup_image(test_puzzle_input[1])

    img_surrounding_string = str_from_surrounding_pixels(image_pixels, (2, 2))

    assert img_surrounding_string == "...#...#."

def test_new_value_for_img_pos():
    image_pixels = setup_image(test_puzzle_input[1])
    im_enhancer = setup_image_enhancer_config(test_puzzle_input[0])
    new_value = new_pixel_value(image_pixels, (2, 2), im_enhancer)

    assert new_value

def test_enhance_image_once():
    image_pixels = setup_image(test_puzzle_input[1])
    im_enhancer = setup_image_enhancer_config(test_puzzle_input[0])

    enhance_image(image_pixels, im_enhancer)

    assert len(image_pixels) == 24

    assert (-1, 0 ) in image_pixels
    assert (-1, 1 ) in image_pixels
    assert (-1, 3 ) in image_pixels
    assert (-1, 4 ) in image_pixels
    assert (0, -1) in image_pixels
    assert (0, 2) in image_pixels
    assert (0, 4) in image_pixels
    assert (1, -1) in image_pixels
    assert (1, 0) in image_pixels
    assert (1, 2) in image_pixels
    assert (1, 5) in image_pixels
    assert (2, -1) in image_pixels
    assert (2, 0) in image_pixels
    assert (2, 1) in image_pixels
    assert (2, 2) in image_pixels
    assert (2, 5) in image_pixels
    assert (3, 0) in image_pixels
    assert (3, 3) in image_pixels
    assert (3, 4) in image_pixels
    assert (4, 1) in image_pixels
    assert (4, 2) in image_pixels
    assert (4, 5) in image_pixels
    assert (5, 2) in image_pixels
    assert (5, 4) in image_pixels

def test_enhance_image_twice():
    image_pixels = setup_image(test_puzzle_input[1])
    im_enhancer = setup_image_enhancer_config(test_puzzle_input[0])

    enhance_image(image_pixels, im_enhancer)
    enhance_image(image_pixels, im_enhancer)

    assert len(image_pixels) == 35

    assert (-2, 5) in image_pixels
    assert (-1, -1) in image_pixels
    assert (-1, 2) in image_pixels
    assert (-1, 4) in image_pixels
    assert (0, -2) in image_pixels
    assert (0, 0) in image_pixels
    assert (0, 4) in image_pixels
    assert (0, 5) in image_pixels
    assert (0, 6) in image_pixels
    assert (1, -2) in image_pixels
    assert (1, 2) in image_pixels
    assert (1, 3) in image_pixels
    assert (1, 5) in image_pixels
    assert (2, -2) in image_pixels
    assert (2, 4) in image_pixels
    assert (2, 6) in image_pixels
    assert (3, -1) in image_pixels
    assert (3, 1) in image_pixels
    assert (3, 2) in image_pixels
    assert (3, 3) in image_pixels
    assert (3, 4) in image_pixels
    assert (3, 5) in image_pixels
    assert (4, 0) in image_pixels
    assert (4, 2) in image_pixels
    assert (4, 3) in image_pixels
    assert (4, 4) in image_pixels
    assert (4, 5) in image_pixels
    assert (4, 6) in image_pixels
    assert (5, 1) in image_pixels
    assert (5, 2) in image_pixels
    assert (5, 4) in image_pixels
    assert (5, 5) in image_pixels
    assert (6, 2) in image_pixels
    assert (6, 3) in image_pixels
    assert (6, 4) in image_pixels

def test_test_part1():
    assert part1(test_puzzle_input) == 35

def test_final_part1():
    assert part1(puzzle_input) == 5619

def test_test_part2():
    assert part2(test_puzzle_input) == 3351

def test_final_part2():
    assert part2(puzzle_input) == 20122
