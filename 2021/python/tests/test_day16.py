from common import puzzle_input_as_str
import pytest
from day16 import part1, part2, parse_package, count_version_sums

bits_transmission = puzzle_input_as_str(16)

def test_hex_string_to_literal_value():
    hex_string = "D2FE28"
    # result, _ = parse_package(hex_string)
    result, _ = parse_package(hex_string)
    assert result == {
        'version': 6, 'type_id': 4, 'value': 2021
        }

def test_hex_string_to_operator():
    hex_string = "38006F45291200"
    result, _ = parse_package(hex_string)
    assert result == {
        'version': 1, 'type_id': 6, 'value': 1,
        'sub_packets': [
            { 'version': 6, 'type_id': 4, 'value': 10 },
            { 'version': 2, 'type_id': 4, 'value': 20 }]
        }

def test_hex_string_to_operator2():
    hex_string = "EE00D40C823060"
    result, _ = parse_package(hex_string)
    assert result == {
        'version': 7, 'type_id': 3, 'value': 3,
        'sub_packets': [
            { 'version': 2, 'type_id': 4, 'value': 1 },
            { 'version': 4, 'type_id': 4, 'value': 2 },
            { 'version': 1, 'type_id': 4, 'value': 3 }]
        }

@pytest.mark.parametrize("hex_string, version_sum", [
                         ("8A004A801A8002F478", 16),
                         ("620080001611562C8802118E34", 12),
                         ("C0015000016115A2E0802F182340", 23),
                         ("A0016C880162017C3686B18A3D4780", 31)])
def test_hex_string_to_operator3(hex_string, version_sum):
    result, _ = parse_package(hex_string)
    assert count_version_sums(result) == version_sum


def test_operator_packages():
    # finds the sum of 1 and 2, resulting in the value 3.
    hex_string = "C200B40A82"
    result, _ = parse_package(hex_string)
    assert result['value'] == 3

    # finds the product of 6 and 9, resulting in the value 54.
    hex_string = "04005AC33890"
    result, _ = parse_package(hex_string)
    assert result['value'] == 54

    # finds the minimum of 7, 8, and 9, resulting in the value 7.
    hex_string = "880086C3E88112"
    result, _ = parse_package(hex_string)
    assert result['value'] == 7

    # finds the maximum of 7, 8, and 9, resulting in the value 9.
    hex_string = "CE00C43D881120"
    result, _ = parse_package(hex_string)
    assert result['value'] == 9

    # produces 1, because 5 is less than 15.
    hex_string = "D8005AC2A8F0"
    result, _ = parse_package(hex_string)
    assert result['value'] == 1

    # produces 0, because 5 is not greater than 15.
    hex_string = "F600BC2D8F"
    result, _ = parse_package(hex_string)
    assert result['value'] == 0

    # produces 0, because 5 is not equal to 15.
    hex_string = "9C005AC2F8F0"
    result, _ = parse_package(hex_string)
    assert result['value'] == 0

    # produces 1, because 1 + 3 = 2 * 2
    hex_string = "9C0141080250320F1802104A08"
    result, _ = parse_package(hex_string)
    assert result['value'] == 1

def test_final_part1():
    assert part1(bits_transmission) == 893

def test_final_part2():
    assert part2(bits_transmission) == 4358595186090
