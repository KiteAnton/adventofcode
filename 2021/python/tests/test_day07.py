from common import puzzle_input_as_str
from day07 import move_all_to_position, part1, part2, setup_crab_positions
from day07 import move_cost

crab_positions = puzzle_input_as_str(7)
test_crab_positions = puzzle_input_as_str(7, 1)


def test_end_positions():
    positions = setup_crab_positions(test_crab_positions[0])
    assert move_all_to_position(positions, target_position=1) == 41
    assert move_all_to_position(positions, target_position=2) == 37
    assert move_all_to_position(positions, target_position=3) == 39
    assert move_all_to_position(positions, target_position=10) == 71

def test_test_part1():
    assert part1(test_crab_positions) == 37

def test_final_part1():
    assert part1(crab_positions) == 333755

def test_move_cost():
    assert move_cost(1) == 1
    assert move_cost(2) == 3
    assert move_cost(3) == 6
    assert move_cost(4) == 10
    assert move_cost(5) == 15
    assert move_cost(9) == 45
    assert move_cost(11) == 66

def test_test_part2():
    assert part2(test_crab_positions) == 168

def test_final_part2():
    assert part2(crab_positions) == 94017638
