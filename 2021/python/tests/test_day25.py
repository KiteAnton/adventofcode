from common import puzzle_input_as_str
from day25 import move_step, part1, setup_map

puzzle_input = puzzle_input_as_str(25)
test_puzzle_input = puzzle_input_as_str(25, 1)
test_puzzle_input2 = puzzle_input_as_str(25, 2)
test_puzzle_input3 = puzzle_input_as_str(25, 3)
test_puzzle_input4 = puzzle_input_as_str(25, 4)


def test_setup_map():
    east_facing, south_facing, map_constraints = setup_map(test_puzzle_input)
    assert len(east_facing) == 23
    assert len(south_facing) == 26

    assert (2, 4) in south_facing
    assert (6, 5) in east_facing

    assert map_constraints == (9, 10)


def test_move_step():
    east_facing, south_facing, map_constraints = setup_map(test_puzzle_input)
    no_change = move_step(east_facing, south_facing, map_constraints)
    assert not no_change
    assert len(east_facing) == 23
    assert len(south_facing) == 26


def test_move_steps():
    east_facing, south_facing, map_constraints = setup_map(test_puzzle_input2)
    no_change = move_step(east_facing, south_facing, map_constraints)
    assert not no_change
    assert east_facing == {(0, 3), (0, 4), (0, 5), (0, 6), (0, 8)}

    no_change = move_step(east_facing, south_facing, map_constraints)
    assert not no_change
    assert east_facing == {(0, 3), (0, 4), (0, 5), (0, 7), (0, 9)}

    east_facing, south_facing, map_constraints = setup_map(test_puzzle_input3)
    assert east_facing == {(1, 1), (2, 7)}
    assert south_facing == {(1, 2), (1, 7)}
    no_change = move_step(east_facing, south_facing, map_constraints)
    assert not no_change
    assert east_facing == {(1, 1), (2, 8)}
    assert south_facing == {(2, 2), (2, 7)}


def test_wrap_around():
    east_facing, south_facing, map_constraints = setup_map(test_puzzle_input4)
    for _ in range(3):
        assert not move_step(east_facing, south_facing, map_constraints)
    move_step(east_facing, south_facing, map_constraints)

    assert east_facing == {(0, 0), (2, 2), (3, 1), (4, 3)}
    assert south_facing == {(1, 2), (2, 4), (3, 3), (6, 0)}


def test_test_part1():
    assert part1(test_puzzle_input) == 58


def test_final_part1():
    assert part1(puzzle_input) == 520
