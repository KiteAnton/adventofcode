from common import puzzle_input_as_int
from day01 import part1, part2

sonar_sweep = puzzle_input_as_int(1)
test_sonar_sweep = puzzle_input_as_int(1, 1)


def test_test_part1():
    assert part1(test_sonar_sweep) == 7

def test_final_part1():
    assert part1(sonar_sweep) == 1266

def test_test_part2():
    assert part2(test_sonar_sweep) == 5

def test_final_part2():
    assert part2(sonar_sweep) == 1217
