from common import puzzle_input_as_str
from day22 import part1, part2, set_states, setup_reboot_sequences

reboot_steps = puzzle_input_as_str(22)
test_small_reboot_steps = puzzle_input_as_str(22, 1)
test_reboot_steps = puzzle_input_as_str(22, 2)
test_reboot_steps3 = puzzle_input_as_str(22, 3)


def test_setup_reboot_sequences():
    reboot_sequences = setup_reboot_sequences(test_small_reboot_steps)
    assert len(reboot_sequences) == 4
    expected_sequences = [
        {"command": "on", "x": [10, 12], "y": [10, 12], "z": [10, 12]},
        {"command": "on", "x": [11, 13], "y": [11, 13], "z": [11, 13]},
        {"command": "off", "x": [9, 11], "y": [9, 11], "z": [9, 11]},
        {"command": "on", "x": [10, 10], "y": [10, 10], "z": [10, 10]},
    ]
    for i, expected_sequence in enumerate(expected_sequences):
        assert reboot_sequences[i] == expected_sequence


def test_turn_on_off_cubes():
    reactor_cubes = set()

    sequence = {"command": "on", "x": [10, 12], "y": [10, 12], "z": [10, 12]}
    set_states(reactor_cubes, sequence)
    assert len(reactor_cubes) == 27

    sequence = {"command": "on", "x": [11, 13], "y": [11, 13], "z": [11, 13]}
    set_states(reactor_cubes, sequence)
    assert len(reactor_cubes) == 27 + 19

    sequence = {"command": "off", "x": [9, 11], "y": [9, 11], "z": [9, 11]}
    set_states(reactor_cubes, sequence)
    assert len(reactor_cubes) == 27 + 19 - 8

    sequence = {"command": "on", "x": [10, 10], "y": [10, 10], "z": [10, 10]}
    set_states(reactor_cubes, sequence)
    assert len(reactor_cubes) == 27 + 19 - 8 + 1


def test_test_part1():
    assert part1(test_reboot_steps) == 590784


def test_final_part1():
    assert part1(reboot_steps) == 612714


def test_test_part2():
    assert part2(test_reboot_steps3) == 2758514936282235


def test_final_part2():
    assert part2(reboot_steps) == 1311612259117092
