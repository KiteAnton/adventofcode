import math
from common import puzzle_input_as_str

snailfish_input = puzzle_input_as_str(18)

def string_to_lists(input_string):
    lists = list(input_string)
    for i, val in enumerate(lists):
        if val not in [',', ']', '[']:
            lists[i] = int(val)
    return lists

def explode_pairs(lists):
    nest_levels = 0
    for i, val in enumerate(lists):
        if val == '[':
            nest_levels += 1
            continue
        if val == ']':
            nest_levels -= 1
            if nest_levels >= 4:
                add_right = lists[i-1]
                add_left = lists[i-3]
                lists[i-4] = 0
                for _ in range(4):
                    lists.pop(i-3)
                search_pos = i-5
                while search_pos >= 0:
                    if lists[search_pos] not in ['[', ',', ']']:
                        lists[search_pos] += add_left
                        break
                    search_pos -= 1
                search_pos = i-3
                while search_pos < len(lists):
                    if lists[search_pos] not in ['[', ',', ']']:
                        lists[search_pos] += add_right
                        break
                    search_pos += 1
                return True

    return False

def split_number(lists):
    for i, val in enumerate(lists):
        if isinstance(val, int) and val >= 10:
            left_val = math.floor(val / 2)
            right_val = math.ceil(val / 2)
            lists[i] = '['
            lists.insert(i+1, left_val)
            lists.insert(i+2, ',')
            lists.insert(i+3, right_val)
            lists.insert(i+4, ']')
            return True

    return False

def reduce_numbers(lists):
    result = True  # Run at least once
    while result:
        result = explode_pairs(lists)
        if not result:
            result = split_number(lists)


def add_lists(first_list, second_list):
    second_list = string_to_lists(',' + second_list + ']')
    new_list = ['['] + first_list
    new_list += second_list
    reduce_numbers(new_list)
    return new_list


def sum_up_from_input(puzzle_input):
    lists = string_to_lists(puzzle_input[0])
    for i in range(1, len(puzzle_input)):
        lists = add_lists(lists, puzzle_input[i])

    return lists

def list_structure_from_string_list(lists):
    new_list = lists.copy()
    changed = True
    while changed:
        changed = False
        bracket_stack = []
        for i, char in enumerate(new_list):
            if char == '[':
                bracket_stack.append(i)
            elif char == ']':
                start_list_pos = bracket_stack.pop()
                new_list[start_list_pos] = \
                    [new_list[start_list_pos+1], new_list[i-1]]
                for i in range(start_list_pos+1, i+1):
                    new_list.pop(start_list_pos+1)
                changed = True
                break

    return new_list

def calculate_magnitude(list_input):
    magnitude = 0
    magnitude_factor = [3, 2]
    for i, val in enumerate(list_input):
        if isinstance(val, list):
            val = calculate_magnitude(val)
        magnitude += val * magnitude_factor[i]

    return magnitude

def part1(puzzle_input):
    lists = sum_up_from_input(puzzle_input)
    real_list = list_structure_from_string_list(lists)[0]
    magnitude = calculate_magnitude(real_list)

    return magnitude

def part2(puzzle_input):
    max_magnitude = 0
    for i, _ in enumerate(puzzle_input):
        lists = string_to_lists(puzzle_input[i])
        for j, _ in enumerate(puzzle_input):
            if i != j:
                lists_added = add_lists(lists, puzzle_input[j])
                lists_added = list_structure_from_string_list(lists_added)
                magnitude = calculate_magnitude(lists_added[0])
                if magnitude > max_magnitude:
                    max_magnitude = magnitude


    return max_magnitude


print("Part 1:", part1(snailfish_input))
print("Part 2:", part2(snailfish_input))
