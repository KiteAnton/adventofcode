import heapq
from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(15)

def setup_map(puzzle_input, iterations=1):
    cost_map = {}
    rows = len(puzzle_input)
    cols = len(puzzle_input[0])

    for i in range(rows * iterations):
        line = puzzle_input[i % rows]
        value_to_add_row = int(i / rows)
        for j in range(cols * iterations):
            value_to_add_cols = int(j / cols)
            new_value = int(line[j%cols]) + value_to_add_row + value_to_add_cols
            while new_value > 9:
                new_value -= 9
            cost_map[(i, j)] = new_value

    return cost_map, rows * iterations, cols * iterations

def find_path(cost_map, nr_row, nr_col, start, end_node):
    to_visit = [(0, start)]
    nodes = {start: 0}

    movements = [ [-1, 0], [0, 1], [1, 0], [0, -1] ]

    while to_visit:
        current_node_g, current_node = heapq.heappop(to_visit)

        if current_node == end_node:
            return current_node_g

        for new_position in movements:
            new_node_position = (current_node[0] + new_position[0],
                                 current_node[1] + new_position[1])

            # Check not already visitem and within map
            if new_node_position in nodes or \
               not 0 <= new_node_position[0] <= nr_row or \
               not 0 <= new_node_position[1] <= nr_col:
                continue

            new_node_g = current_node_g + cost_map[new_node_position]
            if new_node_position in nodes and \
               nodes[new_node_position] <= new_node_g:
                continue
            nodes[new_node_position] = new_node_g
            heapq.heappush(to_visit, (new_node_g, new_node_position))

def part1(puzzle_input):
    cost_map, nr_row, nr_col = setup_map(puzzle_input)
    start = (0, 0)
    end = (nr_row - 1, nr_col - 1)

    return find_path(cost_map, nr_row - 1, nr_col - 1, start, end)

def part2(puzzle_input):
    cost_map, nr_rows, nr_cols = setup_map(puzzle_input, iterations=5)

    start = (0, 0)
    end = (nr_rows - 1, nr_cols - 1)

    return find_path(cost_map, nr_rows - 1, nr_cols - 1, start, end)

print("Part 1", part1(puzzle_input))
print("Part 2", part2(puzzle_input))

# import cProfile
# cProfile.run('print("Part 1:", part1(puzzle_input))')
# cProfile.run('print("Part 2:", part2(puzzle_input))')
