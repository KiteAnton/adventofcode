import heapq
import sys

from common import puzzle_input_as_str

amphipod_start = puzzle_input_as_str(23)

move_cost = {"A": 1, "B": 10, "C": 100, "D": 1000}
room_order = ["A", "B", "C", "D"]


def setup_map(puzzle_input):
    hallway = [""] * 11

    rooms = [[], [], [], []]
    for room in range(4):
        rooms[room].append(puzzle_input[2][room * 2 + 3])
        rooms[room].append(puzzle_input[3][room * 2 + 1])

    # Remove any pods that are already in correct position
    for room, room_type in enumerate(["A", "B", "C", "D"]):
        while rooms[room][-1] == room_type:
            rooms[room].pop()

    return hallway, rooms


def calculate_unoptimizable_cost(rooms):
    cost = 0
    cost_vs_depth = [0, 1, 3, 6, 10]

    for room in rooms:
        for i, pod in enumerate(room):
            cost += (i + 1) * move_cost[pod]

    for i, room_type in enumerate(["A", "B", "C", "D"]):
        cost += cost_vs_depth[len(rooms[i])] * move_cost[room_type]

    return cost


def room_available(rooms, pod_type):
    return len(rooms[room_order.index(pod_type)]) == 0


def path_free(hallway, start, end):

    for i in range(min(start, end), max(start, end) + 1):
        if hallway[i]:
            return False

    return True


def heuristic(hallway, rooms):
    cost = 0
    for pos, pod in enumerate(hallway):
        if not pod:
            continue
        final_destination = (room_order.index(pod) + 1) * 2
        cost += abs(final_destination - pos) * move_cost[pod]

    for room_index, room in enumerate(rooms):
        if not room:
            continue
        room_pos = (room_index + 1) * 2
        for pod in room:
            final_destination = (room_order.index(pod) + 1) * 2
            cost += abs(final_destination - room_pos) * move_cost[pod]

    return cost


def possible_moves(hallway, rooms):

    # Move from room to hallway, on directly into new room if possible
    for room_index, room in enumerate(rooms):
        if not room:
            continue
        new_room = list(room)
        pod = new_room.pop(0)
        room_pos = (room_index + 1) * 2
        new_rooms = list(rooms)
        new_rooms[room_index] = new_room

        # Check if destination room is 'free', including path to destination
        destination_room = room_order.index(pod)
        destination_room_pos = (destination_room + 1) * 2
        if not rooms[destination_room] and path_free(
            hallway, room_pos, destination_room_pos
        ):
            new_hallway = list(hallway)
            cost = abs(destination_room - room_index) * 2 * move_cost[pod]
            yield new_hallway, new_rooms, cost
            continue

        # Move INTO hallway
        for pos, occupied in enumerate(hallway):
            if occupied or pos in [2, 4, 6, 8]:
                continue
            room_pos = (room_index + 1) * 2
            if path_free(hallway, pos, room_pos):
                new_hallway = list(hallway)
                new_hallway[pos] = pod
                cost = abs(pos - room_pos) * move_cost[pod]
                yield new_hallway, new_rooms, cost

    # Move FROM hallway to final destination
    for pos, pod in enumerate(hallway):
        if pod == "":
            continue
        room_index = room_order.index(pod)
        if rooms[room_index]:
            continue
        final_destination = (room_index + 1) * 2
        new_hallway = list(hallway)
        new_hallway[pos] = ""
        if path_free(new_hallway, pos, final_destination):
            cost = abs(pos - final_destination) * move_cost[pod]
            yield new_hallway, rooms, cost


def solve_a_star(hallway, rooms):

    to_visit = [(0, 0, hallway, rooms)]
    visited = {}

    while to_visit:
        _, current_cost, current_hallway, current_rooms = heapq.heappop(
            to_visit
        )

        if current_hallway == [""] * 11 and current_rooms == [[], [], [], []]:
            return current_cost

        for new_hallway, new_rooms, cost in possible_moves(
            current_hallway, current_rooms
        ):
            visited_node = (
                tuple(new_hallway),
                tuple(new_rooms[0]),
                tuple(new_rooms[1]),
                tuple(new_rooms[2]),
                tuple(new_rooms[3]),
            )

            new_cost = current_cost + cost
            if visited_node in visited and visited[visited_node] < new_cost:
                continue
            visited[visited_node] = new_cost
            heuristic_cost = heuristic(new_hallway, new_rooms)
            heapq.heappush(
                to_visit, (new_cost + heuristic_cost, new_cost, new_hallway, new_rooms)
            )

    return 0


def part1(puzzle_input):
    hallway, rooms = setup_map(puzzle_input)
    unoptimizable_cost = calculate_unoptimizable_cost(rooms)

    lowest_cost = solve_a_star(hallway, rooms)

    return lowest_cost + unoptimizable_cost


def part2(puzzle_input):
    hallway, rooms = setup_map(puzzle_input)

    rooms[0].insert(1, "D")
    rooms[0].insert(1, "D")

    rooms[1].insert(1, "B")
    rooms[1].insert(1, "C")

    rooms[2].insert(1, "A")
    rooms[2].insert(1, "B")

    rooms[3].insert(1, "C")
    rooms[3].insert(1, "A")

    unoptimizable_cost = calculate_unoptimizable_cost(rooms)

    lowest_cost = solve_a_star(hallway, rooms)

    return lowest_cost + unoptimizable_cost


if __name__ == "__main__":
    print("Part1: ", part1(amphipod_start))
    print("Part2: ", part2(amphipod_start))
