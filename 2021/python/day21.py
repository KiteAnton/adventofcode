from functools import cache

from common import puzzle_input_as_str

starting_order = puzzle_input_as_str(21)


def setup_player_positions(puzzle_input):
    *_, player1_position = puzzle_input[0].split(" ")
    *_, player2_position = puzzle_input[1].split(" ")

    return int(player1_position), int(player2_position)


def dice_result(start):
    result = 0
    die_value = start - 1
    for _ in range(3):
        die_value = (die_value % 100) + 1
        result += die_value
    return result


def move_position(start, steps):
    end_value = 10
    return (start - 1 + steps) % end_value + 1


def part1(puzzle_input):
    player1_position, player2_position = setup_player_positions(puzzle_input)
    player1 = {"score": 0, "position": player1_position}
    player2 = {"score": 0, "position": player2_position}

    next_player = player1

    dice_index = 1

    while player1["score"] < 1000 and player2["score"] < 1000:

        next_player["position"] = move_position(
            next_player["position"], dice_result(dice_index)
        )
        dice_index += 3
        next_player["score"] += next_player["position"]

        if next_player == player1:
            next_player = player2
        else:
            next_player = player1

    return next_player["score"] * (dice_index - 1)


@cache
def play_quantum_round(player1_pos, player2_pos, score1, score2):
    wins_player1 = 0
    wins_player2 = 0

    if score1 >= 21:
        return 1, 0
    if score2 >= 21:
        return 0, 1

    for die1 in [1, 2, 3]:
        for die2 in [1, 2, 3]:
            for die3 in [1, 2, 3]:
                new_pos = move_position(player1_pos, die1 + die2 + die3)

                w2, w1 = play_quantum_round(
                    player2_pos, new_pos, score2, score1 + new_pos
                )
                wins_player1 += w1
                wins_player2 += w2

    return wins_player1, wins_player2


def part2(puzzle_input):
    player1_position, player2_position = setup_player_positions(puzzle_input)

    wins_player1, wins_player2 = play_quantum_round(
        player1_position, player2_position, 0, 0
    )

    return max(wins_player1, wins_player2)


if __name__ == "__main__":
    print("Part 1:", part1(starting_order))
    print("Part 2:", part2(starting_order))
