from common import puzzle_input_as_str

navigation_subsystem = puzzle_input_as_str(10)


def incorrect_lines_and_error_score(lines):
    illegal_chars = {')': 3, ']': 57, '}': 1197, '>': 25137 }
    expected_closing_char = {'(': ')', '[': ']', '{': '}', '<': '>'}
    correct_lines = []
    syntax_error_score = 0
    for line in lines:
        chunk_stack = []
        for char in line:
            if char in ['(', '[', '{', '<']:
                chunk_stack.append(char)
            else:
                if char != expected_closing_char[chunk_stack.pop()]:
                    syntax_error_score += illegal_chars[char]
                    break
        else:
            correct_lines.append(line)

    return syntax_error_score, correct_lines


def part1(puzzle_input):
    syntax_error_score, _ = incorrect_lines_and_error_score(puzzle_input)
    return syntax_error_score


def part2(puzzle_input):
    _, correct_lines = incorrect_lines_and_error_score(puzzle_input)
    auto_completion_score = []
    chunk_score = {'(': 1, '[': 2, '{': 3, '<': 4}

    for line in correct_lines:
        line_score = 0
        chunk_stack = []
        for char in line:
            if char in ['(', '[', '{', '<']:
                chunk_stack.append(char)
            else:
                chunk_stack.pop()

        while chunk_stack:
            line_score *= 5
            line_score += chunk_score[chunk_stack.pop()]
        auto_completion_score.append(line_score)

    auto_completion_score.sort()

    return auto_completion_score[int((len(auto_completion_score)-1)/2)]


print("Part 1:", part1(navigation_subsystem))
print("Part 2:", part2(navigation_subsystem))
