import string
import math
from common import puzzle_input_by_newlines

puzzle_input = puzzle_input_by_newlines(14)


def setup_polymer_template(template_raw):
    polymer_template = {}
    for line in template_raw:
        char_pair, insert_char = line.split(" -> ")
        polymer_template[char_pair] = [char_pair[0] + insert_char, \
                                       insert_char + char_pair[1]]
    return polymer_template

def setup_polymer(initial_polymer):
    polymer = {}
    for i in range(len(initial_polymer)-1):
        char_pair = initial_polymer[i:i+2]
        if char_pair not in polymer:
            polymer[char_pair] = 0
        polymer[char_pair] += 1

    return polymer

def polymer_step(polymer, polymer_template):
    old_polymer = polymer.copy()

    for key, value in old_polymer.items():
        new_pairs = polymer_template[key]
        if new_pairs[0] not in polymer:
            polymer[new_pairs[0]] = 0
        if new_pairs[1] not in polymer:
            polymer[new_pairs[1]] = 0
        polymer[new_pairs[0]] += value
        polymer[new_pairs[1]] += value
        polymer[key] -= value

def find_most_and_least_common_char(polymer):
    nr_most_common = 0
    nr_least_common = 0

    for char in string.ascii_uppercase:
        char_nr = 0
        for key, value in polymer.items():
            if not value:
                continue
            char_nr += key.count(char) * value

        if not char_nr: continue
        char_nr = math.ceil(char_nr / 2)
        if not nr_most_common or char_nr > nr_most_common:
            nr_most_common = char_nr
        if not nr_least_common or char_nr < nr_least_common:
            nr_least_common = char_nr

    return nr_most_common, nr_least_common


def run_iterations(initial_polymer, polymer_template_raw, iterations):
    polymer = setup_polymer(initial_polymer)
    polymer_template = setup_polymer_template(polymer_template_raw)

    for _ in range(iterations):
        polymer_step(polymer, polymer_template)

    nr_most_common, nr_least_common = find_most_and_least_common_char(polymer)

    return nr_most_common - nr_least_common

def part1(puzzle_input):
    return run_iterations(puzzle_input[0][0], puzzle_input[1], 10)

def part2(puzzle_input):
    return run_iterations(puzzle_input[0][0], puzzle_input[1], 40)

print("Part 1", part1(puzzle_input))
print("Part 2", part2(puzzle_input))
