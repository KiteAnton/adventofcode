from common import puzzle_input_as_str

octopus_map = puzzle_input_as_str(11)

def sim_iteration(oct_map):
    # Increase all values with 1
    for row in range(len(oct_map)):
        for col in range(len(oct_map[row])):
            oct_map[row][col] += 1

    # Check for energy levels > 9, if so then 'flash'
    # Since flash might 'trigger flash' of adjacent octopus then loop
    # until no more flashes occurs.
    flashed = set()
    while True:
        flashes_start = len(flashed)
        for row in range(len(oct_map)):
            for col in range(len(oct_map[row])):
                if oct_map[row][col] > 9:
                    if (row, col) not in flashed:
                        flashed.add((row, col))
                        increace_by_flash(oct_map, row, col)
        if flashes_start == len(flashed):
            break

    # Reset all 'flashed' values to 0
    for row in range(len(oct_map)):
        for col in range(len(oct_map[row])):
            if oct_map[row][col] > 9:
                oct_map[row][col] = 0

    return len(flashed)

def increace_by_flash(oct_map, row, col):
    for i in range(max(row-1, 0), min(len(oct_map), row+2)):
        for j in range(max(col-1, 0), min(len(oct_map[i]), col+2)):
            oct_map[i][j] += 1

def setup_map(input_map):
    oct_map = []
    for line in input_map:
        oct_line = list(map(int, list(line)))
        oct_map.append(oct_line)
    return oct_map

def part1(puzzle_input):
    total_flashes = 0
    puzzle_map = setup_map(puzzle_input)
    for _ in range(100):
        total_flashes += sim_iteration(puzzle_map)

    return total_flashes

def part2(puzzle_input):
    puzzle_map = setup_map(puzzle_input)
    flashes_wanted = len(puzzle_map) * len(puzzle_map[0])
    day_all_flashes = 0
    for i in range(2000):
        flashes = sim_iteration(puzzle_map)
        if flashes == flashes_wanted:
            day_all_flashes = i + 1
            break

    return day_all_flashes


print("Part 1:", part1(octopus_map))
print("Part 2:", part2(octopus_map))
