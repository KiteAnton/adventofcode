from common import puzzle_input_as_str


submarine_instructions = puzzle_input_as_str(2)


def part1(puzzle_input):
    depth = 0
    horizontal_position = 0

    for line in puzzle_input:
        operation, value = line.split(' ')
        if operation == 'forward':
            horizontal_position += int(value)
        elif operation == 'down':
            depth += int(value)
        elif operation == 'up':
            depth -= int(value)

    return depth * horizontal_position


def part2(puzzle_input):
    depth = 0
    horizontal_position = 0
    aim = 0

    for line in puzzle_input:
        operation, value = line.split(' ')
        if operation == 'forward':
            horizontal_position += int(value)
            depth += aim * int(value)
        elif operation == 'down':
            aim += int(value)
        elif operation == 'up':
            aim -= int(value)

    return depth * horizontal_position

print("Part 1:", part1(submarine_instructions))
print("Part 2:", part2(submarine_instructions))
