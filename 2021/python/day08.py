from common import puzzle_input_as_str


display_sequences = puzzle_input_as_str(8)

def intersection(list1, list2):
    return [value for value in list1 if value in list2]

def missing_values(list1, list2):
    return [value for value in list1 if value not in list2]

def setup_input_outputs(line):
    inputs_raw, outputs_raw = line.split(' | ')
    inputs = [sorted(list(value)) for value in inputs_raw.split()]
    outputs = [sorted(list(value)) for value in outputs_raw.split()]

    return inputs, outputs

def find_output_number(line):
    mapped_numbers = {}
    numbers = {}
    segments = {}
    chars_done = set()
    words_5 = []
    words_6 = []
    inputs, outputs = setup_input_outputs(line)

    for inp in inputs:
        if len(inp) == 2:
            numbers[1] = inp
            mapped_numbers[tuple(inp)] = 1
        elif len(inp) == 3:
            numbers[7] = inp
            mapped_numbers[tuple(inp)] = 7
        elif len(inp) == 4:
            numbers[4] = inp
            mapped_numbers[tuple(inp)] = 4
        elif len(inp) == 5:
            words_5.append(inp)
        elif len(inp) == 6:
            words_6.append(inp)
        elif len(inp) == 7:
            numbers[8] = inp
            mapped_numbers[tuple(inp)] = 8

    # Find segment A
    segments['A'] = missing_values(numbers[7], numbers[1])[0]
    chars_done.add(segments['A'])

    # Find digit 6, and then getting segment C and F
    for word in words_6:
        if intersection(word, numbers[1]) != numbers[1]:
            numbers[6] = word
            mapped_numbers[tuple(word)] = 6
            segment_c = missing_values(numbers[1], word)
            segments['C'] = segment_c[0]
            chars_done.add(segments['C'])
            segments['F'] = missing_values(numbers[1], segment_c)[0]
            chars_done.add(segments['F'])
            words_6.remove(word)
            break

    # Find digit 2 and segment b
    for word in words_5:
        if segments['F'] not in word:
            numbers[2] = word
            mapped_numbers[tuple(word)] = 2
            for char in missing_values(numbers[8], word):
                if char != segments['F']:
                    segments['B'] = char
                    chars_done.add(segments['B'])
            words_5.remove(word)
            break

    # Find segment D
    for char in numbers[4]:
        if char not in chars_done:
            segments['D'] = char
            chars_done.add(char)
            break

    # Find digit 0 and then 9
    for word in words_6:
        if segments['D'] not in word:
            numbers[0] = word
            mapped_numbers[tuple(word)] = 0
        else:
            numbers[9] = word
            mapped_numbers[tuple(word)] = 9

    # Find digit 3 and 5
    for word in words_5:
        if segments['B'] in word:
            numbers[5] = word
            mapped_numbers[tuple(word)] = 5
        else:
            numbers[3] = word
            mapped_numbers[tuple(word)] = 3

    # Map output
    output_result = 0
    result_weight = [1000, 100, 10, 1]
    for i, output in enumerate(outputs):
        output_result += mapped_numbers[tuple(output)] * result_weight[i]

    return output_result


def part1(puzzle_input):
    count = 0
    for line in puzzle_input:
        _, outputs = setup_input_outputs(line)
        for output in outputs:
            if len(output) in [2, 3, 4, 7]:
                count += 1
    return count

def part2(puzzle_input):
    total_result = 0
    for line in puzzle_input:
        total_result += find_output_number(line)

    return total_result


print("Part 1:", part1(display_sequences))
print("Part 2:", part2(display_sequences))
