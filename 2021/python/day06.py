from common import puzzle_input_as_str

lanternfishes = puzzle_input_as_str(6)


def simulate_day_growth(fishes, day):
    weekday = day % 7
    new_fishes = fishes.get(weekday, 0)
    fishes['total'] += new_fishes
    fishes[weekday] = new_fishes + fishes['new'][0]
    fishes['new'][0] = fishes['new'][1]
    fishes['new'][1] = new_fishes


def setup_fish_pattern(starting_sequence):
    fishes = list(map(int, starting_sequence.split(',')))
    fishes_per_day = {}
    fishes_per_day['new'] = [0, 0]
    fishes_per_day['total'] = len(fishes)
    for i in range(7):
        fishes_per_day[i] = 0
    for fish in fishes:
        fishes_per_day[fish] += 1

    return fishes_per_day


def part1(puzzle_input):

    fishes_per_day = setup_fish_pattern(puzzle_input[0])

    for i in range(80):
        simulate_day_growth(fishes_per_day, i)

    return fishes_per_day['total']


def part2(puzzle_input):

    fishes_per_day = setup_fish_pattern(puzzle_input[0])

    for i in range(256):
        simulate_day_growth(fishes_per_day, i)

    return fishes_per_day['total']


# import cProfile
# cProfile.run('print("Part 2: 362346 -- ", part2(lanternfishes))')

print("Part 1:", part1(lanternfishes))
print("Part 2:", part2(lanternfishes))
