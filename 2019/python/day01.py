from common import puzzle_input_as_int

puzzle_input = puzzle_input_as_int(day=1)


def part1(fuel_mass):
    return sum(map(lambda x: (x // 3) - 2, fuel_mass))


def part2(fuel_mass):
    total_fuel = 0
    for mass in fuel_mass:
        fuel_needed = mass // 3 - 2
        new_fuel = fuel_needed
        while new_fuel > 0:
            new_fuel = new_fuel // 3 - 2
            if new_fuel <= 0:
                break
            fuel_needed += new_fuel
        total_fuel += fuel_needed
    return total_fuel


def main():
    print("Part 1:", part1(puzzle_input))
    print("Part 2:", part2(puzzle_input))


if __name__ == "__main__":
    main()
