from common import puzzle_input_as_str
from IntcodeComputer import IntcodeComputer

puzzle_input = puzzle_input_as_str(day=5)


def part1(computer):
    computer.inputs.append(1)
    computer.run()
    return computer.output


def part2(computer):
    computer.inputs.append(5)
    computer.run()
    return computer.output


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)
    result_part1 = part1(computer)
    assert result_part1 == 5044655
    print("Part 1:", result_part1)

    result_part2 = part2(computer)
    assert result_part2 == 7408802
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
