import math

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=12)


def parse_input(puzzle_input: list) -> list:
    moons = []
    for line in puzzle_input:
        line = line[1:-1]
        words = line.split(", ")
        x, y, z = (
            int(words[0].split("=")[1]),
            int(words[1].split("=")[1]),
            int(words[2].split("=")[1]),
        )
        new_moon = ((x, y, z), (0, 0, 0))

        moons.append(new_moon)

    return moons


def move_moons(moons: list):
    for i, moon in enumerate(moons):
        (x, y, z), (vx, vy, vz) = moon
        for j, other_moon in enumerate(moons):
            if i == j:
                continue
            (ox, oy, oz), _ = other_moon

            if x > ox:
                vx -= 1
            elif x < ox:
                vx += 1

            if y > oy:
                vy -= 1
            elif y < oy:
                vy += 1

            if z > oz:
                vz -= 1
            elif z < oz:
                vz += 1

        moons[i] = ((x, y, z), (vx, vy, vz))

    for i, moon in enumerate(moons):
        (x, y, z), (vx, vy, vz) = moon
        moons[i] = ((x + vx, y + vy, z + vz), (vx, vy, vz))


def total_energy(moons: list) -> int:
    total_energy = 0

    for moon in moons:
        (x, y, z), (vx, vy, vz) = moon
        total_energy += (abs(x) + abs(y) + abs(z)) * (
            abs(vx) + abs(vy) + abs(vz)
        )

    return total_energy


def moon_states(moons: list) -> tuple:
    x_state, y_state, z_state = [], [], []
    for moon in moons:
        ((x, y, z), (vx, vy, vz)) = moon
        x_state.append((x, vx))
        y_state.append((y, vy))
        z_state.append((z, vz))

    return tuple(x_state), tuple(y_state), tuple(z_state)


def run_until_repeat(moons: list) -> int:
    states = [{}, {}, {}]

    iterations = 0
    diffs = [0, 0, 0]

    while True:
        move_moons(moons)
        iterations += 1
        cur_states = moon_states(moons)
        for i in range(3):
            if not diffs[i] and cur_states[i] in states[i]:
                diffs[i] = iterations - states[i][cur_states[i]]
            else:
                states[i][cur_states[i]] = iterations

        if all(diffs):
            break

    return math.lcm(*diffs)


def main():
    moons = parse_input(puzzle_input)
    for _ in range(1000):
        move_moons(moons)
    result_part1 = total_energy(moons)

    moons = parse_input(puzzle_input)
    result_part2 = run_until_repeat(moons)

    assert result_part1 == 7636
    assert result_part2 == 281691380235984

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
