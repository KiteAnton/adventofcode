from itertools import permutations

from common import puzzle_input_as_str
from IntcodeComputer import IntcodeComputer

puzzle_input = puzzle_input_as_str(day=7)


def run_amplifiers(phase_settings: tuple, int_computers: list) -> int:

    for i, int_computer in enumerate(int_computers):
        int_computer.reset()
        int_computer.inputs.append(phase_settings[i])

    thrust_output = 0

    finished = False
    while not finished:
        for int_computer in int_computers:
            int_computer.inputs.append(thrust_output)
            finished = int_computer.run(reset=False)
            thrust_output = int_computer.output

    return thrust_output


def find_max_settings(int_computers: list, phase_options: tuple) -> int:
    options = permutations(phase_options, 5)
    thrust_outputs = map(
        lambda option: run_amplifiers(
            phase_settings=option, int_computers=int_computers
        ),
        options,
    )
    return max(thrust_outputs)


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    int_computers = [IntcodeComputer(int_codes) for _ in range(5)]
    result_part1 = find_max_settings(int_computers, (0, 1, 2, 3, 4))
    result_part2 = find_max_settings(int_computers, (5, 6, 7, 8, 9))

    assert result_part1 == 30940
    assert result_part2 == 76211147
    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
