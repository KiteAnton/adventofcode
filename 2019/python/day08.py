import heapq
from collections import defaultdict

from common import fill_char, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=8)


def part1(image: str, layer_size: int) -> int:
    image_layers = [
        image[i : i + layer_size] for i in range(0, len(image), layer_size)
    ]

    layer_count = []
    for layer in image_layers:
        count = defaultdict(int)
        for char in layer:
            count[char] += 1

        heapq.heappush(layer_count, (count["0"], count["1"] * count["2"]))

    _, product_ones_twos = heapq.heappop(layer_count)
    return product_ones_twos


def part2(image: str, width: int, height: int) -> str:
    image_len = len(image)

    output = ""
    for pixel in range(width * height):

        for value in image[pixel : image_len : width * height]:
            if value == "0":
                output += " "
                break
            elif value == "1":
                output += fill_char
                break

        if (pixel + 2) % width == 0:
            output += "\n"

    return output


def main():
    image = puzzle_input[0]
    result_part1 = part1(image, layer_size=25 * 6)

    assert result_part1 == 2286

    result_part2 = part2(image, width=25, height=6)
    print("Part 1:", result_part1)
    print("Part 2:\n", result_part2)


if __name__ == "__main__":
    main()
