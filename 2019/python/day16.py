from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=16)


def multiply_sequence(input_sequence: list):
    pattern = [0, 1, 0, -1]

    for i in range(len(input_sequence)):
        pattern_index = 1
        new_value = 0
        for value in input_sequence:
            new_value += value * pattern[pattern_index // (i + 1)]
            pattern_index = (pattern_index + 1) % (len(pattern) * (i + 1))

        input_sequence[i] = abs(new_value) % 10


def part1(input_sequence: list) -> int:
    for i in range(100):
        multiply_sequence(input_sequence)

    return int("".join(map(str, input_sequence[:8])))


def part2(input_sequence: list) -> int:
    offset = int("".join(map(str, input_sequence[:7])))
    input_sequence = input_sequence[offset:]

    for _ in range(100):
        value = 0
        for i in range(len(input_sequence) - 1, -1, -1):
            value += input_sequence[i]
            input_sequence[i] = value % 10

    return int("".join(map(str, input_sequence[:8])))


def main():

    input_sequence = puzzle_input[0]
    input_sequence = [int(char) for char in input_sequence]

    result_part1 = part1(input_sequence.copy())

    result_part2 = part2(input_sequence * 10_000)

    assert result_part1 == 25131128
    assert result_part2 == 53201602

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
