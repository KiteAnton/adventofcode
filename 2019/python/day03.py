from common import manhattan_distance, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=3)


def move_wire(wire_position: list, direction: str) -> None:
    if direction == "R":
        wire_position[1] += 1
    elif direction == "L":
        wire_position[1] -= 1
    elif direction == "U":
        wire_position[0] += 1
    elif direction == "D":
        wire_position[0] -= 1


def run_wire(instructions: list) -> dict:
    wire_pos = [0, 0]
    visited = {}
    steps_moved = 0
    for move in instructions:
        direction = move[0]
        steps = int(move[1:])
        for _ in range(steps):
            steps_moved += 1
            move_wire(wire_pos, direction)
            visited[tuple(wire_pos)] = steps_moved
    return visited


def main():
    first = puzzle_input[0].split(",")
    second = puzzle_input[1].split(",")

    first_visited = run_wire(first)
    second_visited = run_wire(second)

    crossings = set(first_visited.keys()) & set(second_visited.keys())
    distances = map(
        lambda crossing: manhattan_distance(crossing, [0, 0]), crossings
    )

    steps_to_intersection = map(
        lambda crossing: first_visited[crossing] + second_visited[crossing],
        crossings,
    )

    print("Part 1:", min(distances))
    print("Part 2:", min(steps_to_intersection))


if __name__ == "__main__":
    main()
