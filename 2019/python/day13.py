import os
import time

from common import puzzle_input_as_str
from IntcodeComputer import IntcodeComputer, RunningMode

puzzle_input = puzzle_input_as_str(day=13)

VISUALIZE = True


class ArcadeGame:
    def __init__(self, computer: IntcodeComputer):
        self.computer = computer
        self.walls = set()
        self.blocks = set()
        self.ball = (0, 0)
        self.paddle = (0, 0)
        self.score = 0

    def init_game(self):
        self.computer.reset()

    def collect_outputs(self):
        outputs = []
        self.computer.run(wait_on_output=True, reset=False)
        while self.computer.mode == RunningMode.OUTPUT_READY:
            outputs.append(self.computer.output)
            self.computer.run(wait_on_output=True, reset=False)

        for i in range(0, len(outputs), 3):
            try:
                x, y, tile_id = outputs[i : i + 3]
            except:
                continue
            if x == -1 and y == 0:
                self.score = tile_id
                continue

            pos = (x, y)
            match tile_id:
                case 0:
                    self.blocks.discard(pos)
                case 1:
                    self.walls.add(pos)
                case 2:
                    self.blocks.add(pos)
                case 3:
                    self.paddle = pos
                case 4:
                    self.ball = pos

    def __str__(self):
        ROW_SIZE = 25
        COLUMN_SIZE = 46

        os.system("clear")
        output = ""

        for y in range(ROW_SIZE):
            for x in range(COLUMN_SIZE):
                pos = (x, y)
                if pos in self.walls:
                    if y == 0:
                        output += "="
                    else:
                        output += "|"
                elif pos in self.blocks:
                    output += "★"
                elif pos == self.paddle:
                    output += "▔"
                elif pos == self.ball:
                    output += "⦿"
                else:
                    output += " "

            output += "\n"
        for x in range(COLUMN_SIZE):
            output += "="

        output += "\n"
        output += f"Score: {self.score}\n"
        output += f"Blocks: {len(self.blocks)}\n"

        return output

    def move_left(self):
        self.computer.inputs.append(-1)

    def move_right(self):
        self.computer.inputs.append(1)

    def stay(self):
        self.computer.inputs.append(0)


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    int_codes[0] = 2
    computer = IntcodeComputer(int_codes)

    arcade = ArcadeGame(computer)
    arcade.collect_outputs()
    result_part1 = len(arcade.blocks)

    while computer.mode != RunningMode.ENDED:
        if arcade.ball[0] < arcade.paddle[0]:
            arcade.move_left()
        elif arcade.ball[0] > arcade.paddle[0]:
            arcade.move_right()
        else:
            arcade.stay()
        arcade.collect_outputs()
        if VISUALIZE:
            print(arcade)
            time.sleep(0.005)

    result_part2 = computer.output

    assert result_part1 == 324
    assert result_part2 == 15957

    print("Part1:", result_part1)
    print("Part2:", result_part2)


if __name__ == "__main__":
    main()
