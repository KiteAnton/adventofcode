from common import manhattan_distance, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=4)


def valid_numbers(number: int) -> list:
    number_str = str(number)
    prev_value = None
    adj_values_list = []
    adj_values = []

    for char in number_str:
        digit = int(char)
        if prev_value:
            if digit < prev_value:
                return [False, False]
            if digit == prev_value:
                adj_values.append(digit)
            else:
                if adj_values:
                    adj_values_list.append(adj_values)
                adj_values = []
        prev_value = digit
    if adj_values:
        adj_values_list.append(adj_values)

    for adj_val_group in adj_values_list:
        if len(adj_val_group) == 1:
            return [True, True]
    return [len(adj_values_list) > 0, False]


def main():
    range_min, range_max = list(map(int, puzzle_input[0].split("-")))
    valid1 = 0
    valid2 = 0

    for number in range(range_min, range_max):
        valids = valid_numbers(number)
        valid1 += valids[0]
        valid2 += valids[1]

    print("Part 1:", valid1)
    print("Part 2:", valid2)


if __name__ == "__main__":
    main()
