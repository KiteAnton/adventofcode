import heapq
from collections import deque

from common import find_adjacent_positions_2d


def a_star(start, end, nr_rows, nr_cols):
    to_visit = [(0, start)]
    nodes = {start: (0, start)}

    boundaries = ((0, nr_rows - 1), (0, nr_cols - 1))
    while to_visit:

        current_node_g, current_node = heapq.heappop(to_visit)

        if current_node == end:
            return current_node_g, nodes

        # Gets valid moves in directions, within given area
        for new_position in find_adjacent_positions_2d(
            current_node, boundaries=boundaries
        ):

            # Check if move allow,

            # Cost for this new move
            new_node_g = current_node_g + Cost(new_position)

            # Heuristics
            new_node_h = manhattan_distance(new_position, end)

            new_node_f = new_node_g + new_node_h

            # Don't add if 'cheaper' solution already exists
            if new_position in nodes and nodes[new_position][0] <= new_node_f:
                continue

            nodes[new_position] = (new_node_f, current_node)
            heapq.heappush(to_visit, (new_node_g, new_position))

    return 0


# Time complexity O(V + E)
# V number of nodes, E: number of edges
# Space complexity O(V)
def dfs(visited, graph, node):
    if node not in visited:
        visited.add(node)
        for option in graph[node]:
            dfs(visited, graph, option)


# Time complexity O(V+E)
# V number of nodes, E: number of edges
# Space complexity O(V)
def bfs(graph, node):
    visited = set()
    queue = deque([node])

    while queue:
        state = queue.popleft()

        if state in visited:
            continue

        for option in graph[state]:
            visited.add(option)
            queue.append(option)
