import heapq

from common import (
    find_adjacent_positions_2d,
    find_boundaries,
    puzzle_input_as_str,
)
from IntcodeComputer import IntcodeComputer, RunningMode

puzzle_input = puzzle_input_as_str(day=15)


class RepairDroid:
    def __init__(self, computer: IntcodeComputer):
        self.computer = computer
        self.blocked_positions = set()
        self.reset()

    def reset(self):
        self.computer.reset()
        self.position = (0, 0)
        self.try_position = (0, 0)
        self.position_oxygen = (0, 0)

    def move_north(self) -> bool:
        self.try_position = (self.position[0] - 1, self.position[1])
        self.computer.inputs.append(1)
        return self.check_status()

    def move_south(self) -> bool:
        self.try_position = (self.position[0] + 1, self.position[1])
        self.computer.inputs.append(2)
        return self.check_status()

    def move_west(self) -> bool:
        self.try_position = (self.position[0], self.position[1] - 1)
        self.computer.inputs.append(3)
        return self.check_status()

    def move_east(self) -> bool:
        self.try_position = (self.position[0], self.position[1] + 1)
        self.computer.inputs.append(4)
        return self.check_status()

    def check_status(self) -> bool:
        self.computer.run(wait_on_output=True, reset=False)
        if self.computer.mode == RunningMode.OUTPUT_READY:
            match self.computer.output:
                case 0:
                    self.blocked_positions.add(self.try_position)
                    return False
                case 1:
                    self.position = self.try_position
                    return True
                case 2:
                    self.position_oxygen = self.try_position
                    return False
        return False

    def __str__(self):
        ((min_row, max_row), (min_col, max_col)) = find_boundaries(
            self.blocked_positions | {self.position, self.position_oxygen}
        )

        output = ""
        for row in range(min_row, max_row + 1):
            for col in range(min_col, max_col + 1):

                if (row, col) in self.blocked_positions:
                    output += "#"
                elif (row, col) == self.position_oxygen and (row, col) != (
                    0,
                    0,
                ):
                    output += "0"
                elif (row, col) == self.position:
                    output += "X"
                elif (row, col) == (0, 0):
                    output += "S"
                else:
                    output += " "
            output += "\n"

        return output


def find_oxygen_position(repair_droid: RepairDroid) -> int:
    start = (0, 0)
    to_visit = [(0, start, [])]
    nodes = {}

    while to_visit:

        current_node_g, current_node, input_sequence = heapq.heappop(to_visit)
        repair_droid.reset()

        for input_value in input_sequence:
            match input_value:
                case 1:
                    repair_droid.move_north()
                case 2:
                    repair_droid.move_south()
                case 3:
                    repair_droid.move_west()
                case 4:
                    repair_droid.move_east()

        if input_sequence and repair_droid.computer.output == 0:
            continue
        if repair_droid.position_oxygen != (0, 0):
            return current_node_g

        nodes[current_node] = current_node_g

        for move in [1, 2, 3, 4]:
            new_pos = current_node
            match move:
                case 1:
                    new_pos = (current_node[0] - 1, current_node[1])
                case 2:
                    new_pos = (current_node[0] + 1, current_node[1])
                case 3:
                    new_pos = (current_node[0], current_node[1] - 1)
                case 4:
                    new_pos = (current_node[0], current_node[1] + 1)

            if new_pos in repair_droid.blocked_positions or (
                new_pos in nodes and nodes[new_pos] <= current_node_g + 1
            ):
                continue
            heapq.heappush(
                to_visit,
                (
                    current_node_g + 1,
                    new_pos,
                    input_sequence + [move],
                ),
            )

    return 0


visited = set()


def find_longest_path(node, steps, blocked_positions):

    visited.add(node)

    result = steps
    for new_pos in find_adjacent_positions_2d(node):
        if new_pos not in blocked_positions and new_pos not in visited:
            new_result = find_longest_path(
                new_pos, steps + 1, blocked_positions
            )
            result = max(result, new_result)

    return result


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)

    repair_droid = RepairDroid(computer)
    result_part1 = find_oxygen_position(repair_droid)
    print(repair_droid)

    assert result_part1 == 366
    result_part2 = find_longest_path(
        repair_droid.position_oxygen,
        steps=0,
        blocked_positions=repair_droid.blocked_positions,
    )

    assert result_part2 == 384

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
