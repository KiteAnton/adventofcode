import heapq
import math
from collections import defaultdict

from common import manhattan_distance, puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=10)


def parse_input(puzzle_input: list) -> set:

    asteroids = set()
    for row, line in enumerate(puzzle_input):
        for col, char in enumerate(line):
            if char == "#":
                asteroids.add((row, col))

    return asteroids


def find_angles_and_distances(asteroids: set) -> dict:
    angles_and_distances = {}

    for asteroid in asteroids:
        line_of_sights = defaultdict(list)
        for other_asteroid in asteroids:
            if other_asteroid == asteroid:
                continue
            diff_row = other_asteroid[0] - asteroid[0]
            diff_col = other_asteroid[1] - asteroid[1]
            distance = manhattan_distance(asteroid, other_asteroid)
            angle = math.atan2(diff_row, diff_col) + math.pi / 2
            heapq.heappush(line_of_sights[angle], (distance, other_asteroid))

        nr_angles = len(line_of_sights.keys())
        list_for_asteroid = [
            (key, value) for key, value in line_of_sights.items()
        ]

        angles_and_distances[nr_angles] = sorted(list_for_asteroid)

    return angles_and_distances


def shoot_asteroids(asteroids_to_shoot: list) -> int:
    # reorder list, start at angle 0
    while True:
        if asteroids_to_shoot[0][0] >= 0:
            break
        asteroids_to_shoot.append(asteroids_to_shoot.pop(0))

    removed = 0
    removed_asteroid = (0, (0, 0))
    while removed < 200:
        angle = asteroids_to_shoot.pop(0)
        removed_asteroid = angle[1].pop(0)
        removed += 1
        if len(angle[1]):
            asteroids_to_shoot.append(angle)

    return removed_asteroid[1][1] * 100 + removed_asteroid[1][0]


def main():
    asteroids = parse_input(puzzle_input)
    angles_and_distances = find_angles_and_distances(asteroids)
    result_part1 = max(angles_and_distances.keys())
    result_part2 = shoot_asteroids(angles_and_distances[result_part1])

    assert result_part1 == 284
    assert result_part2 == 404
    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()

main()
