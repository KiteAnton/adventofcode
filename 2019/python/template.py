import string
from collections import defaultdict, deque
from copy import copy, deepcopy
from pprint import pprint

from common import (
    find_common_char_in_strings,
    manhattan_distance,
    puzzle_input_as_int,
    puzzle_input_as_str,
)

puzzle_input = puzzle_input_as_str(day=5)
puzzle_input = puzzle_input_as_str(day=5, test=1)

puzzle_input = puzzle_input_as_str(day=5, separator="\n\n")
puzzle_input = puzzle_input_as_str(day=5, separator="\n\n", test=1)

puzzle_input = puzzle_input_as_int(day=5)
puzzle_input = puzzle_input_as_int(day=5, test=1)

puzzle_input = puzzle_input_as_int(day=5, separator="\n\n")
puzzle_input = puzzle_input_as_int(day=5, separator="\n\n", test=1)


def part1(puzzle_input):
    return


def part2(puzzle_input):
    return


def main():

    result_part1 = 0
    result_part2 = 0

    # assert result_part1 ==
    # assert result_part2 ==

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()

main()
