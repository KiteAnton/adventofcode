import string
from collections import defaultdict, deque
from copy import copy, deepcopy
from pprint import pprint

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=6)


def parse_input(puzzle_input: list) -> tuple:
    orbit_map = {}
    orbit_map_reversed = {}

    for line in puzzle_input:
        orbits, space_object = line.split(")")
        assert space_object not in orbit_map
        orbit_map[space_object] = orbits
        if orbits not in orbit_map_reversed:
            orbit_map_reversed[orbits] = []
        orbit_map_reversed[orbits].append(space_object)

    return orbit_map, orbit_map_reversed


def part1(orbit_map_reversed: dict) -> int:
    queue = deque([("COM", 0)])
    orbit_count = {}

    while queue:
        (node, orbits) = queue.popleft()
        orbit_count[node] = orbits

        if node not in orbit_map_reversed:
            continue
        for option in orbit_map_reversed[node]:
            queue.append((option, orbits + 1))

    return sum(orbit_count.values())


def part2(orbit_map: dict) -> int:
    map_you = []
    next_node = "YOU"
    while next_node != "COM":
        next_node = orbit_map[next_node]
        map_you.append(next_node)

    map_san = []
    next_node = "SAN"
    while next_node != "COM":
        next_node = orbit_map[next_node]
        map_san.append(next_node)

    for index, (option1, option2) in enumerate(
        zip(reversed(map_you), reversed(map_san))
    ):
        if option1 != option2:
            return len(map_you) - index + len(map_san) - index

    return 0


def main():
    orbit_map, orbit_map_reversed = parse_input(puzzle_input)
    result_part1 = part1(orbit_map_reversed)
    result_part2 = part2(orbit_map)

    assert result_part1 == 160040
    assert result_part2 == 373

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
