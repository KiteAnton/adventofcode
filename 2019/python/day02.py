from common import puzzle_input_as_str
from IntcodeComputer import IntcodeComputer

puzzle_input = puzzle_input_as_str(day=2)


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)
    computer.run(noun=12, verb=2)
    result_part1 = computer.int_codes[0]

    result_part2 = 0
    halt_value = 19690720
    for noun in range(0, 99):
        for verb in range(0, 99):
            computer.run(noun=noun, verb=verb)
            if computer.int_codes[0] == halt_value:
                result_part2 = 100 * noun + verb
                break
        else:
            continue
        break

    assert result_part1 == 3654868
    assert result_part2 == 7014
    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
