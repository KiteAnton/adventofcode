from common import fill_char, find_boundaries, puzzle_input_as_str
from IntcodeComputer import IntcodeComputer, RunningMode

puzzle_input = puzzle_input_as_str(day=11)


def calculate_panels(computer: IntcodeComputer, white_panels: set) -> tuple:
    black_panels = set()
    current_position = (0, 0)
    directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    direction = 0
    while True:
        computer.inputs.append(current_position in white_panels)

        computer.run(reset=False, wait_on_output=True)
        match computer.output:
            case 0:
                white_panels.discard(current_position)
                black_panels.add(current_position)
            case 1:
                white_panels.add(current_position)
                black_panels.discard(current_position)

        computer.run(reset=False, wait_on_output=True)
        match computer.output:
            case 0:
                direction = (direction - 1) % 4
            case 1:
                direction = (direction + 1) % 4

        current_position = (
            current_position[0] + directions[direction][0],
            current_position[1] + directions[direction][1],
        )

        if computer.mode == RunningMode.ENDED:
            break

    return white_panels, black_panels


def draw_identifier(white_panels: set) -> str:
    ((row_min, row_max), (col_min, col_max)) = find_boundaries(white_panels)

    output = "\n"
    for row in range(row_min, row_max + 1):
        for col in range(col_min, col_max + 1):
            if (row, col) in white_panels:
                output += fill_char
            else:
                output += " "

        output += "\n"

    return output


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)
    white_panels = set()
    white_panels, black_panels = calculate_panels(
        computer, white_panels=white_panels
    )
    result_part1 = len(white_panels) + len(black_panels)

    white_panels.clear()
    computer.reset()
    white_panels.add((0, 0))
    white_panels, _ = calculate_panels(computer, white_panels=white_panels)
    result_part2 = draw_identifier(white_panels)

    assert result_part1 == 2093

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
