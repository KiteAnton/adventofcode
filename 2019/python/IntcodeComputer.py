from collections import defaultdict, deque
from enum import Enum

from common import find_nth_digit


class ParameterMode(Enum):

    POSITION = 0
    IMMEDIATE = 1
    RELATIVE = 2


class RunningMode(Enum):

    RUNNING = 0
    ENDED = 1
    WAITING_FOR_INPUT = 2
    OUTPUT_READY = 3


class IntcodeComputer:

    END_VALUE = 99

    def __init__(self, int_codes: list):
        self.start_int_codes = int_codes
        self.reset()
        self.mode = RunningMode.RUNNING

    def reset(self):
        self.output = 0
        self.inputs = deque()
        self.int_code_pointer = 0
        self.int_codes = defaultdict(int)
        for i, value in enumerate(self.start_int_codes):
            self.int_codes[i] = value

        self.parameter_modes = []
        self.load_next_opcode()
        self.relative_base = 0

    def load_next_opcode(self):
        if self.int_code_pointer >= len(self.int_codes):
            return False

        opcode_raw = self.int_codes[self.int_code_pointer]
        self.opcode = opcode_raw % 100
        self.parameter_modes.clear()
        for i in range(3):
            match find_nth_digit(opcode_raw, digit=i + 2, from_right=True):
                case 0:
                    self.parameter_modes.append(ParameterMode.POSITION)
                case 1:
                    self.parameter_modes.append(ParameterMode.IMMEDIATE)
                case 2:
                    self.parameter_modes.append(ParameterMode.RELATIVE)

    def get_params(self, amount: int) -> list:
        params = []
        for i in range(amount):
            match self.parameter_modes[i]:
                case ParameterMode.IMMEDIATE:
                    params.append(
                        self.int_codes[self.int_code_pointer + 1 + i]
                    )
                case ParameterMode.POSITION:
                    params.append(
                        self.int_codes[
                            self.int_codes[self.int_code_pointer + 1 + i]
                        ]
                    )
                case ParameterMode.RELATIVE:
                    params.append(
                        self.int_codes[
                            self.int_codes[self.int_code_pointer + 1 + i]
                            + self.relative_base
                        ]
                    )

        return params

    def get_position_value(self, steps_forward: int) -> int:
        if self.int_code_pointer + steps_forward >= len(self.int_codes):
            return 0
        match self.parameter_modes[steps_forward - 1]:
            case ParameterMode.POSITION:
                return self.int_codes[self.int_code_pointer + steps_forward]
            case ParameterMode.RELATIVE:
                return (
                    self.int_codes[self.int_code_pointer + steps_forward]
                    + self.relative_base
                )
        return 0

    def move_pointer(self, pointer: int, absolute: bool = False):
        if absolute:
            self.int_code_pointer = pointer
        else:
            self.int_code_pointer += pointer
        self.load_next_opcode()

    def addition(self):
        value1, value2 = self.get_params(amount=2)
        param3 = self.get_position_value(steps_forward=3)
        self.int_codes[param3] = value1 + value2
        self.move_pointer(4)

    def multiplication(self):
        value1, value2 = self.get_params(amount=2)
        param3 = self.get_position_value(steps_forward=3)
        self.int_codes[param3] = value1 * value2
        self.move_pointer(4)

    def read_input(self):
        param1 = self.get_position_value(steps_forward=1)
        if not len(self.inputs):
            self.mode = RunningMode.WAITING_FOR_INPUT
            return
        input_value = self.inputs.popleft()
        self.int_codes[param1] = input_value
        self.move_pointer(2)

    def set_output(self):
        value1, *_ = self.get_params(amount=1)
        self.output = value1
        self.move_pointer(2)
        self.mode = RunningMode.OUTPUT_READY

    def jump_if_true(self):
        value1, value2 = self.get_params(amount=2)
        if value1:
            self.move_pointer(value2, absolute=True)
        else:
            self.move_pointer(3)

    def jump_if_false(self):
        value1, value2 = self.get_params(amount=2)
        if not value1:
            self.move_pointer(value2, absolute=True)
        else:
            self.move_pointer(3)

    def less_than(self):
        value1, value2 = self.get_params(amount=2)
        param3 = self.get_position_value(steps_forward=3)
        if value1 < value2:
            self.int_codes[param3] = 1
        else:
            self.int_codes[param3] = 0
        self.move_pointer(4)

    def equals(self):
        value1, value2 = self.get_params(amount=2)
        param3 = self.get_position_value(steps_forward=3)
        if value1 == value2:
            self.int_codes[param3] = 1
        else:
            self.int_codes[param3] = 0
        self.move_pointer(4)

    def adjust_relative_base(self):
        value1, *_ = self.get_params(amount=1)
        self.relative_base += value1
        self.move_pointer(2)

    def step(self):
        match self.opcode:
            case 1:
                self.addition()

            case 2:
                self.multiplication()

            case 3:
                self.read_input()

            case 4:
                self.set_output()

            case 5:
                self.jump_if_true()

            case 6:
                self.jump_if_false()

            case 7:
                self.less_than()

            case 8:
                self.equals()

            case 9:
                self.adjust_relative_base()

        if self.opcode == self.END_VALUE:
            self.mode = RunningMode.ENDED

    def run(
        self,
        reset: bool = True,
        wait_on_output: bool = False,
        noun: int = 0,
        verb: int = 0,
    ) -> bool:
        if reset:
            self.reset()
        self.mode = RunningMode.RUNNING
        if noun and verb:
            self.int_codes[1] = noun
            self.int_codes[2] = verb

        while self.mode == RunningMode.RUNNING or (
            self.mode == RunningMode.OUTPUT_READY and not wait_on_output
        ):
            self.step()

        return self.mode == RunningMode.ENDED
