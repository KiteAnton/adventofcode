from common import puzzle_input_as_str
from IntcodeComputer import IntcodeComputer

puzzle_input = puzzle_input_as_str(day=9)


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)
    computer.inputs.append(1)
    computer.run()
    result_part1 = computer.output

    computer.inputs.append(2)
    computer.run()
    result_part2 = computer.output

    assert result_part1 == 2350741403
    assert result_part2 == 53088
    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
