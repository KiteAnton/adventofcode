from collections import defaultdict
from ctypes import create_string_buffer

from common import puzzle_input_as_str

puzzle_input = puzzle_input_as_str(day=14)


def parse_input(puzzle_input: list) -> tuple:
    reactions = {}
    primitives = {}

    for line in puzzle_input:
        consumes, produces = line.split(" => ")
        amount_produced, produced_mineral = produces.split()
        for consume in consumes.split(", "):
            amount_cost, mineral = consume.split()
            if mineral == "ORE":
                primitives[produced_mineral] = (
                    int(amount_produced),
                    int(amount_cost),
                )
            else:
                if produced_mineral not in reactions:
                    reactions[produced_mineral] = (int(amount_produced), {})
                reactions[produced_mineral][1][mineral] = int(amount_cost)

    return primitives, reactions


def primitives_needed_for_mineral(
    mineral_needed: str,
    amount_needed: int,
    primitives: dict,
    reactions: dict,
    inventory: defaultdict,
) -> defaultdict:
    if mineral_needed in inventory and inventory[mineral_needed] > 0:
        amount_needed -= inventory[mineral_needed]
        inventory[mineral_needed] = 0

    primitives_needed = defaultdict(int)
    mineral_to_produce = reactions[mineral_needed]
    times = (amount_needed - 1) // mineral_to_produce[0] + 1

    for consume_mineral, consume_amount in mineral_to_produce[1].items():
        if consume_mineral in primitives:
            primitives_needed[consume_mineral] += consume_amount * times
        else:
            primitives_for_mineral = primitives_needed_for_mineral(
                consume_mineral,
                consume_amount * times,
                primitives,
                reactions,
                inventory,
            )
            for new_mineral, new_amount in primitives_for_mineral.items():
                primitives_needed[new_mineral] += new_amount

    if (mineral_to_produce[0] * times) > amount_needed:
        inventory[mineral_needed] = (
            mineral_to_produce[0] * times
        ) - amount_needed
    return primitives_needed


def cost_for_primitives(primitives_needed: dict, primitives_cost: dict) -> int:
    cost = 0
    for primitive, amount in primitives_needed.items():
        primitive_cost = primitives_cost[primitive]
        times = (amount - 1) // primitive_cost[0] + 1
        cost += primitive_cost[1] * times

    return cost


def find_most_fuel_possible(
    cost_one_fuel: int, primitives: dict, reactions: dict
) -> int:

    cargo_hold = 1_000_000_000_000

    low = cargo_hold // cost_one_fuel
    high = 0

    need_increase = True

    current_try = low * 2
    while True:
        inventory = defaultdict(int)

        primities_needed = primitives_needed_for_mineral(
            "FUEL",
            current_try,
            primitives=primitives,
            reactions=reactions,
            inventory=inventory,
        )
        cost = cost_for_primitives(primities_needed, primitives)
        if cost > cargo_hold:
            high = current_try
            need_increase = False
        elif cost < cargo_hold:
            low = current_try
            need_increase = True

        if low + 1 == high:  # Solution found
            return low

        if not high:
            current_try *= 2
            continue

        if need_increase:
            current_try += int((high - low) // 2)
        else:
            current_try -= int((high - low) // 2)


def main():

    primitives, reactions = parse_input(puzzle_input)
    inventory = defaultdict(int)

    primities_needed = primitives_needed_for_mineral(
        "FUEL",
        1,
        primitives=primitives,
        reactions=reactions,
        inventory=inventory,
    )
    result_part1 = cost_for_primitives(primities_needed, primitives)

    result_part2 = find_most_fuel_possible(
        cost_one_fuel=result_part1, primitives=primitives, reactions=reactions
    )

    assert result_part1 == 2556890
    assert result_part2 == 1120408

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
