from os.path import isabs
import string
from collections import defaultdict, deque
from copy import copy, deepcopy
from pprint import pprint

from common import (
    find_adjacent_positions_2d,
    find_boundaries,
    find_common_char_in_strings,
    manhattan_distance,
    puzzle_input_as_str,
)
from IntcodeComputer import IntcodeComputer, RunningMode

puzzle_input = puzzle_input_as_str(day=17)


class CameraOutput:
    def __init__(self, computer: IntcodeComputer):
        self.computer = computer
        self.reset()

    def reset(self):
        self.scaffolds = set()
        self.robots = set()
        self.free_space = set()

    def get_image(self):
        row = 0
        col = 0
        self.computer.run(wait_on_output=True, reset=False)
        while self.computer.mode == RunningMode.OUTPUT_READY:
            pos = (row, col)
            col += 1
            match self.computer.output:
                case 35:
                    if pos in self.scaffolds:
                        print("duplicate", pos)
                    self.scaffolds.add(pos)
                case 46:
                    self.free_space.add(pos)
                case 10:
                    row += 1
                    col = 0
                case 94:
                    self.robots.add(pos)

            self.computer.run(wait_on_output=True, reset=False)

    def find_intersections(self):
        intersections = set()

        for scaffold in self.scaffolds:
            is_intersection = True
            for test_pos in find_adjacent_positions_2d(scaffold):
                if test_pos not in self.scaffolds:
                    is_intersection = False

            if is_intersection:
                intersections.add(scaffold)

        return intersections

    def __str__(self):
        ((row_min, row_max), (col_min, col_max)) = find_boundaries(
            self.free_space | self.robots | self.scaffolds
        )

        output = ""
        for row in range(row_min, row_max + 1):
            for col in range(col_min, col_max + 1):
                pos = (row, col)
                if pos in self.scaffolds:
                    output += "#"
                elif pos in self.robots:
                    output += "^"
                else:
                    output += " "
            output += "\n"

        return output


def part1(camera: CameraOutput) -> int:
    camera.get_image()
    intersections = camera.find_intersections()

    result = 0
    for intersection in intersections:
        result += intersection[0] * intersection[1]

    return result


def part2(puzzle_input):
    return


def main():
    int_codes = list(map(int, puzzle_input[0].split(",")))
    computer = IntcodeComputer(int_codes)
    camera = CameraOutput(computer)
    camera.get_image()
    print(camera)

    result_part1 = part1(camera)
    result_part2 = 0

    assert result_part1 == 4408
    # assert result_part2 ==

    print("Part 1:", result_part1)
    print("Part 2:", result_part2)


if __name__ == "__main__":
    main()
